class Demo{
	int x =10;
	String s = "Classes and Object";
	
	void info(){
		print(x);
		print(s);
	}
}

void main(){
	
	Demo d = new Demo();
	d.info();
	
	print(d.s);
	print(d.x);
}
