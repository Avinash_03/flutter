abstract class IFC{
	void material(){
		print("Indian Body");
	}
	void taste();
}
class Indianfc implements IFC{
	void material(){
		print("Indian material");
	}
	void taste(){
		print("Indian taste");
	}
}
class EUFC extends IFC{
	void material(){
		print("Indian material");
	}
	void taste(){
		print("Europian Taste");
	}
}
void main(){
	Indianfc obj = new Indianfc();
	obj.material();
	obj.taste();
}
