/*	In dart Interface key-word not use for write interfece ,direct implement key-word use 
	in interface we can achhive multiple inheritance

*/
abstract class Developer{
	int x=10;
	Developer(){
		print("In Developer Constructor:");
	}
	void develop(){
		print("In Build Software");
	}
	void devtype(){}
}
class mobileDev implements Developer{
	int x=20;
	mobileDev(){
		//Developer();				interface madhe constuctorla auto call jat nahi explicitlydyava lagto
							//tyasathi class lagto abstract class access hot nahi
		print("In mobileDev Constructor:");
	}
	void develop(){
		print("In Build Software");		//we can override the all method is neccessry
	}	
	void devtype(){
		print("Flutter Developer");
	}
}
void main(){
	Developer obj = new mobileDev();
	obj.develop();
	obj.devtype();
	print(obj.x);					//old value replace by new value
}
