/*	In dart Interface key-word not use for write interfece ,direct implement key-word use 
	in interface we can achhive multiple inheritance

*/
abstract class Developer{
	void develop(){
		print("In Build Software");
	}
	void devtype();
}
class mobileDev implements Developer{
	void develop(){
		print("In Build Software");		//we can override the all method is neccessry
	}	
	void devtype(){
		print("Flutter Developer");
	}
}
void main(){
	mobileDev obj = new mobileDev();
	obj.develop();
	obj.devtype();
}
