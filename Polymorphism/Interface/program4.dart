//multiple inheritance
abstract class interfaceDemo1{
	void m1(){
		print("In m1 interface");
	}
}
abstract class interfaceDemo2{
	void m2(){
		print("In m2 interface");
	}
}
class Demo implements interfaceDemo1,interfaceDemo2{
	void m1(){
		print("In m1 Demo");
	}
	void m2(){
		print("In m2 Demo");
	}
}
void main(){
	Demo obj = new Demo();
	obj.m1();
	obj.m2();
}
