//In Abstract class Present 0-100% abstraction (abstraction means method does't have body)
abstract class Parent{
	void Property(){
		print("Banglow,Cars,Gold");
	}
	void Career();
	void Marry();
}
class Child extends Parent{
	void Career(){
		print("Engg");
	}
	void Marry(){
		print("GF");
	}
}
void main(){
	Child obj = new Child();
	obj.Career();
	obj.Marry();
}
