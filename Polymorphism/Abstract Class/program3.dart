abstract class Developer{
	Developer(){
		print("In Developer Constructor");
	}
	void develope(){
		print("We Build Software");
	}
	void devtype();
}
class MobileDev extends Developer{
	void devtype(){
		print("Mobile developer");
	}
}
class WebDev extends Developer{
	void devtype(){
		print("Web Developer");
	}
}
void main(){
	Developer obj1 = new Developer();
	Developer obj2 = new MobileDev();
	WebDev obj3 = new WebDev();
	Developer obj4 = new WebDev();

	obj1.develope();
	obj1.devtype();
	obj2.develope();
	obj2.devtype();
	obj3.develope();
	obj3.devtype();
	obj4.develope();
	obj4.devtype();
	
}
