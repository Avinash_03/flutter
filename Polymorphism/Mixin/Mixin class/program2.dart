abstract mixin class  Demo{
	
	//Demo(){}					// cant write constructor
	
	void fun1(){
		print("In fun1");
	}
	void fun2();
}
class Asach {
	void ashi(){
		print("In ashi method");
	}	
}
class Child extends Asach with Demo{
	void fun2(){
		print("In fun2");
	}
}
void main(){
	Child obj = new Child();
//	Demo obj1 = new Demo();					cnt create object 
	obj.fun1();
	obj.fun2();
	obj.ashi();
}

