mixin Demo{
	Demo(){						//mixin cant be declare constructor
		print("In Demo Constructor");		
	}
	void m1(){
		print("In m1 Method");
	}
	void m2();
}
void main(){
	Demo obj = new Demo();			//we cant create object of mixin
}
