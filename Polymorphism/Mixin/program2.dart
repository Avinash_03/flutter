//mixin like class
mixin Demoparent{
	void m1(){
		print("In m1 Demoparent");
	}
}
mixin Demo{
	void m2(){
		print("In m2 Demo");
	}
}
class DemoChild with Demo,Demoparent{
}

void main(){
	DemoChild obj = new DemoChild();
	obj.m1();
	obj.m2();
}
