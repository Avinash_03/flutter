/*Private Acces Specifier

In dart Private keyword not allow in this we can use symbol like _
And Private have a file Scope in a dart and java have class Scope
*/

class Player{
	
	int? _jerNo;
	String? _PName;

	Player(this._jerNo,this._PName);
	
	void PlayerInfo(){
		print(_jerNo);
		print(_PName);
	}
}
void main(){
	Player obj = new Player(45,"Rohit");
	obj.PlayerInfo();
	
	obj._jerNo=18;
	obj._PName="Virat";
	obj.PlayerInfo();
}

