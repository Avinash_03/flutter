//getter Method 
//3 way to write getter Mothod
//Way 3
class Demo{
	int? _x;
	String? str;
	double? _sal;
	
	Demo(this._x,this.str,this._sal);
	
	get getX => _x;
	get getName => str;
	get getSalary => _sal;
}
//main in next code
