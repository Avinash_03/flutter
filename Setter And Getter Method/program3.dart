//getter Method 
//3 way to write getter Mothod
//Way 1
class Demo{
	int? _x;
	String? str;
	double? _sal;
	
	Demo(this._x,this.str,this._sal);
	
	int? getX(){
		return _x;
	}
	String? getName(){
		return str;
	}
	double? getSalary(){
		return _sal;
	}
}
//main in next code
