//Setter
//3 way to write setter method
//Way 3
class Demo{
	
	int? _x;
	String? str;
	double? _sal;

	Demo(this._x,this.str,this._sal);
	
	set setX(int x) => _x=x;
	set setName(String name) => str=name;
	set setSalary(double Sal) =>_sal=Sal;
	
	
	void Disp(){
		print(_x);
		print(str);
		print(_sal);
	}
}
//main in next code
