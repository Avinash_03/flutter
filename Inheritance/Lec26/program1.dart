class Parent{
	int x=10;
	String str= "SurName";
	void ParentDisp(){
		print("In Parent Method");
	}
}
class Child extends Parent{
}
void main(){
	Child obj = new Child();
	print(obj.x);
	print(obj.str);
	obj.ParentDisp();
}
