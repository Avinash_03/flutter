class Parent{		//Base,Superclass,Parentclass
	Parent(){
		print("In Parent Constructor:");	
	}
	call(){
		print("In Method");	
	}
}
class Child extends Parent{	//Derived,subclass,Childclass

	Child(){
		super();
		print("In Child Constructor:");
	}
}
void main(){
	Child obj = new Child();
}
