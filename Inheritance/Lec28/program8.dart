//Passing argument's to child constructor
class Parent{
	int? x;
	String? name;

	Parent(this.x,this.name);
	
	void PrintData(){
		print(x);
		print(name);
	}
}
class Child extends Parent{
	int? y;
	String? str;

	Child(this.y,this.str){	     //Error: The superclass, 'Parent', has no unnamed constructor that takes no arguments.

		Parent(y,str);
	}
	
	void DispData(){
		print(y);
		print(str);
	}
}
void main(){
	Child obj = new Child(10,"Kanha");
}
