//Types of Inheritance
//Multiple
class Parent{
	Parent(){
		print("Parent Constructor");
	}
}
class Parent2{
	Parent2(){
		print("Parent2 Constructor");
	}
}
class Child extends Parent,Parent2{    //Error: Each class definition can have at most one extends clause.
	Child(){
		print("Child Constructor");
	}
}
void main(){
	Child obj = new Child();
}
