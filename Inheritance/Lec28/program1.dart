class Parent{
	int x=10;
	Parent(){
		print("In Parent Constructor:");
		print(this.hashCode);
	}
	void PrintData(){
		print(x);
	}
}
class Child extends Parent{
	int x=20;
	
	Child(){
		print("In Child Constructor:");
		print(this.hashCode);
	}
	void DispData(){
		print(x);
	}
}
void main(){
	Child obj = new Child();
	obj.PrintData();
	obj.DispData();
}
