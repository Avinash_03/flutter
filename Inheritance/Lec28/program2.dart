class Parent{		//Base,Superclass,Parentclass
	Parent(){
		print("In Parent Constructor:");	
	}
}
class Child extends Parent{	//Derived,subclass,Childclass

	Child(){
		super();	// Error: Superclass has no method named 'call'.	
		print("In Child Constructor:");
	}
}
void main(){
	Child obj = new Child();
}
