class Parent{
	int x=10;
	String str1="Name";

	void ParentMethod(){
		print(x);
		print(str1);
	}
}
class Child extends Parent{
	int y=20;
	String str2="Data";

	void ChildMethod(){
		print(y);
		print(str2);
	}
}
void main(){
	Child obj  = new Child(); 
	print(obj.x);	
	print(obj.str1);
	obj.ParentMethod();	

	print(obj.y);	
	print(obj.str2);
	obj.ChildMethod();	
}
