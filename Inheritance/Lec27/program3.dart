class Parent{
	int x=10;
	String str1="Name";

	get getX => x;
	get getStr =>str1;
}
class Child extends Parent{
	int y=20;
	String str2="Data";

	get getY => y;
	get getStr2 => str2;
}
void main(){
	Child obj  = new Child(); 
	print(obj.getX);	
	print(obj.getStr);

	print(obj.getY);	
	print(obj.getStr2);
}
