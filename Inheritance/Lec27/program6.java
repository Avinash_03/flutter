class Parent{
	int x=10;
	String str1="Name";

	void ParentMethod(){
		System.out.println(x);
		System.out.println(str1);
	}
}
class Child extends Parent{
	int x=20;
	String str1="Data";

	void ChildMethod(){
		System.out.println(x);
		System.out.println(str1);
	}
}
class Client{
 public static void main(String[] args){
	Parent obj = new Parent();
	Child obj2  = new Child(); 
	System.out.println(obj2.x);	
	System.out.println(obj2.str1);
	obj2.ParentMethod();			//in java it gives 10 Name O/P but in dart constructor not inherit default

	System.out.println(obj.x);	
	System.out.println(obj.str1);
	obj.ParentMethod();	
}
}
