class Parent{
	int x=10;
	String str1="Name";

	void ParentMethod(){
		print(x);
		print(str1);
	}
}
class Child extends Parent{
	int y=20;
	String str2="Data";

	void ChildMethod(){
		print(x);
		print(str1);
	}
}
void main(){
	Parent obj  = new Parent(); 
	print(obj.y);		//Error : The getter 'y' isn't defined for the class 'Parent'.
	print(obj.str2);	//Error : The getter 'str2' isn't defined for the class 'Parent'.
	obj.ChildMethod();	//Error : The method 'ChildMethod' isn't defined for the class 'Parent'.
}
