import "Dart:io";
import "Dart:core";	//this is default package
void main(){
	print(stdin.runtimeType);	//Stdin - this is a class
	int age =stdin.readLineSync();		//Error - 'String?' can't be assigned to a variable of type 'int'.
	print("Age = $age");
}
