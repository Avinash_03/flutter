//Logical 	!	&&	||
void main(){
	int x = 10;
	int y = 8;
	
	print(x&&y);		//Error
	print(x||y);		//Error
	print(!x);		//Error
	print(!y);		//Error
}
//note :- need boolean value
