//No Argument Constructor
class Demo{
	Demo(){
		print("In Constructor: ");
	}	
	Demo._private(){
		print("In Private Constructor: ");
	}	
}
/*
void main(){
	Demo obj = new Demo();
	Demo obj1 = Demo();
	new Demo();
	Demo();
}*/

