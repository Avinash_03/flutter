/* Date -21 -10 -23
*/
class Demo{
	static Demo obj = new Demo();
	Demo(){
		print("In Constructor");
	}
	Demo fun(){
		return obj;
	}
}
void main(){
	Demo obj = new Demo();		//In constructor
	obj.fun();			//In constructor
}
