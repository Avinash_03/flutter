//Named Constructor
class Demo{
	Demo(){
		print("In  Normal Constructor: ");	
	}
	Demo.data(){
		print("In Named Constructor: ");	
	}
	Demo.data1(){
		print("In Named Constructor: ");	
	}
}
void main(){
	Demo obj = new Demo();
	Demo.data();
	Demo.data1();
}


