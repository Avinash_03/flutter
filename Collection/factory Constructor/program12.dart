class Bakend{
	String? lang;
	Bakend._code(String lang){
		if(lang=='JavaScript'){
			this.lang="NodeJs";
		}else if(lang=="Java"){
			this.lang="SpringBoot";
		}else{
			this.lang="NodeJs/SpringBoot";
		}
	}
	factory Bakend(String lang){
		return new Bakend._code(lang);
	}
}
void main(){
	Bakend obj = new Bakend("JavaScript");
	Bakend obj2 = new Bakend("Java");
	Bakend obj3 = new Bakend("Kotlin");

	print(obj.lang);
	print(obj2.lang);
	print(obj3.lang);
}
