//Constant Constructor
class Demo{
	final int? x;
	const Demo(this.x);	//it cant have body
}
void main(){
	Demo obj = const Demo(10);
	Demo obj1 = const Demo(10);
	
	print(obj.hashCode);	//12
	print(obj1.hashCode);	//12	same code
}
