class Demo{
	Demo._private(){
		print("In Constructor");
	}
	factory Demo(){
		print("In Factory Constructor");
		return new Demo._private();
	}
}
//main in next file
