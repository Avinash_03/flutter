abstract class Developer{
	factory Developer (String Devtype){
		if(Devtype=="Mobile"){
			return Mobile();
		}else if(Devtype=="Bakend"){
			return Bakend();
		}else if(Devtype=='Frontend'){
			return Frontend();
		}else{
			return Other();
		}	
	}
	void Devlang();
}
class Mobile implements Developer{
	Mobile(){
	//	print("Flutter,Android");
	}
	void Devlang(){
		print("Flutter,Android");
	}
}
class Bakend implements Developer{
	void Devlang(){
		print("NodeJs/SpringBoot");
	}
}
class Frontend implements Developer{
	void Devlang(){
		print("JavaScipt/PHP/React/CSS");
	}
}
class Other implements Developer{
	void Devlang(){
		print("Testing/other");
	}
}
void main(){
	Developer obj = new Developer("Mobile");	//factory constructor return obj of child
	obj.Devlang();					//Flutter, android
}
