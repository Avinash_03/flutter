class Demo{
	Demo(){
		print("In Constructor");
	}
	factory Demo(){					//error alrady declear
		print("In Factory Constructor");
		return new Demo();			//return is compusory 
	}
}
void main(){
	Demo obj = new Demo();
}
