/*Singleton Pattern
*/
class Demo{
	static Demo obj = new Demo._private();
	Demo._private(){
		print("In Private Constructor");
	}
	factory Demo(){
		return obj;
	}
	Demo.x(int num){
		print(num);
	}
}
void main(){
	Demo obj = new Demo();
	Demo obj1 = new Demo();
	
	print(obj.hashCode);
	print(obj1.hashCode);
}
