/*	1)Empty
	2)filled
	3)generate
	4)of
	5)unmodififiable
*/
//1)Empty
void main(){
	List Player1 =List.empty();	//Fix size List
	List Player2 = List.empty(growable:true);	//growable
	
//	Player1.add('Virat');		//Error Empty has a no size
//	Player2[0]='MSD';		//Error we can add data first then use [] on list
	
	Player2.add('Virat');
	Player2.add("Rohit");

	print(Player1);		//[]
	print(Player2);		//[Virat,Rohit]
}
