/*Properties
	1)first				6)last
	2)hashCode			7)length
	3)isEmpty			8)reversed
	4)isNotEmpty			9)runtimeType
	5)iterator			10)single
*/
void main(){
	List player = ['Virat','Rohit','Kl','Msd'];

	print(player.first);
	print(player.hashCode);
	print(player.isEmpty);
	print(player.isNotEmpty);
	print(player.iterator);
	print(player.last);
	print(player.length);
	print(player.runtimeType);
	print(player.single);
}
