/*	1)Empty
*/
//1)Empty
void main(){
	List Player1 =List.empty();	//Fix size List
	List Player2 = List.empty(growable:true);	//growable
	
	
	Player2.add('Virat');
	Player2.add("Rohit");

	print(Player1);		//[]
	print(Player2);		//[Virat,Rohit]
	Player2[0]='MSD';		
	print(Player2);		//[MSD,Rohit]
}
