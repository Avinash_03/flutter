import 'dart:io';
String getDetail(){
	stdout.write("Enter Your Order: ");
	String order = stdin.readLineSync()!;
	return order;
}
Future<String> getOrder(){
	return Future.delayed(Duration(seconds:3),()=>getDetail());
}
Future<String> getMessage()async{
	var order= getOrder();
	return 'Place order of: $order';
}
void main() async{
	print("Start Code");
	print(await getMessage());
	print("End Code");
}
