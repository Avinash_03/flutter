void fun1(){
	for(int i=1;i<6;i++){
		print("In fun1-Loop 1");
	}
	Future.delayed(Duration(seconds:5),()=>print("Delay"));
	for(int i=1;i<6;i++){
		print("In fun1-Loop 2");
	}
}
void fun2(){
	for(int i=1;i<6;i++){
		print("In fun2");
	}
}
void main(){
	print("Start Main");
	fun1();
	fun2();
	print("End Main");
}
