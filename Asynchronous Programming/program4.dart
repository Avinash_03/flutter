Future<String> getOrder(){
	return Future.delayed(Duration(seconds:5),()=>'Burger');
}
Future<String> getMessage()async{
	var order= await getOrder();
	return 'You are order is : $order';
}
void main() async{
	print("Start Code");
	print(await getMessage());
	print("End Code");
}
