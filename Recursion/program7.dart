//Addition of 1 to 5 in Recursion
int add=0;
int x=5;
int? fun(){
	if(x<1)
		return add;
	add=add+x;
	x--;
	fun();
	return add;

}
void main(){
	int x=5;
	int? ret =fun();
	print(ret);
}
