//Named Constructor
class Company{
	int? empcnt;
	String? CompName;

	Company.constr(this.empcnt,this.CompName);
	CompInfo(){
		print(empcnt);
		print(CompName);
	}
} 
void main(){
	 
	Company obj1 = new Company.constr(10,"Bincaps");
	obj1.CompInfo();
	Company obj2 = new Company.constr(12,"Incubator");
	obj2.CompInfo();
}
//this constructor use when we can need two constructor ,in dart we cant give same name of constructor
