//Date- 25-09-23
//Lec- 23
class Demo{
	int x = 10;
	static int y = 20;
}
void main(){
	Demo obj = new Demo();

	print(obj.x);
	print(obj.y);	// Error: The getter 'y' isn't defined for the class 'Demo'.
			//y is static ans static is a class veriable
}
