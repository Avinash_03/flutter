class Demo{
	int? x;
	String? str;
	
	Demo(int Value, String name){
		print("In Parameterized Constructor");
	}
	
	void PrintData(){
		print(x);
		print(str);
	}
}
void main(){
	Demo obj = new Demo(10,"Kanha");
	obj.PrintData();
}
