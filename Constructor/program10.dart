class Demo{
	int x=5;
	Demo(){					//Demo(Demo this)
		print("In Constructor");	
	}
}
void main(){
	Demo obj = new Demo();		//this line perform 3Work -->   //1) Object Create
									//2) Address Copy = obj
}									//3) Demo(obj);
