class Parent{
	Parent(){
		print("In parent Constructor");
	}
	call(){
		print("In Call Method");
	}
}
class Child extends Parent{
	Child(){
		print("In Child Constructor");
	}
}
void main(){
	Child obj = new Child();
	obj();					
}
/*	O/P -
		In Parent 
		In Child
		In call	
*/


