//Date - 25-09-23
//Lec - 23

class Demo{
	int x=10;
	static int y=20;

	static void PrintData(){	//in dart every thing is object 
		print(x);		//(Error non static not access in static) Error: Undefined name 'x'.
		print(y);
	}
}
void main(){
	Demo obj = new Demo();
	Demo.PrintData();
}
