/*Constant object
	
object const kelya mule new object banat nahi jr argument same asel tr
object const alsya mule constructor pn const lagto
*/

class Player{
	final int? jerNo;
	final String? PName;
	
	const Player(this.jerNo,this.PName);
}
void main(){
	Player obj = const Player(18,"Virat");
	print(obj.hashCode);		//same code
	Player obj2 = const Player(18,"Virat");
	print(obj2.hashCode);		//same code
	Player obj3 = const Player(1,"Kl");
	print(obj3.hashCode);		//Diff code
}
