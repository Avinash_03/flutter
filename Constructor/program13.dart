//Default Parameter
class Company{
	int? empcnt;
	String? CompName;

	Company(this.empcnt,{this.CompName = "C2W"});
	
	CompInfo(){
		print(empcnt);
		print(CompName);
	}
} 
void main(){
	 
	Company obj1 = new Company(10,"Bincaps");		//Error we cnt provide arrgument in default value
	obj1.CompInfo();
	Company obj2 = new Company(10);
	obj2.CompInfo();
}
