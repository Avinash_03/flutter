import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Bottomshit1 extends StatefulWidget {
  const Bottomshit1({super.key});

  @override
  State<Bottomshit1> createState() => _BottomshitState();
}

class _BottomshitState extends State<Bottomshit1> {

  TextEditingController titlecontroller= TextEditingController();
  TextEditingController desccontroller= TextEditingController();
  TextEditingController datecontroller= TextEditingController();

  

  int cnt=0;
  void _increase(){ 
       cnt++;
       
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("Bottomshit1"),
      ),
      body: Container(),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          showModalBottomSheet(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            context: context, 
            builder: (BuildContext context){
              return StatefulBuilder(
                builder: (context, setState) {
                return Column(children: [
                Text("Count= $cnt"),
                ElevatedButton(
                  onPressed: (){setState(() {
                    _increase();
                  });}, 
                  child: Icon(Icons.add,))
              ],);
                
              } );
              
            }
            //   Column(
            //     children: 
            //     [
            //       const SizedBox(height: 15,),
            //       Text("Create Task",style: GoogleFonts.quicksand(
            //         textStyle: const TextStyle(
            //           color: Color.fromRGBO(0, 0, 0, 1),
            //           fontSize: 22,
            //           fontWeight: FontWeight.w600
            //         )
            //       )),
            //       Row(
            //         children:  [
            //            const SizedBox(width: 15,),
            //           Text("Title",textAlign: TextAlign.start,style: GoogleFonts.quicksand(textStyle: const TextStyle(color:  Color.fromRGBO(0, 139, 148, 1),fontSize: 11,fontWeight: FontWeight.w400)),),
            //         ],
            //       ),
            //       Container(
            //         margin:const EdgeInsets.only(right:15,left: 15,bottom: 10),
            //         decoration: BoxDecoration(
            //           border: Border.all(color: const Color.fromRGBO(0, 139, 148, 1),width: 0.5),
            //           borderRadius: BorderRadius.circular(5)
              
            //         ),
            //         child:const  TextField(
                      
            //           decoration: InputDecoration(
            //             contentPadding: EdgeInsets.all(10),
            //             border: InputBorder.none,
            //             hintText: "Enter Title"
                        
            //           ),
            //         ),
            //       ),
            //       Row(
            //         children:  [
            //            const SizedBox(width: 15,),
            //           Text("Description",textAlign: TextAlign.start,style: GoogleFonts.quicksand(textStyle: const TextStyle(color:  Color.fromRGBO(0, 139, 148, 1),fontSize: 11,fontWeight: FontWeight.w400)),),
            //         ],
            //       ),
            //          Container(
            //         margin:const EdgeInsets.only(right:15,left: 15,bottom: 10),
            //         decoration: BoxDecoration(
            //           border: Border.all(color: const Color.fromRGBO(0, 139, 148, 1),width: 0.5),
            //           borderRadius: BorderRadius.circular(5)
              
            //         ),
            //         child:const TextField(
                    
            //           decoration: InputDecoration(
            //             contentPadding: EdgeInsets.all(10),
            //             border: InputBorder.none,
            //             hintText: "Enter Description"
                        
            //           ),
            //         ),
            //       ),
            //       Row(
            //         children:  [
            //            const SizedBox(width: 15,),
            //           Text("Date",textAlign: TextAlign.start,style: GoogleFonts.quicksand(textStyle: const TextStyle(color:  Color.fromRGBO(0, 139, 148, 1),fontSize: 11,fontWeight: FontWeight.w400),),)
            //         ],
            //       ),
            //        Container(
            //         margin:const EdgeInsets.only(right:15,left: 15,bottom: 10),
            //         decoration: BoxDecoration(
            //           border: Border.all(color: const Color.fromRGBO(0, 139, 148, 1),width: 0.5),
            //           borderRadius: BorderRadius.circular(5)
              
            //         ),
            //         child:const TextField(
                      
            //           decoration: InputDecoration(
            //             contentPadding: EdgeInsets.all(10),
            //             border: InputBorder.none,
            //             hintText: "Enter Date"
                        
            //           ),
            //         ),
            //       ),
            //     ElevatedButton(
            //       style:  ButtonStyle(
            //         shape:MaterialStatePropertyAll(RoundedRectangleBorder(
            //           borderRadius: BorderRadius.circular(10)
            //         )),
            //         fixedSize:const MaterialStatePropertyAll(Size(300, 50)),
            //         backgroundColor:const MaterialStatePropertyAll(Color.fromRGBO(0, 139, 148, 1))
            //       ),
            //       onPressed: (){
              
            //       }, 
            //       child:  Text("Submit",style: GoogleFonts.inter(textStyle:const TextStyle(
            //         color: Color.fromRGBO(255,255,255,1,
            //         ),
            //         fontSize: 20,
            //         fontWeight: FontWeight.w700
            //       )),))
                
            //     ],
            //   );
            // }
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}