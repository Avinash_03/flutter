import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Addskill extends StatefulWidget {
  const Addskill({super.key});

  @override
  State<Addskill> createState() => _AddskillState();
}

class _AddskillState extends State<Addskill> {
  TextEditingController skillController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        child:Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Container(
            margin: const EdgeInsets.all(10),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                boxShadow: const [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.3),
                      blurRadius: 8,
                      offset: Offset(0, 3))
                ]),
            child: TextFormField(
              controller: skillController,
              decoration: const InputDecoration(
                  contentPadding: EdgeInsets.only(left: 10),
                  border: InputBorder.none,
                  hintText: "Enter Skills"),
            ),
          ),
          const SizedBox(height: 10,),
            ElevatedButton(
                    style: const 
                    ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.blue)
                    ),
                    onPressed: (){ 
                    Navigator.push(context, MaterialPageRoute(builder:(context)=> const Addskill()));

                    }, 
                    child:const Text("Add Skill",style: TextStyle(color: Colors.white),),),
                   
          Expanded(
            child: SizedBox(
              
              
               child:
             ListView.builder(
                  itemCount: 100,
                  itemBuilder: (context,index){
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        color: Colors.blue,
                        height: 30,
                      ),
                    );
                  }
                  
              
              
              ),
            ),
          ),
        ],

      ),
      ),
    );
  }
}
