import 'package:flutter/material.dart';
import 'package:inheritedwidget/addskillscreen.dart';
import 'package:inheritedwidget/main.dart';

class EmployeeScreen extends StatefulWidget {
  const EmployeeScreen({super.key});

  @override
  State<EmployeeScreen> createState() => _EmployeeScreenState();
}

class _EmployeeScreenState extends State<EmployeeScreen> {
  
  @override
  Widget build(BuildContext context) {
    Myinherited obj= Myinherited.of(context);
    return Column(
      children: [
        Container(
          width: double.infinity,
              margin:const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: const[
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.6),
                    blurRadius: 8,
                    offset: Offset(0, 3)
                  )
                ]
           ),
           child: Column(
            children: [
              Text('${obj.id}',),
              const SizedBox(height: 10,),
               Text("${obj.name}"),
              const SizedBox(height: 10,),
               Text("${obj.username}"),
               const SizedBox(height: 40,),
               ElevatedButton(
                    style: const 
                    ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.blue)
                    ),
                    onPressed: (){ 
                    Navigator.push(context, MaterialPageRoute(builder:(context)=> const Addskill()));

                    }, 
                    child:const Text("Add Skill",style: TextStyle(color: Colors.white),)),
                    const SizedBox(height: 10,)
                    
            ],
           ),
        )
      ],
    );
  }
}