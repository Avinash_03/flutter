import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inheritedwidget/employeeScreen.dart';
import 'package:inheritedwidget/logindata.dart';
import 'package:inheritedwidget/main.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}


class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              margin:const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: const[
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.6),
                    blurRadius: 8,
                    offset: Offset(0, 3)
                  )
                ]
              ),
              child: Column(
                children: [
                  Text("Login",style: GoogleFonts.poppins(textStyle:const TextStyle(fontSize: 16)),),
                  const SizedBox(height: 10,),
                  const Logindata(text:'Id'),
                  const SizedBox(height: 10,),
                  const Logindata(text: 'Name'),
                  const SizedBox(height: 10,),
                  const Logindata(text: 'UserName'),
                  const SizedBox(height: 40,),
                  ElevatedButton(
                    style: const 
                    ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.blue)
                    ),
                    onPressed: (){ 
                    Navigator.push(context, MaterialPageRoute(builder:(context)=> Myinherited(id:int.tryParse(idController.text)??0,name: nameController.text,username: usernameController.text,child:const EmployeeScreen(),)));

                    }, 
                    child:const Text("Submit",style: TextStyle(color: Colors.white),)),
                    const SizedBox(
                      height: 10,
                    )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}