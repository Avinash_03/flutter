import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
TextEditingController idController=TextEditingController();
TextEditingController nameController=TextEditingController();
TextEditingController usernameController=TextEditingController();
class Logindata extends StatefulWidget {
  final String text;
  const Logindata({super.key,required this.text });

  @override
  State<Logindata> createState() => _LogindataState();
}

class _LogindataState extends State<Logindata> {

  TextEditingController getController(){
    if(widget.text=='Id'){
      return idController;
    }else if(widget.text=='Name'){
      return nameController;
    }else{
      return usernameController;
    }
  }
  @override
  Widget build(BuildContext context) {
    return  Column(
      children: [
        Text(widget.text,style: GoogleFonts.poppins(textStyle:const TextStyle(fontSize: 15)),),
        Container(
          margin:const EdgeInsets.all(10),
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: const[
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.3),
                blurRadius: 8,
                offset: Offset(0, 3)
              )
            ]
          ),
          child: TextFormField(
            controller: getController(),
            decoration:  InputDecoration(
            contentPadding:const  EdgeInsets.only(left: 10),
            border: InputBorder.none,
            hintText: "Enter ${widget.text}"
            ),
          ),
        ),
      ],
    );
  }
}