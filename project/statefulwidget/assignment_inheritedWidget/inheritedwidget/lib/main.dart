import 'package:flutter/material.dart';
import 'package:inheritedwidget/loginScreen.dart';

void main() {
  runApp(const MainApp());
}

class Myinherited extends InheritedWidget{
  
  
  final int? id;
  final String? name;
  final String? username;
   const Myinherited({super.key, this.id , this.name, this.username, required super.child});

  
  static  Myinherited of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType()!;
  }

  @override
  bool updateShouldNotify(Myinherited oldWidget){
    return id!=oldWidget.id;
  }  
}
class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginScreen()
    );
  }
}
