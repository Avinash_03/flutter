import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class Company extends ChangeNotifier{
  String compName;
  int empCount;
  Company({required this.compName,required this.empCount});

  void changeCompany( String compName,int empCount){
    this.compName=compName;
    this.empCount=empCount;
    notifyListeners();
  }
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create:(context){
        return Company(compName: "google", empCount:250);
      },
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Homepage()
      ),
    );
  }
}

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(Provider.of<Company>(context).compName),
          const SizedBox(height: 40,),
          Text("${Provider.of<Company>(context).empCount}"),
          const SizedBox(height: 50,),
          ElevatedButton(onPressed: (){
            setState(() {
              Provider.of<Company>(context,listen: false).changeCompany("faceBook",20);
            });
            
          }, 
          child:const Text("Change Information"))
        ],
      ),
    );
  }
}