import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
void main() {
  runApp(const MainApp());
}
var obj;
class Company {
  String compName;
  int empCount;
  Company({required this.compName,required this.empCount, });
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return Provider(
      create:(context){
        return obj= Company(compName: "google", empCount:250);
      },
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Homepage()
      ),
    );
  }
}

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(Provider.of<Company>(context).compName),
          const SizedBox(height: 40,),
          Text("${Provider.of<Company>(context).empCount}"),
          const SizedBox(height: 50,),
          ElevatedButton(onPressed: (){
            setState(() {
              obj.compName='avi';
              obj.empCount=3;
            });
          }, 
          child:const Text("Change Information"))
        ],
      ),
    );
  }
}