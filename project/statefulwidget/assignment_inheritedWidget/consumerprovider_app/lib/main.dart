import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    log("In MainApp Build");
    return MultiProvider(
      providers: [
        Provider(create: (context) {
          return const Player(pName: "Virat", jerNo: 18);
        }),
        ChangeNotifierProvider(create: (context) {
          return Match(matchNo: 200, runs: 8900);
        })
      ],
      child: const MaterialApp(
        home: Home(),
      ),
    );
  }
}

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State createState() => HomeState();
}

class HomeState extends State {
  @override
  Widget build(BuildContext context) {
    log("In Home Build");
    return Scaffold(
        appBar: AppBar(
          title: const Text("Consumer"),
        ),
        body: Column(
          children: [
            Text(Provider.of<Player>(context).pName),
            const SizedBox(height: 30),
            Text("${Provider.of<Player>(context).jerNo}"),
            const SizedBox(height: 30),
            Consumer(builder: (context,value,child) {
              return Column(
                children: [
                  Text("${Provider.of<Match>(context).runs}"),
                  const SizedBox(height: 30),
                  Text("${Provider.of<Match>(context).matchNo}"),
                ],
              );
            }),
            const SizedBox(height: 30),
            ElevatedButton(
              onPressed: () {
                Provider.of<Match>(context, listen: false)
                    .changedata(9000, 250);
              },
              child: const Text("Change Data"),
            )
          ],
        ));
  }
}

class Player {
  final String pName;
  final int jerNo;

  const Player({required this.pName, required this.jerNo});
}

class Match with ChangeNotifier {
  int runs;
  int matchNo;

  Match({required this.matchNo, required this.runs});

  void changedata(int runs, int matchNo) {
    this.runs = runs;
    this.matchNo = matchNo;
    notifyListeners();
  }
}
