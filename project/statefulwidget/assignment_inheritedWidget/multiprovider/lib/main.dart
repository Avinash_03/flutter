import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (constext){
          return Employee(empName: "Raj", empid:12);
        }),
        ChangeNotifierProvider(
          create: (context){
            return Project(projectName: "Quiz App", devType: 'Desktop');
        })
      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner:false,
        home: Home(),
      ),
    );
  }
}
class Home extends StatefulWidget{
  const Home({super.key});

  @override
  State createState()=> HomeState();
}

class HomeState extends State{
  @override
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(
        title: const Text("MultiProvider"),

      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(Provider.of<Employee>(context).empName),
          const SizedBox(height: 30,),
          Text("${Provider.of<Employee>(context).empid}"),
          const SizedBox(height: 30,),
          Text(Provider.of<Project>(context).projectName),
          const SizedBox(height: 30,),
          Text(Provider.of<Project>(context).devType),
          const SizedBox(height: 30,),
          ElevatedButton(
            onPressed: (){
              Provider.of<Project>(context,listen: false).changeDetails("ToDo App", "Mobile");
          }, 
          child:const Text("Change Data")
          )
        ],
      ),
    );
  }
}
class Employee{
  String empName;
  int empid;
  Employee({required this.empName,required this.empid});
}

class Project extends ChangeNotifier{
  String projectName;
  String devType;

  Project({required this.projectName,required this.devType});

  changeDetails(String projectName,String devType){
    this.projectName=projectName;
    this.devType=devType;
    notifyListeners();
  }
}
