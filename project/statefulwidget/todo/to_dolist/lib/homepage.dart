import 'package:flutter/material.dart';
import 'model.dart';
import 'package:google_fonts/google_fonts.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {

List clr=const [Color.fromRGBO(250, 232, 232,1),Color.fromRGBO(232, 237, 250,1),Color.fromRGBO(250, 249, 232,1),Color.fromRGBO(250, 232, 250,1)];

Color getcolor(int ind){
  return clr[ind%clr.length];
}
List data=[];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(2,167, 177, 1),
        title:  Text("To-do list", style:GoogleFonts.quicksand(textStyle: const TextStyle(color: Color.fromRGBO(255, 255, 255, 1),fontSize: 26,fontWeight: FontWeight.w700),) ),
      ),
      body: Padding(
        padding:const EdgeInsets.symmetric(vertical: 10,horizontal: 10),
        child: ListView.builder(
          itemCount: data.length,
          itemBuilder: (BuildContext context,int  index) {
            return Container(
              margin: const EdgeInsets.all(10),
              width: double.infinity,
              decoration: BoxDecoration(
                color: getcolor(index),
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                boxShadow: const[
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.1),
                    blurRadius: 20,
                    spreadRadius: 1,
                    offset: Offset(0, 10)

                  )
                ]
              ),
              child: Column(
                children: [
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      const SizedBox(width: 70,),
                      Text("${data[index].title}",style:const TextStyle(color: Color.fromRGBO(0, 0, 0,1,),fontWeight: FontWeight.w600,fontSize: 12),)
                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      const SizedBox(width: 10,),
                      Container(
                        height: 52,
                        width: 52,
                        decoration:const BoxDecoration(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 10,
                              color: Color.fromRGBO(0, 0, 0,0.07)
                            )
                          ]
                        ),
                        child: Center(child: Image.network("https://cdn.pixabay.com/photo/2016/03/31/19/50/checklist-1295319_1280.png",height: 19.07,width: 23.79,)),
                      ),
                      const SizedBox(width: 10,),
                      SizedBox(
                        width: 350,
                        child: Text(data[index].description,style: const TextStyle(color: Color.fromRGBO(84, 84, 84, 1)),),
                      )

                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(children: [
                    const SizedBox(width: 10,),
                    Text(data[index].date,style: const TextStyle(color: Color.fromRGBO(132, 132, 132, 1),fontWeight: FontWeight.w500,fontSize: 10)),
                    const Spacer(),
                    const Icon(Icons.edit_outlined,color: Color.fromRGBO(0, 139, 148, 1),),
                    const SizedBox(width: 10,),
                    const Icon(Icons.delete_outline,color: Color.fromRGBO(0, 139, 148, 1))
                  ],),
                  const SizedBox(height: 10,)
                ],
              ),

            );

          }
          
          
          ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState(() {
            
            data.add(
              Model(title:"Lorem Ipsum is simply setting industry. ",description:"Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",date:"10 July 2023")
            );
          });

        },
        child: const Icon(Icons.add,weight: 50,),
        
      ),
    );

  }
}