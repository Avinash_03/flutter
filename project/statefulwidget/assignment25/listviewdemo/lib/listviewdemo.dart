import 'package:flutter/material.dart';

class Listviewdemo extends StatefulWidget {
  const Listviewdemo({super.key});

  @override
  State<Listviewdemo> createState() => _ListviewdemoState();
}

class _ListviewdemoState extends State<Listviewdemo> {

  List cricket=[["One Day","https://akm-img-a-in.tosshub.com/aajtak/images/author/rohit-sharma.jpg","https://akm-img-a-in.tosshub.com/businesstoday/images/story/202301/virat_kohli_hundred_1200x768-sixteen_nine.jpeg?size=948:533","https://akm-img-a-in.tosshub.com/indiatoday/images/story/202301/shubmangillindvnz-sixteen_nine.jpg?VersionId=w.MsdLuMju716lVJu.nL9HH3DjG2KSSq&size=690:388"],["ODI","https://images.indianexpress.com/2023/09/Rahul-15.jpg","https://crictoday.com/wp-content/uploads/2023/11/Shreyas-Iyer-1.webp","https://imgeng.jagran.com/images/2023/nov/RJADJEA1700127903469.jpg"],["T-20","https://img.theweek.in/content/dam/week/news/sports/images/2023/11/23/Suryakumar%20Yadav-pti.jpg","https://crickettimes.com/wp-content/uploads/2023/07/Ruturaj-Gaikwad-to-lead-India-in-Asian-Games.jpg","https://cdn-wp.thesportsrush.com/2022/03/071969d6-kishan-injury.jpg?w=3840&q=60"]];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Listview",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w900,fontSize: 25),),
        centerTitle: true,
        backgroundColor: Colors.black,
      ),
      body: ListView.separated(
        itemCount: cricket.length,
        itemBuilder: (BuildContext context,int  index1) {
          return Column(
            
            children: [
              Text(cricket[index1][0],style:const TextStyle(fontWeight: FontWeight.w700,fontSize: 20),),
              ListView.builder(

                shrinkWrap: true,
                itemCount: cricket.length,
                physics:const BouncingScrollPhysics(),
                itemBuilder: (BuildContext context, int index){
                  return Container(
                    height: 200,
                    width: 200,
                    margin: const EdgeInsets.all(10),
                    child: Image.network(cricket[index1][index+1]),
                  );
                }
              ),
            ],
          );
        }, 
        separatorBuilder: (BuildContext context, int index){
          return const Text("_________________________________________________________");
        }, 
        
      ),
    );
  }
}