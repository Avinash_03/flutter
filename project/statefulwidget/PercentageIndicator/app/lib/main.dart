import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import "package:percent_indicator/percent_indicator.dart";

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home: Scaffold(
        body: Center(
          child: CircularPercentIndicator(
            lineWidth: 10,
            percent: 0.5,
            animation: true,
            startAngle: 0,
            center: Text("!00"),
            radius:50 ),
        ),
      ),
    );
  }
}
