import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:roadwayreport/check_useroaddmin.dart';
import 'package:svg_flutter/svg_flutter.dart';
import 'login_screen.dart';

class Register extends StatefulWidget{
  const Register({super.key});

  @override
  State createState()=>RegisterState();
}

class RegisterState extends State{
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  
  final TextEditingController passwordController1 = TextEditingController();
  
  Future<void> _register() async {
    try {
      UserCredential userCredential= await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: usernameController.text,
        password: passwordController.text,
      );
      // await FirebaseFirestore.instance.collection('users').doc(userCredential.user!.uid).set({
      //   'email': usernameController.text,
      //   'isAdmin': data=='u'?false:true, // Set isAdmin to false for regular users
      // });
    Navigator.push(context, MaterialPageRoute(builder: (context)=>const Login()));
    } catch (e){
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Invalid Detail OR Network Error")));

    }
    setState(() {
      usernameController.clear();
passwordController.clear();
    });
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(top: 60,right: 40,left: 40),
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 40,),
              Center(child: SvgPicture.asset("assets/images/Group 77.svg")),
              const SizedBox(height: 60,),
              Text("Create your Account",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 16)),),
              const SizedBox(height: 25,),
              Container(
                padding: const EdgeInsets.all(15),
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(255,255,255,1),
                 borderRadius: BorderRadius.circular(8) ,
                 boxShadow: const [BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.15),
                    blurRadius: 10,
                    offset: Offset(0,3)
                 )]
                ),
                child: TextFormField(
                  cursorHeight: 20,
                  cursorColor: Colors.black,
                  decoration:  InputDecoration(
                    border: InputBorder.none,
                    hintText: "Name",
                    hintStyle: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.4),fontWeight: FontWeight.w400,fontSize: 12))
                  ),
                ),
              ),
              const SizedBox(height: 25,),
               Container(
                padding: const EdgeInsets.all(15),
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(255,255,255,1),
                 borderRadius: BorderRadius.circular(8) ,
                 boxShadow: const [BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.15),
                    blurRadius: 10,
                    offset: Offset(0,3)
                 )]
                ),
                child: TextFormField(
                  cursorHeight: 20,
                  cursorColor: Colors.black,
                  keyboardType: TextInputType.emailAddress,
                  controller: usernameController,
                  decoration:  InputDecoration(
                    border: InputBorder.none,
                    hintText: "EmailId",
                    hintStyle: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.4),fontWeight: FontWeight.w400,fontSize: 12))
                  ),
                ),
              ),
              const SizedBox(height: 25,),
               Container(
                padding: const EdgeInsets.all(15),
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(255,255,255,1),
                 borderRadius: BorderRadius.circular(8) ,
                 boxShadow: const [BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.15),
                    blurRadius: 10,
                    offset: Offset(0,3)
                 )]
                ),
                child: TextFormField(
                  cursorHeight: 20,
                  cursorColor: Colors.black,
                  controller: passwordController,
                  decoration:  InputDecoration(
                    border: InputBorder.none,
                    hintText: "Password",
                    hintStyle: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.4),fontWeight: FontWeight.w400,fontSize: 12))
                  ),
                ),
              ),
              const SizedBox(height: 25,),
               Container(
                padding: const EdgeInsets.all(15),
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(255,255,255,1),
                 borderRadius: BorderRadius.circular(8) ,
                 boxShadow: const [BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.15),
                    blurRadius: 10,
                    offset: Offset(0,3)
                 )]
                ),
                child: TextFormField(
                  cursorHeight: 20,
                  cursorColor: Colors.black,
                  controller: passwordController1,
                  decoration:  InputDecoration(
                    border: InputBorder.none,
                    hintText: "Confirm Password",
                    hintStyle: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.4),fontWeight: FontWeight.w400,fontSize: 12))
                  ),
                ),
              ),
              const SizedBox(height: 30,),
              GestureDetector(
                onTap: (){
                  if(usernameController.text.trim().isEmpty||passwordController.text.trim().isEmpty||passwordController1.text.trim().isEmpty||(passwordController.text.trim()!=passwordController1.text.trim())){
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Enter Valid Detail')));
                    }else{
                      _register();
                    
                    }
                },
                child: Container(
                  height: 50,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(14, 161, 125, 1),
                   borderRadius: BorderRadius.circular(8) ,
                   boxShadow: const [BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15),
                      blurRadius: 10,
                      offset: Offset(0,3)
                   )]
                  ),
                  child: Center(child: Text("Sign Up ",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(255,255,255,1),fontWeight: FontWeight.w500,fontSize: 15)),),),
                 
                ),
              ),
              const SizedBox(height: 50,),
               Center(child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                 children: [
                   Text("Already have an account? ",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.6),fontWeight: FontWeight.w400,fontSize: 12)),),
                    GestureDetector(
                      onTap: (){
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const Login()));
                      },
                      child: Text("Sign In",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(14, 161, 125, 1),fontWeight: FontWeight.w400,fontSize: 12)),)),
                 ],
               )),
            ],
          ),
        ),
    )
    );

  }
}