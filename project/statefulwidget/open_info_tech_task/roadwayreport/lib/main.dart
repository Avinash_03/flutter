import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:roadwayreport/select_sceern.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as p;
import 'check_useroaddmin.dart';
void main() async{
    WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  database =await openDatabase(
    p.join(await getDatabasesPath(),'roaddetail.DB'),
    version: 1,
    onCreate: (db,version){
      return db.execute('CREATE TABLE Test (id INTEGER PRIMARY KEY AUTOINCREMENT, location TEXT,image TEXT)');
    }
  );

  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Select()
    );
  }
}
