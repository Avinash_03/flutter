import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:roadwayreport/register_screen.dart';
import 'package:roadwayreport/splash_screen.dart';
import 'check_useroaddmin.dart';

class Login extends StatefulWidget{
  const Login({super.key});

  @override
  State createState()=>LoginState();
}

class LoginState extends State{

  Future signin(BuildContext context)async{
    try{
    UserCredential userCredential  = await FirebaseAuth.instance.signInWithEmailAndPassword(email: usernameController.text, password: passWordController.text);
    // DocumentSnapshot userSnapshot = await FirebaseFirestore.instance.collection('users').doc(userCredential.user!.uid).get();

      // Check if the user is an admin
      // bool isAdmin = userSnapshot['isAdmin'] ?? false;
      if(true){
        Navigator.push(context, MaterialPageRoute(builder: (context)=>const Splashscreen()));
      }else{
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Please Login Throw The User")));
      }
    } catch (e){
      print(e);
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Invalid Detail OR Network Error")));

    }
    setState(() {
      usernameController.clear();
passWordController.clear();
    });

  }
  TextEditingController usernameController= TextEditingController();
  TextEditingController passWordController= TextEditingController();
@override
void initState(){
  super.initState();

}

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(top: 60,right: 40,left: 40),
          child: Column(
            mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: 200,),
              Text(data=='u'?"Login to your Account":"Admin Login to your Account",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 16)),),
              const SizedBox(height: 25,),
              Container(
                padding: const EdgeInsets.all(15),
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(255,255,255,1),
                 borderRadius: BorderRadius.circular(8) ,
                 boxShadow: const [BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.15),
                    blurRadius: 10,
                    offset: Offset(0,3)
                 )]
                ),
                child: TextFormField(
                  // key:check,
                  cursorHeight: 20,
                  cursorColor: Colors.black,
                  keyboardType: TextInputType.emailAddress,
                  controller: usernameController,
                  decoration:  InputDecoration(
                    
                    border: InputBorder.none,
                    hintText: "Enter EmailId",
                    hintStyle: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.4),fontWeight: FontWeight.w400,fontSize: 12))
                  ),
                 
                ),
              ),
              const SizedBox(height: 25,),
               Container(
                padding: const EdgeInsets.all(15),
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(255,255,255,1),
                 borderRadius: BorderRadius.circular(8) ,
                 boxShadow: const [BoxShadow( 
                    color: Color.fromRGBO(0, 0, 0, 0.15),
                    blurRadius: 10,
                    offset: Offset(0,3)
                 )]
                ),
                child: TextFormField(
                  // key: check,
                  cursorHeight: 20,
                  cursorColor: Colors.black,
                  mouseCursor:SystemMouseCursors.basic,
                  controller: passWordController,
                  decoration:  InputDecoration(
                  
                    border: InputBorder.none,
                    hintText: "Password",
                    hintStyle: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.4),fontWeight: FontWeight.w400,fontSize: 12))
                  ),
                  
                ),
              ),
              const SizedBox(height: 30,),
              GestureDetector(
                onTap:(){
                  setState(() {
                  
                    if(usernameController.text.isEmpty||passWordController.text.isEmpty){
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Enter Valid Detail')));
                    }else{
                      signin(context);
                    
                    }
                  });
                 
                },
                child: Container(
                  height: 50,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(14, 161, 125, 1),
                   borderRadius: BorderRadius.circular(8) ,
                   boxShadow: const [BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15),
                      blurRadius: 10,
                      offset: Offset(0,3)
                   )]
                  ),
                  child: Center(child: Text("Login",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(255,255,255,1),fontWeight: FontWeight.w500,fontSize: 15)),),),
                 
                ),
              ),
              const SizedBox(height: 50,),
               Center(child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                 children: [
                   Text(data=='u'?"Don’t have an account? ":'',style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.6),fontWeight: FontWeight.w400,fontSize: 12)),),
                    GestureDetector(
                      onTap: (){
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const Register()));
                      },
                      child: Text(data=='u'?"Sign up":'',style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(14, 161, 125, 1),fontWeight: FontWeight.w400,fontSize: 12)),)),
                 ],
               )),
            ],
          ),
        ),
    )
    );

  }
}