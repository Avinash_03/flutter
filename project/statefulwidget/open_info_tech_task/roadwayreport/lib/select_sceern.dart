
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:roadwayreport/check_useroaddmin.dart';
import 'package:roadwayreport/login_screen.dart';

class Select extends StatefulWidget {
  const Select({super.key});

  @override
  State<Select> createState() => _SelectState();
}

class _SelectState extends State<Select> {

  //String? data;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(251, 247, 248, 1),
      body: Center(
        child: Container(
          height: 400,
          margin:const  EdgeInsets.all(10),
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: const Color.fromRGBO(255, 255, 255, 1),
            borderRadius: BorderRadius.circular(10),
            boxShadow: const [BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.15),
                    blurRadius: 10,
                    offset: Offset(0,3)
                 )]
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Select One",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 1),fontWeight: FontWeight.w700,fontSize: 20)),),
              const SizedBox(height: 50,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    onTap: (){
                      //Check('a');
                      data='a';
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>const Login()));
                    },
                    child: Container(
                      height: 242,
                      width: 150,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(14)),
                        gradient: LinearGradient(colors: [
                          Color.fromRGBO(0, 77, 228, 1),
                          Color.fromRGBO(1, 47, 135, 1)
                        ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
                      ),
                       child:Center(child: Text("Admin",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(255,255, 255, 1),fontWeight: FontWeight.w700,fontSize: 20)),)),
                    ),
                  ),
                  const SizedBox(width: 10,),
                  GestureDetector(
                    onTap: (){
                      // Check('u');
                      data='u';
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>const Login()));
                    },
                    child: Container(
                      height: 242,
                      width: 150,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(14)),
                        gradient: LinearGradient(colors: [
Color.fromRGBO(197, 4, 98, 1),
                          Color.fromRGBO(80, 3, 112, 1)
                        ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
                      ),
                      child:Center(child: Text("User",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(255,255, 255, 1),fontWeight: FontWeight.w700,fontSize: 20)),)),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
