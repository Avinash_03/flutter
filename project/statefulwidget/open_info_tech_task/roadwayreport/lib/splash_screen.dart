import 'dart:async';

import "package:flutter/material.dart";
import 'package:roadwayreport/admin_screen.dart';
import 'package:roadwayreport/check_useroaddmin.dart';
import 'package:roadwayreport/user_screen.dart';
import 'package:svg_flutter/svg_flutter.dart';

class Splashscreen extends StatefulWidget{
  const Splashscreen({super.key});
  @override
  State createState()=> SplashscreenState();
}
class SplashscreenState extends State{
@override
void initState(){
  super.initState();
  Timer(const Duration(seconds: 2), ()=>
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>data=='u'?const User():const AdminScreen()))

  );
}
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SvgPicture.asset('assets/undraw_navigator_a479 1.svg'),
      ),
    );
  }
}
