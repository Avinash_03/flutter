import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:roadwayreport/check_useroaddmin.dart';
import 'package:roadwayreport/modelfile.dart';
import 'package:sqflite/sqflite.dart';
import 'package:svg_flutter/svg.dart';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:uuid/uuid.dart';

class User extends StatefulWidget {
  const User({super.key});

  @override
  State<User> createState() => _UserState();
}

class _UserState extends State<User> {
  TextEditingController locationController = TextEditingController();
  File? _image;
  String? _location;
  Future _pickImage() async {
    final pickedFile =
        await ImagePicker().pickImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print("No Image Selected");
      }
    });
  }

  Future _takePicture() async {
    try {
      final pickedFile =
          await ImagePicker().pickImage(source: ImageSource.camera);
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
          print(_image);
        } else {
          print("No Image Selected");
        }
      });
    } catch (e) {
      print(e);
    }
  }

  // Future<void> _uploadImage() async {
  //   try {
  //     // Check if image and location are selected
  //     if (_image == null || locationController.text.trim().isEmpty) {
  //       ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
  //           content: Text('Please select an image and enter a location')));
  //       return;
  //     }

  //     // Show a loading indicator or disable the upload button to indicate the upload is in progress

  //     // Upload image to Firebase Storage
  //     UploadTask uploadTask = FirebaseStorage.instance
  //         .ref()
  //         .child('images')
  //         .child(Uuid().v1())
  //         .putFile(_image!);
  //     TaskSnapshot taskSnapshot = await uploadTask;
  //     String imageUrl = await taskSnapshot.ref.getDownloadURL();

  //     // Add image URL and location to Firestore
  //     await FirebaseFirestore.instance.collection('posts').add({
  //       'imageUrl': imageUrl,
  //       'location': locationController.text.trim(),
  //       'timestamp': Timestamp.now(),
  //     });

  //     // Clear the form fields
  //     setState(() {
  //       _image = null;
  //       locationController.clear();
  //     });

  //     // Show success message to the user
  //     ScaffoldMessenger.of(context).showSnackBar(
  //         const SnackBar(content: Text('Image uploaded successfully')));
  //   } catch (e) {
  //     // Handle errors and show appropriate feedback to the user
  //     print('Error uploading image: $e');
  //     ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
  //         content: Text('An error occurred while uploading the image')));
  //   }
  // }
  Future<void> _uploadImage(User1 data)async{
      try{
        final localDB = await database;
  await localDB.insert(
    'Test',
    data.getmap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
  setState(() {
    _image=null;
    locationController.clear();
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Details Upload Successful")));
  });
      }catch(e){
          print("object$e");
      }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(height: 150,),
          Container(
            margin: const EdgeInsets.all(20),
            height: 500,
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                boxShadow: const [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.2),
                      offset: Offset(0, 3),
                      blurRadius: 15)
                ]),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // const Spacer(),
                SizedBox(
                    height: 200,
                    width: 200,
                    child: _image == null
                        ? SvgPicture.asset("assets/Group 42.svg")
                        : Image.file(_image!)),
                const SizedBox(
                  height: 0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        _pickImage();
                      },
                      child: Container(
                        padding: const EdgeInsets.only(right: 10, left: 10),
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Center(
                            child: Image.asset(
                          'assets/photo.png',
                          height: 40,
                        )),
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        _takePicture();
                      },
                      child: Container(
                        height: 50,
                        padding: const EdgeInsets.only(right: 10, left: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Center(
                            child: Image.asset(
                          'assets/joede--cam-icon.png',
                          height: 45,
                        )),
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  padding: const EdgeInsets.all(15),
                  height: 50,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: const [
                        BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.15),
                            blurRadius: 10,
                            offset: Offset(0, 3))
                      ]),
                  child: TextFormField(
                    controller: locationController,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Enter Location",
                        hintStyle: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                                color: Color.fromRGBO(0, 0, 0, 0.4),
                                fontWeight: FontWeight.w400,
                                fontSize: 12))),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    if (_image != null &&
                        locationController.text.trim().isNotEmpty) {
                      _uploadImage(
                        User1(image: '$_image', location: locationController.text)
                      );
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          content: Text('Invalid Location or Photo')));
                    }
                  },
                  child: Container(
                    padding: const EdgeInsets.only(right: 10, left: 10),
                    height: 50,
                    width: 200,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: const Color.fromRGBO(14, 161, 125, 1)),
                    child: Center(
                        child: Text(
                      "Upload Details",
                      style: GoogleFonts.poppins(
                          textStyle: const TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontWeight: FontWeight.w500,
                              fontSize: 12)),
                    )),
                  ),
                ),
                // const Spacer()
              ],
            ),
          ),
          // const Spacer()
        ],
      ),
    ));
  }
}
