import 'package:flutter/material.dart';

void main() {
  runApp(const MyHomePage());
}


class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State createState() { 

    print("In CreateState");
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State {

void initState(){
  super.initState;
  print("In initState");
}

void didChangeDependencies(){
  super.didChangeDependencies;
  print("In didChangeDependencies");
}

void deactivate(){
  super.deactivate;
  print("In deactive");
}

void dispose(){
  super.dispose;
  print("In dispose");
}
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      print("In SetState");
     
     if(_counter<=5)
      _counter++;
      else
      inc=false;
    });
  }
  void _decrementCounter() {
    setState(() {
      print("In SetState");
     if(_counter>0)
      _counter--;
    });
  }
bool inc=true;
Scaffold checkno(){
  if(inc==true){
    return Scaffold(
        appBar: AppBar(
         
          title:const Text("Counter"),
        ),
        body: Center(
          
          child: Column(
          
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text(
                'You have pushed the button this many times:',
              ),
              Text(
                '$_counter',
                style: Theme.of(context).textTheme.headlineMedium,
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: _incrementCounter,
          tooltip: 'Increment',
          child: const Icon(Icons.add),
        ), // This trailing comma makes auto-formatting nicer for build methods.
      );

  
  }else{
    return     Scaffold(
        appBar: AppBar(
         
          title:const Text("Counter"),
        ),
        body: Center(
          
          child: Column(
          
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text(
                'You have pushed the button this many times:',
              ),
              Text(
                '$_counter',
                style: Theme.of(context).textTheme.headlineMedium,
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: _decrementCounter,
          tooltip: 'Increment',
          child: const Icon(Icons.add),
        ), // This trailing comma makes auto-formatting nicer for build methods.
      );
  
  }
}

  @override
  Widget build(BuildContext context) {
    print("IN BUILD");
    return MaterialApp(
      home: checkno()
    );
  }
}
