import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyLogin(),
    );
  }
}

class MyLogin extends StatefulWidget {
  const MyLogin({super.key});

  @override
  State createState() => _MyLoginState();
}

class _MyLoginState extends State {
  final TextEditingController _idcontroller = TextEditingController();
  final TextEditingController _passwordcontroller = TextEditingController();

  // GlobalKey<FormFieldState> idKey = GlobalKey();
  // GlobalKey<FormFieldState> passKey = GlobalKey();
GlobalKey<FormState> loginkey=GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login Page"),
      ),
      body: Form(
        key: loginkey,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                //key: idKey,             key required when use formfiledstate key
                controller: _idcontroller,
                decoration: InputDecoration(
                  hintText: "Enter UserId",
                  border:
                      OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                ),
                validator: (value) {
                  if(value!.isEmpty){
                    return "Enter Valid Id";
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                //key: passKey,
                controller: _passwordcontroller,
                decoration: InputDecoration(
                  suffix: const Icon(
                    Icons.remove_red_eye_outlined,
                    size: 20,
                  ),
                  hintText: "Enter Password",
                  border:
                      OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                  
                ),
                validator: (value) {
                  if(value!.isEmpty){
                    return "Enter Valid PassWord";
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 10,
              ),
              ElevatedButton(onPressed: () {
                setState(() {
                  // bool validid =idKey.currentState!.validate();
                  // bool validpass=passKey.currentState!.validate();
                  loginkey.currentState!.validate();
                });
              
              }, child: const Text("Login"))
            ],
          ),
        ),
      ),
    );
  }
}
