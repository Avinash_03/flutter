import 'package:flutter/material.dart';

class Mycolors {
  static Color primaryFontColor = const Color(0xFF2C3D63);
  static Color skyblue= const Color(0xFF33A1CC);
  static Color yellow = const Color(0xFFDCB223);
  static Color green = const Color(0xFF31CE95);
  static Color darkgreen= const Color(0xFF0F8181);
  static Color darkorange = const Color.fromARGB(255, 251, 67, 0);
  static Color blue= const Color(0xFF234DDC);
  static Color pink = const Color(0xFFCE316A);
  static Color liteorange = const Color(0xFFFE804E);
}