import "package:flutter/material.dart";
import 'package:svg_flutter/svg_flutter.dart';
import 'colors.dart';
import 'package:google_fonts/google_fonts.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[50],
      appBar: AppBar(
        backgroundColor: Colors.grey[50],
        leading: Row(
          children: [
            const SizedBox(
              width: 15,
            ),
            Container(
                padding: const EdgeInsets.all(12),
                height: 40,
                width: 40,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.06),
                          blurRadius: 6,
                          offset: Offset(0, 3))
                    ]),
                child: SvgPicture.asset(
                  "assets/icons/threeline.svg",
                )),
          ],
        ),
        actions: [
          Container(
            padding: const EdgeInsets.all(5),
            height: 40,
            width: 40,
            decoration: const BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.06),
                      blurRadius: 6,
                      offset: Offset(0, 3))
                ]),
            child: SvgPicture.asset("assets/icons/fav.svg"),
          ),
          const SizedBox(
            width: 10,
          ),
          Stack(children: [
            Container(
              // padding: const EdgeInsets.all(8),
              height: 40,
              width: 40,
              decoration: const BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.06),
                        blurRadius: 6,
                        offset: Offset(0, 3))
                  ]),
              child: Stack(children: [
                Positioned(
                  top: 10,
                  left: 10,
                  child: SvgPicture.asset("assets/icons/notification.svg"),
                ),
                Positioned(
                  bottom: 15,
                  left: 19,
                  child: Container(
                    padding: const EdgeInsets.all(4),
                  
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Mycolors.darkorange,
                    ),
                    child: Text(
                      "2",
                      style: GoogleFonts.roboto(
                          textStyle: const TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                              fontWeight: FontWeight.w500)),
                    ),
                  ),
                )
              ]),
            ),
          ]),
          const SizedBox(
            width: 10,
          ),
          Container(
            padding: const EdgeInsets.all(4),
            height: 40,
            width: 40,
            decoration: const BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.06),
                      blurRadius: 6,
                      offset: Offset(0, 3))
                ]),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Image.asset(
                  "assets/images/profile.png",
                  fit: BoxFit.cover,
                )),
          ),
          const SizedBox(
            width: 10,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 15),
          child: Column(
            children: [
              const SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Welcome,",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Mycolors.primaryFontColor,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500)),
                          ),
                          Text(
                            " Mypcot !!",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Mycolors.primaryFontColor,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600)),
                          ),
                        ],
                      ),
                      Text(
                        "here is your dashboard....",
                        style: GoogleFonts.roboto(
                            textStyle: TextStyle(
                                color: Mycolors.primaryFontColor,
                                fontSize: 12,
                                fontWeight: FontWeight.w500)),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(15),
                        height: 60,
                        width: 60,
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.06),
                                  blurRadius: 6,
                                  offset: Offset(0, 3))
                            ]),
                        child: SvgPicture.asset("assets/icons/search.svg"),
                      ),
                      const SizedBox(
                        width: 10,
                      )
                    ],
                  )
                ],
              ),
              const SizedBox(
                height: 25,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      child: Stack(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(top: 10, right: 20),
                            // height: 200,
                            // width: 300,
                            padding: const EdgeInsets.only(
                                right: 50, top: 10, left: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: Mycolors.skyblue,
                                borderRadius: BorderRadius.circular(10)),
                            child: Row(
                              children: [
                                const SizedBox(
                                  width: 15,
                                ),
                                Column(
                                  children: [
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    Container(
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white),
                                      child: SvgPicture.asset(
                                          "assets/icons/orders-illustration-image.svg"),
                                    ),
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    Container(
                                      padding: const EdgeInsets.only(
                                          right: 25,
                                          left: 25,
                                          top: 5,
                                          bottom: 5),
                                      decoration: BoxDecoration(
                                          color: Mycolors.darkorange,
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Text(
                                        "Order",
                                        style: GoogleFonts.roboto(
                                            textStyle: const TextStyle(
                                                color: Colors.white,
                                                fontSize: 12,
                                                fontWeight: FontWeight.w500)),
                                      ),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                Column(
                                  children: [
                                    const SizedBox(
                                      height: 50,
                                    ),
                                    Stack(
                                      children: [
                                        Container(
                                          padding: const EdgeInsets.all(10),
                                          // height: 50,
                                          // width: 100,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    "02 ",
                                                    style: GoogleFonts.roboto(
                                                        textStyle: TextStyle(
                                                            color: Mycolors
                                                                .primaryFontColor,
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w700)),
                                                  ),
                                                  Text(
                                                    "pending",
                                                    style: GoogleFonts.roboto(
                                                        textStyle: TextStyle(
                                                            color: Mycolors
                                                                .primaryFontColor,
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500)),
                                                  ),
                                                ],
                                              ),
                                              Text(
                                                "Orders from",
                                                style: GoogleFonts.roboto(
                                                    textStyle: TextStyle(
                                                        color: Mycolors
                                                            .primaryFontColor,
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.w500)),
                                              ),
                                              const SizedBox(
                                                height: 10,
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          Positioned(
                              left: MediaQuery.of(context).size.width*0.45,
                              child: Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Mycolors.liteorange,
                                ),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          "You have ",
                                          style: GoogleFonts.roboto(
                                              textStyle: const TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                        Text(
                                          "3",
                                          style: GoogleFonts.roboto(
                                              textStyle: const TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w700)),
                                        ),
                                        Text(
                                          " active",
                                          style: GoogleFonts.roboto(
                                              textStyle: const TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      "Orders from",
                                      style: GoogleFonts.roboto(
                                          textStyle: const TextStyle(
                                              color: Colors.white,
                                              fontSize: 12,
                                              fontWeight: FontWeight.w500)),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    )
                                  ],
                                ),
                              )),
                          Positioned(
                              top: MediaQuery.of(context).size.height*0.060,
                              left: MediaQuery.of(context).size.width*0.5,
                              child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Mycolors.liteorange),
                                  padding: const EdgeInsets.all(2),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.asset(
                                        "assets/images/image1.png",
                                        fit: BoxFit.cover,
                                      )))),
                          Positioned(
                              top:MediaQuery.of(context).size.height*0.060,
                              left: MediaQuery.of(context).size.width*0.57,
                              child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Mycolors.liteorange),
                                  padding: const EdgeInsets.all(2),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.asset(
                                        "assets/images/image2.png",
                                        fit: BoxFit.cover,
                                      )))),
                          Positioned(
                              top:MediaQuery.of(context).size.height*0.060,
                              left: MediaQuery.of(context).size.width*0.64,
                              child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Mycolors.liteorange),
                                  padding: const EdgeInsets.all(2),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.asset(
                                        "assets/images/image3.png",
                                        fit: BoxFit.cover,
                                      )))),
                          Positioned(
                              top: MediaQuery.of(context).size.width*0.42,
                              left: MediaQuery.of(context).size.width*0.51,
                              child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Mycolors.liteorange),
                                  padding: const EdgeInsets.all(2),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.asset(
                                        "assets/images/alia.png",
                                        fit: BoxFit.cover,
                                      )))),
                          Positioned(
                              top: MediaQuery.of(context).size.width*0.42,
                              left:  MediaQuery.of(context).size.width*0.58,
                              child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Mycolors.liteorange),
                                  padding: const EdgeInsets.all(2),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.asset(
                                        "assets/images/hukka.png",
                                        fit: BoxFit.cover,
                                      )))),
                        ],
                      ),
                    ),
                    SizedBox(
                      child: Stack(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(top: 10, right: 20),
                            // height: 200,
                            // width: 300,
                            padding: const EdgeInsets.only(
                                right: 35, top: 17, left: 10, bottom: 17),
                            decoration: BoxDecoration(
                                color: Mycolors.yellow,
                                borderRadius: BorderRadius.circular(10)),
                            child: Row(
                              children: [
                                const SizedBox(
                                  width: 15,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    Container(
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white),
                                      child: SvgPicture.asset(
                                          "assets/icons/subscriptions-illustration-image.svg"),
                                    ),
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    Container(
                                      padding: const EdgeInsets.only(
                                          right: 25,
                                          left: 25,
                                          top: 5,
                                          bottom: 5),
                                      decoration: BoxDecoration(
                                          color: Mycolors.blue,
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Text(
                                        "Subscription",
                                        style: GoogleFonts.roboto(
                                            textStyle: const TextStyle(
                                                color: Colors.white,
                                                fontSize: 12,
                                                fontWeight: FontWeight.w500)),
                                      ),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                Column(
                                  children: [
                                    const SizedBox(
                                      height: 30,
                                    ),
                                    Stack(
                                      children: [
                                        Container(
                                          padding: const EdgeInsets.only(
                                              left: 5, right: 5, top: 10),
                                          // height: 50,
                                          // width: 100,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "10 ",
                                                    style: GoogleFonts.roboto(
                                                        textStyle: TextStyle(
                                                            color: Mycolors
                                                                .primaryFontColor,
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w700)),
                                                  ),
                                                  Text(
                                                    "Active",
                                                    style: GoogleFonts.roboto(
                                                        textStyle: TextStyle(
                                                            color: Mycolors
                                                                .primaryFontColor,
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500)),
                                                  ),
                                                ],
                                              ),
                                              Text(
                                                "Subscriptions",
                                                style: GoogleFonts.roboto(
                                                    textStyle: TextStyle(
                                                        color: Mycolors
                                                            .primaryFontColor,
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.w500)),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      children: [
                                        const SizedBox(
                                          width: 35,
                                        ),
                                        Container(
                                          padding: const EdgeInsets.only(
                                              left: 5, right: 5, top: 5),
                                          // height: 50,
                                          // width: 100,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    "199 ",
                                                    style: GoogleFonts.roboto(
                                                        textStyle: TextStyle(
                                                            color: Mycolors
                                                                .primaryFontColor,
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w700)),
                                                  ),
                                                  Text(
                                                    "pending",
                                                    style: GoogleFonts.roboto(
                                                        textStyle: TextStyle(
                                                            color: Mycolors
                                                                .primaryFontColor,
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500)),
                                                  ),
                                                ],
                                              ),
                                              Text(
                                                "Deliveries",
                                                style: GoogleFonts.roboto(
                                                    textStyle: TextStyle(
                                                        color: Mycolors
                                                            .primaryFontColor,
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.w500)),
                                              ),
                                              const SizedBox(
                                                height: 10,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          Positioned(
                              left: 170,
                              child: Container(
                                padding: const EdgeInsets.only(
                                    top: 5, left: 15, bottom: 15, right: 15),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Mycolors.blue,
                                ),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          "03",
                                          style: GoogleFonts.roboto(
                                              textStyle: const TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w700)),
                                        ),
                                        Text(
                                          " deliveries",
                                          style: GoogleFonts.roboto(
                                              textStyle: const TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    )
                                  ],
                                ),
                              )),
                          Positioned(
                              top: 35,
                              left: 185,
                              child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Mycolors.blue),
                                  padding: const EdgeInsets.all(2),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.asset(
                                        "assets/images/image1.png",
                                        fit: BoxFit.cover,
                                      )))),
                          Positioned(
                              top: 35,
                              left: 210,
                              child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Mycolors.blue),
                                  padding: const EdgeInsets.all(2),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.asset(
                                        "assets/images/image2.png",
                                        fit: BoxFit.cover,
                                      )))),
                          Positioned(
                              top: 35,
                              left: 235,
                              child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Mycolors.blue),
                                  padding: const EdgeInsets.all(2),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.asset(
                                        "assets/images/image3.png",
                                        fit: BoxFit.cover,
                                      )))),
                        ],
                      ),
                    ),
                    SizedBox(
                      child: Stack(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(top: 10, right: 20),
                            // height: 200,
                            // width: 300,
                            padding: const EdgeInsets.only(
                                right: 30, top: 10, left: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: Mycolors.green,
                                borderRadius: BorderRadius.circular(10)),
                            child: Row(
                              children: [
                                const SizedBox(
                                  width: 15,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    Container(
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white),
                                      child: SvgPicture.asset(
                                          "assets/icons/customers-illustration-image.svg"),
                                    ),
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    Container(
                                      padding: const EdgeInsets.only(
                                          right: 25,
                                          left: 25,
                                          top: 5,
                                          bottom: 5),
                                      decoration: BoxDecoration(
                                          color: Mycolors.pink,
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Text(
                                        "View Customers",
                                        style: GoogleFonts.roboto(
                                            textStyle: const TextStyle(
                                                color: Colors.white,
                                                fontSize: 12,
                                                fontWeight: FontWeight.w500)),
                                      ),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Column(
                                  children: [
                                    const SizedBox(
                                      height: 45,
                                    ),
                                    Stack(
                                      children: [
                                        const SizedBox(
                                          // width: 30,
                                        ),
                                        Container(
                                          padding: const EdgeInsets.only(top:10,left: 30,),
                                          // height: 50,
                                          // width: 100,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Row(
                                                    children: [
                                                      Text(
                                                        "1.8% ",
                                                        style: GoogleFonts.roboto(
                                                            textStyle: TextStyle(
                                                                color: Mycolors
                                                                    .primaryFontColor,
                                                                fontSize: 16,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700)),
                                                      ),
                                                      const SizedBox(
                                                        width: 10,
                                                      ),
                                                      Icon(
                                                        Icons
                                                            .arrow_upward_rounded,
                                                        color: Mycolors.green,
                                                      )
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              const SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                children: [
                                                  const SizedBox(width: 10,),
                                                  Image.asset("assets/images/Screenshot from 2024-05-05 00-33-12.png",height: 25,),
                                                ],
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Container(
                                      padding: const EdgeInsets.only(
                                          top: 10,
                                          bottom: 10,
                                          right: 42,
                                          left: 10),
                                      // height: 50,
                                      // width: 100,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Column(
                                        children: [
                                          Row(
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    "10 ",
                                                    style: GoogleFonts.roboto(
                                                        textStyle: TextStyle(
                                                            color: Mycolors
                                                                .primaryFontColor,
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w700)),
                                                  ),
                                                  Text(
                                                    " active",
                                                    style: GoogleFonts.roboto(
                                                        textStyle: TextStyle(
                                                            color: Mycolors
                                                                .primaryFontColor,
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500)),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                          Text(
                                            "Customers",
                                            style: GoogleFonts.roboto(
                                                textStyle: TextStyle(
                                                    color: Mycolors
                                                        .primaryFontColor,
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.w500)),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          Positioned(
                              left: 170,
                              child: Container(
                                padding: const EdgeInsets.only(
                                    right: 5, top: 5, left: 5, bottom: 20),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Mycolors.pink,
                                ),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          "15",
                                          style: GoogleFonts.roboto(
                                              textStyle: const TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w700)),
                                        ),
                                        Text(
                                          " New customers",
                                          style: GoogleFonts.roboto(
                                              textStyle: const TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    )
                                  ],
                                ),
                              )),
                          Positioned(
                              top: 30,
                              left: 185,
                              child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Mycolors.skyblue),
                                  padding: const EdgeInsets.all(2),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.asset(
                                        "assets/images/image1.png",
                                        fit: BoxFit.cover,
                                      )))),
                          Positioned(
                              top: 30,
                              left: 210,
                              child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Mycolors.skyblue),
                                  padding: const EdgeInsets.all(2),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.asset(
                                        "assets/images/image2.png",
                                        fit: BoxFit.cover,
                                      )))),
                          Positioned(
                              top: 30,
                              left: 235,
                              child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Mycolors.skyblue),
                                  padding: const EdgeInsets.all(2),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.asset(
                                        "assets/images/image3.png",
                                        fit: BoxFit.cover,
                                      )))),
                          Positioned(
                              top: 40,
                              left: 265,
                              child: Container(
                                padding: const EdgeInsets.all(2),
                                height: 20,
                                width: 20,
                                decoration: const BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle),
                                child: const Icon(
                                  Icons.add,
                                  size: 15,
                                ),
                              )),
                          Positioned(
                              top: 160,
                              left: 272,
                              child: Stack(
                                children: [
                                  Container(
                                      width: 28,
                                      height: 28,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Mycolors.skyblue),
                                      padding: const EdgeInsets.all(2),
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          child: Image.asset(
                                            "assets/images/image1.png",
                                            fit: BoxFit.cover,
                                          ))),
                                  Positioned(
                                      top: 20,
                                      left: 11,
                                      child: Icon(
                                        Icons.circle,
                                        color: Mycolors.green,
                                        size: 8,
                                      ))
                                ],
                              )),
                          Positioned(
                              top: 160,
                              left: 290,
                              child: Stack(
                                children: [
                                  Container(
                                      width: 28,
                                      height: 28,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Mycolors.skyblue),
                                      padding: const EdgeInsets.all(2),
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          child: Image.asset(
                                            "assets/images/image2.png",
                                            fit: BoxFit.cover,
                                          ))),
                                  Positioned(
                                      top: 20,
                                      left: 14,
                                      child: Icon(
                                        Icons.circle,
                                        color: Mycolors.green,
                                        size: 8,
                                      ))
                                ],
                              )),
                          Positioned(
                              top: 160,
                              left: 310,
                              child: Stack(
                                children: [
                                  Container(
                                      width: 28,
                                      height: 28,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Mycolors.skyblue),
                                      padding: const EdgeInsets.all(2),
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          child: Image.asset(
                                            "assets/images/image3.png",
                                            fit: BoxFit.cover,
                                          ))),
                                  Positioned(
                                      top: 20,
                                      left: 14,
                                      child: Icon(
                                        Icons.circle,
                                        color: Mycolors.green,
                                        size: 8,
                                      ))
                                ],
                              )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "January,23 2021",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Mycolors.primaryFontColor,
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500)),
                        ),
                        Text(
                          "Today",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Mycolors.primaryFontColor,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Container(
                      padding: const EdgeInsets.only(
                          left: 10, right: 5, top: 5, bottom: 5),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: const [
                            BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.06),
                                blurRadius: 6,
                                offset: Offset(0, 3))
                          ]),
                      child: Row(
                        children: [
                          Text(
                            "TIMELINE",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Mycolors.primaryFontColor,
                                    fontSize: 10,
                                    fontWeight: FontWeight.w600)),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.arrow_drop_down,
                            color: Mycolors.primaryFontColor,
                            size: 28,
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: const [
                            BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.06),
                                blurRadius: 6,
                                offset: Offset(0, 3))
                          ]),
                      child: Row(
                        children: [
                          SvgPicture.asset("assets/icons/calender.svg"),
                          const SizedBox(
                            width: 10,
                          ),
                          Text(
                            "JAN,2021",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Mycolors.primaryFontColor,
                                    fontSize: 10,
                                    fontWeight: FontWeight.w600)),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Column(
                      children: [
                        Text(
                          "MON",
                          style: GoogleFonts.roboto(
                              textStyle: const TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 0.2),
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600)),
                        ),
                        Text(
                          "20",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Mycolors.primaryFontColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w700)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Column(
                      children: [
                        Text(
                          "TUE",
                          style: GoogleFonts.roboto(
                              textStyle: const TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 0.2),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600)),
                        ),
                        Text(
                          "21",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Mycolors.primaryFontColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w700)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Column(
                      children: [
                        Text(
                          "WED",
                          style: GoogleFonts.roboto(
                              textStyle: const TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 0.2),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600)),
                        ),
                        Text(
                          "22",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Mycolors.primaryFontColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Column(
                      children: [
                        Text(
                          "THU",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Mycolors.darkgreen,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600)),
                        ),
                        Text(
                          "23",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Mycolors.darkgreen,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w700)),
                        ),
                        Container(
                          height: 7,
                          width: 7,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Mycolors.darkgreen),
                        )
                      ],
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Column(
                      children: [
                        Text(
                          "FRI",
                          style: GoogleFonts.roboto(
                              textStyle: const TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 0.2),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600)),
                        ),
                        Text(
                          "24",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Mycolors.primaryFontColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w700)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Column(
                      children: [
                        Text(
                          "SAT",
                          style: GoogleFonts.roboto(
                              textStyle: const TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 0.2),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600)),
                        ),
                        Text(
                          "25",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Mycolors.primaryFontColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w700)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Column(
                      children: [
                        Text(
                          "SUN",
                          style: GoogleFonts.roboto(
                              textStyle: const TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 0.2),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600)),
                        ),
                        Text(
                          "26",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Mycolors.primaryFontColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w700)),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                margin: const EdgeInsets.only(right: 15),
                padding: const EdgeInsets.all(15),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: const [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.06),
                          blurRadius: 6,
                          offset: Offset(0, 3)),
                    ]),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 200,
                          child: Text(
                            "New order created",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Mycolors.primaryFontColor,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700)),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          width: 200,
                          child: Text(
                            "New order created with Order",
                            style: GoogleFonts.roboto(
                                textStyle: TextStyle(
                                    color: Mycolors.primaryFontColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700)),
                          ),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Text(
                          "09:00 AM",
                          style: GoogleFonts.roboto(
                              textStyle: TextStyle(
                                  color: Mycolors.liteorange,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w700)),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Icon(
                          Icons.arrow_right_alt_sharp,
                          color: Mycolors.liteorange,
                          size: 30,
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(15),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Mycolors.liteorange),
                          child: SvgPicture.asset(
                              "assets/icons/newordercreated.svg"),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
