import 'package:flutter/material.dart';

class Home extends StatefulWidget{
  const Home({super.key});

  @override
  State createState()=> HomeState();
}

class HomeState extends State{

  var _javacnt=0;
  var _fluttercnt=0;
  void increamentcntjava(){
    setState(() {
      _javacnt++;
    });

  }
  void increamentcntflutter(){
    setState(() {
      _fluttercnt++;
    });

  }
  @override
  Widget build(BuildContext context){

    return Scaffold(
        body:Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap:() {
                    increamentcntjava();
                  },
                  child: Container(
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.blue
                    ),
                    child: const Center(child: Text("Java",style:  TextStyle(color: Colors.white),)),
                  ),
                ),
                const SizedBox(width: 30,),
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.blue
                    ),
                  child:  Center(child: Text("$_javacnt",style: const TextStyle(color: Colors.white),)))
              ],
            ),
            const SizedBox(height: 50,),
             Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap:() {
                    increamentcntflutter();
                  },
                  child: Container(
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.red
                    ),
                    child: const Center(child: Text("Flutter",style:  TextStyle(color: Colors.white),)),
                  ),
                ),
                const SizedBox(width: 30,),
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.red
                    ),
                  child:  Center(child: Text("$_fluttercnt",style: const TextStyle(color: Colors.white),)))
              ],
            ),
          ],
        )
      );
  }
}