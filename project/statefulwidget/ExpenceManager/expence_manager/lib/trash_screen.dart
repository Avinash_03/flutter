import "package:expence_manager/drawer.dart";
import "package:flutter/material.dart";
import "package:google_fonts/google_fonts.dart";

class Trash extends StatefulWidget {
  const Trash({super.key});

  @override
  State<Trash> createState() => _TrashState();
}

class _TrashState extends State<Trash> {
  @override
  Widget build(BuildContext context) {
    return   Scaffold(
      appBar: AppBar(
        
        title:  Text("Trash",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w500,fontSize: 16))),
      ),
      body: ListView.builder(
        itemCount: 5,
        itemBuilder:(context,index) {
          return Container(
            //height: 60,
           // width: double.infinity,
            decoration: const BoxDecoration(
              border: Border(bottom: BorderSide(color: Color.fromRGBO(206, 206, 206, 1)))
            ),
            child: Row(children: [
              const SizedBox(width: 10,),
              // SvgPicture.asset('assets/images/Group 67.svg'),
 const Icon(Icons.remove_circle,color: Color.fromRGBO(204, 210, 227, 1),),
              const SizedBox(width: 10,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Medicin",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w400,fontSize: 15))),
                      const SizedBox(width:200 ,),
                    
                    
                     
                    ],
                  ),
              Text("Lorem Ipsum is simply dummy text of the ",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w400,fontSize: 10)))
              ,Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  const SizedBox(width: 200,),
                   Text("3 June | 11:50 AM",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0,0,0, 0.6),fontWeight: FontWeight.w400,fontSize: 10))),
                                ],
              ),
              const SizedBox(height: 5,)
              ],)
            ],),
          );
        }
      ),
      
     drawer:const  Showdrawer()
    );
  }
}