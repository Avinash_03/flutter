import "package:expence_manager/drawer.dart";
import "package:flutter/material.dart";
import "package:google_fonts/google_fonts.dart";
import "package:svg_flutter/svg_flutter.dart";

class Transaction extends StatefulWidget {
  const Transaction({super.key});

  @override
  State<Transaction> createState() => _TransactionState();
}

void mybottomshit(context){
  showModalBottomSheet(isScrollControlled: true ,context:context, builder:(context){
    return Column(
      mainAxisSize:MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom,top: 20,left: 20,right: 20),
         child: 
         Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
                Text("Date",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w400,fontSize: 13))),
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: const Color.fromRGBO(191, 189, 189, 1))
                  ),
                   child: TextFormField(
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.only(left: 10),
                      border:InputBorder.none,
                      hintText:"Enter Date",
                      hintStyle:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0,0, 0.6),fontWeight: FontWeight.w400,fontSize: 13)))
                    
                  ),
                ),
                const SizedBox(height: 20,),
                 Text("Amount",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w400,fontSize: 13))),
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: const Color.fromRGBO(191, 189, 189, 1))
                  ),
                  child: TextFormField(
                    decoration: InputDecoration(
                      contentPadding:const  EdgeInsets.only(left: 10),
                      border:InputBorder.none,
                      hintText:"Enter Amount",
                      hintStyle:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0,0, 0.6),fontWeight: FontWeight.w400,fontSize: 13)))
                    
                  ),
                ),
                const SizedBox(height: 20,),
                 Text("Category",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w400,fontSize: 13))),
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: const Color.fromRGBO(191, 189, 189, 1))
                  ),
                   child: TextFormField(
                    decoration: InputDecoration(
                      contentPadding:const  EdgeInsets.only(left: 10),
                      border:InputBorder.none,
                      hintText:"Enter Category",
                      hintStyle:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0,0, 0.6),fontWeight: FontWeight.w400,fontSize: 13)))
                    
                  ),
                ),
                const SizedBox(height: 20,),
                 const Text("Description"),
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: const Color.fromRGBO(191, 189, 189, 1))
                  ),
                   child: TextFormField(
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.only(left: 10),
                      border:InputBorder.none,
                      hintText:"Enter Description",
                      hintStyle:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0,0, 0.6),fontWeight: FontWeight.w400,fontSize: 13)))
                    
                  ),
                ),
                const SizedBox(height: 20,),
                Center(child: Container(
                  height: 50,
                  width: 100,
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(14, 161, 125, 1),
                    borderRadius: BorderRadius.circular(67)
                  ),
                  child: Center(
                    child: Text("Add",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(255,255,255, 1),fontWeight: FontWeight.w400,fontSize: 13))),
                  ),
                ),)
          ],)
        ),
      ],
    );
  });
}


class _TransactionState extends State<Transaction> {

  
  @override
  Widget build(BuildContext context) {
    return  
    Scaffold(
      appBar: AppBar(
        
        title:  Text("June 2022",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w500,fontSize: 16))),
        actions: [SvgPicture.asset("assets/images/Search_alt.svg"),const SizedBox(width: 20,)],      
      ),
      body: ListView.builder(
        itemCount: 5,
        itemBuilder:(context,index) {
          return Container(
            //height: 60,
           // width: double.infinity,
            decoration: const BoxDecoration(
              border: Border(bottom: BorderSide(color: Color.fromRGBO(206, 206, 206, 1)))
            ),
            
              child: Row(children: [
                const SizedBox(width: 10,),
                SvgPicture.asset('assets/images/Mask group (5).svg',width: 41,),
                const SizedBox(width: 10,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      
                      children: [
                        Text("Medicin",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w400,fontSize: 15))),
                        const SizedBox(width:160 ,),
                      
                      
                        const Icon(Icons.remove_circle,color: Colors.red,size: 20,),
                        const SizedBox(width: 5,),
                      Text("500",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0,0,0, 1),fontWeight: FontWeight.w400,fontSize: 15))),
              
                      ],
                    ),
                Text("Lorem Ipsum is simply dummy text of the ",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w400,fontSize: 10)))
                ,Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    const SizedBox(width: 200,),
                     Text("3 June | 11:50 AM",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0,0,0, 0.6),fontWeight: FontWeight.w400,fontSize: 10))),
                                  ],
                ),
                const SizedBox(height: 5,)
                ],)
              ],),
           
          );
        }
      ),
      floatingActionButton: FloatingActionButton.extended(onPressed: (){
mybottomshit(context);
      },
      
      backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
      shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(67)),
     label: Row(
      children: [
        SvgPicture.asset("assets/images/Group 16.svg"),
        const SizedBox(width: 10,),
        Text("Add Transaction ",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w500,fontSize: 16))),
      ],
     )),
     floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
     drawer:const Showdrawer()
    );
  }
}