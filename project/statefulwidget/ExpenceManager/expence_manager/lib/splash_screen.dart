import 'package:expence_manager/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:svg_flutter/svg_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class Splashscreen extends StatefulWidget{

  const Splashscreen({super.key});

  @override
  State createState()=> SplashscreenState();
}

class SplashscreenState extends State{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,

        children: [
          const Spacer(flex: 1,),
          Center(
            child: 
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>const Login()));
                  },
                  child: Container(
                    padding:const EdgeInsets.all(25),
                    height: 144,
                    width: 144,
                    decoration: const BoxDecoration(
                      color: Color.fromRGBO(234, 238, 235, 1),
                      shape: BoxShape.circle
                    ),
                    child: SvgPicture.asset("assets/images/Group 77.svg"),
                  ),
                ),
              
          ),
            const Spacer(),
      
          Text("Expense Manager",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w600,fontSize: 16,color: Color.fromRGBO(0,0,0,1))),),
          const SizedBox(height: 100,)
        ],
      ),
    );
  }
}