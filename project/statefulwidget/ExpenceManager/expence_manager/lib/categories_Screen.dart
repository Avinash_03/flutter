import 'package:expence_manager/drawer.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:svg_flutter/svg_flutter.dart';

class Categories extends StatefulWidget {
  const Categories({super.key});

  @override
  State<Categories> createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  void showmydialog() {
    showDialog(
      
        context: context,
        builder: (context) => AlertDialog(
        
              title: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Delete Category",
                      style: GoogleFonts.poppins(
                          textStyle: const TextStyle(
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w500,
                              fontSize: 16))),
                ],
              ),
              content: Text(
                  "Are you sure you want to delete the selected category?",
                  style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                          color: Color.fromRGBO(33, 33, 33, 1),
                          fontWeight: FontWeight.w400,
                          fontSize: 13))),
              actions: [
                ElevatedButton(
                  
                  onPressed: () {},
                  child: Text("Delete",
                      style: GoogleFonts.poppins(
                          textStyle: const TextStyle(
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                              fontSize: 13))),
                ),
                const SizedBox(width: 20,),
              
                ElevatedButton(
                  
                  onPressed: () {},
                  child: Text("Cancel",
                      style: GoogleFonts.poppins(
                          textStyle: const TextStyle(
                              color: Color.fromRGBO(33, 33, 33, 1),
                              fontWeight: FontWeight.w400,
                              fontSize: 13))),
                ),
              ],
            ));
  }

  void mybottomshit(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom,
                      top: 20,
                      left: 20,
                      right: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        children: [
                          Container(
                            height: 70,
                            decoration: const BoxDecoration(
                              color: Color.fromRGBO(0, 0, 0, 0.06),
                              shape: BoxShape.circle,
                            ),
                          ),
                          Text("Add",
                              style: GoogleFonts.poppins(
                                  textStyle: const TextStyle(
                                      color: Color.fromRGBO(33, 33, 33, 1),
                                      fontWeight: FontWeight.w400,
                                      fontSize: 13))),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text("Imahe URL",
                          style: GoogleFonts.poppins(
                              textStyle: const TextStyle(
                                  color: Color.fromRGBO(33, 33, 33, 1),
                                  fontWeight: FontWeight.w400,
                                  fontSize: 13))),
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(
                                color: const Color.fromRGBO(191, 189, 189, 1))),
                        child: TextFormField(
                            decoration: InputDecoration(
                                contentPadding: const EdgeInsets.only(left: 10),
                                border: InputBorder.none,
                                hintText: "Enter URL",
                                hintStyle: GoogleFonts.poppins(
                                    textStyle: const TextStyle(
                                        color: Color.fromRGBO(0, 0, 0, 0.6),
                                        fontWeight: FontWeight.w400,
                                        fontSize: 13)))),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text("Category",
                          style: GoogleFonts.poppins(
                              textStyle: const TextStyle(
                                  color: Color.fromRGBO(33, 33, 33, 1),
                                  fontWeight: FontWeight.w400,
                                  fontSize: 13))),
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(
                                color: const Color.fromRGBO(191, 189, 189, 1))),
                        child: TextFormField(
                            decoration: InputDecoration(
                                contentPadding: const EdgeInsets.only(left: 10),
                                border: InputBorder.none,
                                hintText: "Enter Category name",
                                hintStyle: GoogleFonts.poppins(
                                    textStyle: const TextStyle(
                                        color: Color.fromRGBO(0, 0, 0, 0.6),
                                        fontWeight: FontWeight.w400,
                                        fontSize: 13)))),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Center(
                        child: Container(
                          height: 50,
                          width: 100,
                          decoration: BoxDecoration(
                              color: const Color.fromRGBO(14, 161, 125, 1),
                              borderRadius: BorderRadius.circular(67)),
                          child: Center(
                            child: Text("Add",
                                style: GoogleFonts.poppins(
                                    textStyle: const TextStyle(
                                        color: Color.fromRGBO(255, 255, 255, 1),
                                        fontWeight: FontWeight.w400,
                                        fontSize: 13))),
                          ),
                        ),
                      )
                    ],
                  )),
              const SizedBox(
                height: 10,
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Categories",
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
                    color: Color.fromRGBO(33, 33, 33, 1),
                    fontWeight: FontWeight.w500,
                    fontSize: 16))),
      ),
      body: GridView.builder(
          itemCount: 4,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2),
          itemBuilder: (context, index) {
            return GestureDetector(
              onLongPress: () {
                showmydialog();
              },
              child: Container(
                margin: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: const [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.15),
                          blurRadius: 8,
                          offset: Offset(1, 2))
                    ],
                    borderRadius: BorderRadius.circular(14)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SvgPicture.asset("assets/images/Group 67.svg"),
                    Text("Food",
                        style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                                color: Color.fromRGBO(33, 33, 33, 1),
                                fontWeight: FontWeight.w500,
                                fontSize: 16)))
                  ],
                ),
              ),
            );
          }),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            mybottomshit(context);
          },
          backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(67)),
          label: Row(
            children: [
              SvgPicture.asset("assets/images/Group 16.svg"),
              const SizedBox(
                width: 10,
              ),
              Text("Add Category ",
                  style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                          color: Color.fromRGBO(33, 33, 33, 1),
                          fontWeight: FontWeight.w500,
                          fontSize: 16))),
            ],
          )),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      drawer: const Showdrawer(),
    );
  }
}
