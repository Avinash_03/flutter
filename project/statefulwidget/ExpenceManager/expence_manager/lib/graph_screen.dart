import "package:expence_manager/drawer.dart";
import "package:flutter/material.dart";
import "package:google_fonts/google_fonts.dart";
import "package:pie_chart/pie_chart.dart";
import "package:svg_flutter/svg_flutter.dart";

class Showgraph extends StatefulWidget {
  const Showgraph({super.key});

  @override
  State<Showgraph> createState() => _ShowgraphState();
}

class _ShowgraphState extends State<Showgraph> {
  Map<String, double> data = {
    'Food': 20,
    "Fuel": 30,
    "Medicine": 10,
    "Entertainment": 20,
    "Shoping": 20,
  };
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Graphs",
            style: GoogleFonts.poppins(
                textStyle: const TextStyle(
                    color: Color.fromRGBO(33, 33, 33, 1),
                    fontWeight: FontWeight.w500,
                    fontSize: 16))),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            PieChart(
              chartValuesOptions: const ChartValuesOptions(
                showChartValues: false
              ),
              dataMap: data,
              chartType: ChartType.ring,
              chartRadius: 150,
              ringStrokeWidth: 30,
              centerWidget: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Total",
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontWeight: FontWeight.w400,
                      fontSize: 10))),
                      Text("₹ 2550.00",
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontWeight: FontWeight.w600,
                      fontSize: 13))),
                ],
              ),
            ),
            const SizedBox(height: 50,),
            Expanded(
              child: Container(
              
                child: ListView.builder(
                  itemCount: 5,
                  itemBuilder: (context,index){
                    return Container(
                      margin: EdgeInsets.all(10),
                      child: Row(children: [
                        SvgPicture.asset("assets/images/Group 67.svg"),
                        const SizedBox(width: 10,),
                         Text("Food",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w500,fontSize: 16)),),
                          const SizedBox(width: 100,),
                         Text("₹ 650.00",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w500,fontSize: 16)),)
                        
                      ],),
                    );
                  }
                  ),
              ),
            ),
            const Divider(),
            Row(
              children: [
              Text("Total",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w500,fontSize: 16)),),
                const Spacer(),
                         Text("₹ 650.00",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w500,fontSize: 16)),),

              ]
              
              
            )
          ],
        ),
      ),
      drawer: const Showdrawer(),
    );
  }
}
