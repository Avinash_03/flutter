import 'package:expence_manager/register_screen.dart';
import 'package:expence_manager/transaction_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:svg_flutter/svg_flutter.dart';

class Login extends StatefulWidget{
  const Login({super.key});

  @override
  State createState()=>LoginState();
}

class LoginState extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: double.maxFinite,
          padding: const EdgeInsets.only(top: 60,right: 40,left: 40),
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 40,),
              Center(child: SvgPicture.asset("assets/images/Group 77.svg")),
              const SizedBox(height: 60,),
              Text("Login to your Account",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 16)),),
              const SizedBox(height: 25,),
              Container(
                padding: const EdgeInsets.all(15),
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(255,255,255,1),
                 borderRadius: BorderRadius.circular(8) ,
                 boxShadow: const [BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.15),
                    blurRadius: 10,
                    offset: Offset(0,3)
                 )]
                ),
                child: TextFormField(
                  decoration:  InputDecoration(
                    border: InputBorder.none,
                    hintText: "Username",
                    hintStyle: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.4),fontWeight: FontWeight.w400,fontSize: 12))
                  ),
                ),
              ),
              const SizedBox(height: 25,),
               Container(
                padding: const EdgeInsets.all(15),
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(255,255,255,1),
                 borderRadius: BorderRadius.circular(8) ,
                 boxShadow: const [BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.15),
                    blurRadius: 10,
                    offset: Offset(0,3)
                 )]
                ),
                child: TextFormField(
                  decoration:  InputDecoration(
                    border: InputBorder.none,
                    hintText: "Password",
                    hintStyle: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.4),fontWeight: FontWeight.w400,fontSize: 12))
                  ),
                ),
              ),
              const SizedBox(height: 30,),
              GestureDetector(
                onTap:(){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>const Transaction()));
                },
                child: Container(
                  height: 50,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(14, 161, 125, 1),
                   borderRadius: BorderRadius.circular(8) ,
                   boxShadow: const [BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15),
                      blurRadius: 10,
                      offset: Offset(0,3)
                   )]
                  ),
                  child: Center(child: Text("Login",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(255,255,255,1),fontWeight: FontWeight.w500,fontSize: 15)),),),
                 
                ),
              ),
              const SizedBox(height: 250,),
               Center(child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                 children: [
                   Text("Don’t have an account? ",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.6),fontWeight: FontWeight.w400,fontSize: 12)),),
                    GestureDetector(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>const Register()));
                      },
                      child: Text("Sign up",style:  GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(14, 161, 125, 1),fontWeight: FontWeight.w400,fontSize: 12)),)),
                 ],
               )),
            ],
          ),
        ),
    )
    );

  }
}