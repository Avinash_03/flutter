import 'package:expence_manager/categories_Screen.dart';
import 'package:expence_manager/graph_screen.dart';
import 'package:expence_manager/transaction_screen.dart';
import 'package:expence_manager/trash_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:svg_flutter/svg_flutter.dart';

class Showdrawer extends StatefulWidget {
  const Showdrawer({super.key});

  @override
  State<Showdrawer> createState() => _ShowdrawerState();
}

class _ShowdrawerState extends State<Showdrawer> {
  @override
  Widget build(BuildContext context) {
    return  Drawer(
      width: 245,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 30,),
          Container(
            padding: const EdgeInsets.only(left: 18),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 180,
                child: Text("Expense Manager",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w600,fontSize: 16),))),
          Text("Saves all your Transactions",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0,0,0, 0.5),fontWeight: FontWeight.w400,fontSize: 10),)),

            ],
          )),
          const SizedBox(height: 15,),
          GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=>const Transaction()));
              
            },
            child: Container(
              height: 40,
              width: 210,
              decoration: const  BoxDecoration(
                color:Color.fromRGBO(14, 161, 125, 0.15),
                borderRadius: BorderRadius.only(topRight: Radius.circular(20),bottomRight: Radius.circular(20))
              ),
              child: Row(children: [
                const SizedBox(width: 20,),
                SvgPicture.asset("assets/images/Subtract.svg"),
                const SizedBox(width: 5,),
                 Text("Transaction",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w500,fontSize: 16),))
              ],),
            ),
          ),
          const SizedBox(height: 10,),
          GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=>const Showgraph()));
            },
            child: Container(
              height: 40,
              width: 210,
              decoration:const  BoxDecoration(
                color: Color.fromRGBO(14, 161, 125, 0.15),
                borderRadius: BorderRadius.only(topRight: Radius.circular(20),bottomRight: Radius.circular(20))
              ),
              child: Row(children: [
                const SizedBox(width: 20,),
                SvgPicture.asset("assets/images/ icon _pie chart_.svg"),
                const SizedBox(width: 5,),
                 Text("Graphs",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w500,fontSize: 16),))
              ],),
            ),
          ),
          const SizedBox(height: 10,),
          GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=>const Categories()));
            },
            child: Container(
              height: 40,
              width: 210,
              decoration:const  BoxDecoration(
                color: Color.fromRGBO(14, 161, 125, 0.15),
                borderRadius: BorderRadius.only(topRight: Radius.circular(20),bottomRight: Radius.circular(20))
              ),
              child: Row(children: [
                const SizedBox(width: 20,),
                SvgPicture.asset("assets/images/Subtract (1).svg"),
                const SizedBox(width: 5,),
                 Text("Category",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w500,fontSize: 16),))
              ],),
            ),
          ),
          const SizedBox(height: 10,),
          GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=>const Trash()));
            },
            child: Container(
              height: 40,
              width: 210,
              decoration:const  BoxDecoration(
                color: Color.fromRGBO(14, 161, 125, 0.15),
                borderRadius: BorderRadius.only(topRight: Radius.circular(20),bottomRight: Radius.circular(20))
              ),
              child: Row(children: [
                const SizedBox(width: 20,),
                SvgPicture.asset("assets/images/Vector (5).svg"),
                const SizedBox(width: 5,),
                 Text("Trash",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w500,fontSize: 16),))
              ],),
            ),
          ),
          const SizedBox(height: 10,),
          GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=>const Showgraph()));
            },
            child: Container(
              height: 40,
              width: 210,
              decoration:const  BoxDecoration(
                color: Color.fromRGBO(14, 161, 125, 0.15),
                borderRadius: BorderRadius.only(topRight: Radius.circular(20),bottomRight: Radius.circular(20))
              ),
              child: Row(children: [
                const SizedBox(width: 20,),
                SvgPicture.asset("assets/images/Vector (6).svg"),
                const SizedBox(width: 5,),
                 Text("About us",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(33, 33, 33, 1),fontWeight: FontWeight.w500,fontSize: 16),))
              ],),
            ),
          ),
                 
        ],
      ),
    );
  }
}