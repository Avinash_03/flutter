import 'package:flutter/material.dart';

void main()=>runApp(const MyApp());

class MyApp extends StatelessWidget{
  const MyApp({super.key});

  @override
  Widget build(BuildContext context){

    return const MaterialApp(
      debugShowCheckedModeBanner:false,
      home: MyHomepage(),
    );
  }
}

class MyHomepage extends StatefulWidget {
  const MyHomepage({super.key});

  @override
  State createState() => _MyHomepageState();
}

class _MyHomepageState extends State {

  List data=[];

TextEditingController namecontroller=TextEditingController();
TextEditingController cmpnamecontroller=TextEditingController();
TextEditingController locationcontroller=TextEditingController();

bool showinf=false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: 
        AppBar(
          title: const Text("My_Dream_Company"),
          backgroundColor: Colors.blue,
          centerTitle: true,
      ),
      body:
        SingleChildScrollView(
          child: Column(
            children: [        
              SizedBox(   
                child: Column(
                  children: [
                    const SizedBox(height: 50,),
              TextField(
                
                controller: namecontroller,
                autofocus: true,
                decoration: InputDecoration(
                  hintText: "Enter Name",
                  border: OutlineInputBorder(borderRadius:BorderRadius.circular(20)),
                ),
              ),
              const SizedBox(height: 40,),
              TextField(
                controller: cmpnamecontroller,
                decoration: const InputDecoration(
                  hintText: "Enter Company Name",
                  border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20)))
                ),
              ),
              const SizedBox(
                height: 50,
              ),
              TextField(
                controller: locationcontroller,
                decoration:const InputDecoration(
                  hintText: "Enter Location",
                  border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20)))
                ) ,
              ),
              const SizedBox(height: 50,),
                  ],
                ),
              ),
              
               ElevatedButton(
                style:const ButtonStyle(
                  fixedSize: MaterialStatePropertyAll(Size(200, 50))
                ),
                onPressed: (){
                   if(namecontroller.text.isNotEmpty && cmpnamecontroller.text.isNotEmpty && locationcontroller.text.isNotEmpty){
                      showinf=true;
                       setState(() {
                    
                    data.add({
                      "name":namecontroller.text,
                      "cmpname":cmpnamecontroller.text,
                      "location":locationcontroller.text
                    });
                    namecontroller.text='';
                    cmpnamecontroller.text='';
                    locationcontroller.text='';
                  });
                    }
                 
                }, 
                child: const Text("Submit")
              ),
              showinf?
              SizedBox(
                height: 300,
                child: ListView.builder(
                  physics: const ScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: data.length,
                  itemBuilder: (context, index) {
                    
                  return Container(
                    alignment: Alignment.centerLeft,
                    height: 200,
                    width: double.infinity,
                    margin: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      border: Border.all(width: 2,color: Colors.grey)
                    ),
                    child:Column(children: [
                      Text(data[index]['name'],style:const TextStyle(fontSize: 20),),
                      const SizedBox(height: 20,),
                      Text(data[index]['cmpname'],style:const TextStyle(fontSize: 20),),
                      const SizedBox(height: 20,),
                      Text(data[index]['location'],style:const TextStyle(fontSize: 20),),
                    ],)
                  );
                 } ),
              ):Container()
          
            ],
          ),
        ),
    );
  }
}