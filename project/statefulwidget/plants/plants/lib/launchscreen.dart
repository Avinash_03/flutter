
import "package:flutter/cupertino.dart";
import "package:flutter/material.dart";
import "package:google_fonts/google_fonts.dart";
import "package:plants/loginscreen.dart";


class Launchscreen extends StatefulWidget{
   
   const Launchscreen({super.key});
   @override
   State createState()=>LaunchState();
}
class LaunchState extends State{
   @override
   Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: const Color.fromRGBO(251, 247, 248, 1),
      body: Column(
        children: [
          SizedBox(
            height: 464,
           width: double.infinity,
            child: Image.asset('assets/images/image 2.png',),
          ),
          const SizedBox(height: 10,),
           Container(
            padding:const EdgeInsets.only(left: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
             children: [
               Text("Enjoy your",style:GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(47, 47, 47, 1),fontWeight: FontWeight.w500,fontSize: 34.22),),),
               Row(
             children: [
               Text("life with ",style:GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(47, 47, 47, 1),fontWeight: FontWeight.w500,fontSize: 34.22),),),
              Text("Plants",style:GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(47, 47, 47, 1),fontWeight: FontWeight.w800,fontSize: 34.22),),),
             
             ],
           ),
             ],
           )),
           
          const SizedBox(height: 50,),
          GestureDetector(
            onTap:() {
               Navigator.push(context, MaterialPageRoute(builder: (BuildContext context )=>const Login()));
            },
            child: Container(
              margin:const EdgeInsets.all(20),
              height: 50,
              width: double.infinity,   
              decoration:const BoxDecoration(
                boxShadow: [BoxShadow(
                  blurRadius: 40,
                  color: Color.fromRGBO(0, 0, 0, 0.15),
                  offset: Offset(0, 20)
                )],
                borderRadius: BorderRadius.all(Radius.circular(10)),
                gradient: LinearGradient(colors: [Color.fromRGBO(62, 102, 24, 1),Color.fromRGBO(124, 180, 70, 1)],begin: Alignment.bottomCenter,end: Alignment.topCenter,stops: [0.2,1])
              ),
              child: Center(
                child: Text("Get Started >",style:GoogleFonts.rubik(textStyle:const TextStyle(color: Color.fromRGBO(255, 255, 255, 1),fontWeight: FontWeight.w500,fontSize: 18),),
                             ),
            
              ),     
            ),
          )
        ],
      ),
    );
   }
}