import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:plants/home.dart';

class Verification extends StatefulWidget{
  const Verification({super.key});

  @override
  State createState()=> VerificationState();
}

class VerificationState extends State{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(251, 247, 248, 1),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            
            Row(
              children: [
                const SizedBox(width: 10,),
                const Icon(Icons.arrow_back),
                const Spacer(),

                SvgPicture.asset("assets/images/Group 5314 (1).svg"),
              ],
            ),
            Container(
              padding:const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Verification",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 1),fontWeight: FontWeight.w700,fontSize: 20)),),
                  const SizedBox(height: 10,),
                  SizedBox(
                    width: 300,
                    child: Text("Enter the OTP code from the phone we just sent you.",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.6),fontWeight: FontWeight.w400,fontSize: 14)))),

                    const SizedBox(height: 35,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 56,
                          width: 56,
                          decoration: BoxDecoration(
                            color: const Color.fromRGBO(255, 255, 255, 1),
                            border: Border.all(color: const Color.fromRGBO(204, 211, 196, 1)),
                            borderRadius: BorderRadius.circular(8)
                          ),
                          child: TextFormField(
                            // controller: ,
                            decoration: const InputDecoration(
                              border: InputBorder.none
                            ),
                          ),
                        ),
                        Container(
                          height: 56,
                          width: 56,
                          decoration: BoxDecoration(
                            color: const Color.fromRGBO(255, 255, 255, 1),
                            border: Border.all(color: const Color.fromRGBO(204, 211, 196, 1)),
                            borderRadius: BorderRadius.circular(8)
                          ),
                          child: TextFormField(
                            // controller: ,
                            decoration: const InputDecoration(
                              border: InputBorder.none
                            ),
                          ),
                        ),
                        Container(
                          height: 56,
                          width: 56,
                          decoration: BoxDecoration(
                            color: const Color.fromRGBO(255, 255, 255, 1),
                            border: Border.all(color: const Color.fromRGBO(204, 211, 196, 1)),
                            borderRadius: BorderRadius.circular(8)
                          ),
                          child: TextFormField(
                            // controller: ,
                            decoration: const InputDecoration(
                              border: InputBorder.none
                            ),
                          ),
                        ),
                        Container(
                          height: 56,
                          width: 56,
                          decoration: BoxDecoration(
                            color: const Color.fromRGBO(255, 255, 255, 1),
                            border: Border.all(color: const Color.fromRGBO(204, 211, 196, 1)),
                            borderRadius: BorderRadius.circular(8)
                          ),
                          child: TextFormField(
                            // controller: ,
                            decoration: const InputDecoration(
                              border: InputBorder.none
                            ),
                          ),
                        )
                        
                      ],
                    ),
                    const SizedBox(height: 35,),
                    Row(
                      children: [
                        Text("Don’t receive OTP code! ",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.6),fontWeight: FontWeight.w400,fontSize: 14))),
                        Text("Resend",style: GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 0.8),fontWeight: FontWeight.w500,fontSize: 14))),

                        
                      ],
                    ),
                     const SizedBox(height: 10,),
                         GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>const Home()));
              },
               child: Container(
                height: 50,
                width: double.infinity,   
                decoration:const BoxDecoration(
                  boxShadow: [BoxShadow(
                    blurRadius: 40,
                    color: Color.fromRGBO(0, 0, 0, 0.15),
                    offset: Offset(0, 20)
                  )],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  gradient: LinearGradient(colors: [Color.fromRGBO(62, 102, 24, 1),Color.fromRGBO(124, 180, 70, 1)],begin: Alignment.bottomCenter,end: Alignment.topCenter,stops: [0.2,1])
                ),
                child: Center(
                  child: Text("Submit",style:GoogleFonts.rubik(textStyle:const TextStyle(color: Color.fromRGBO(255, 255, 255, 1),fontWeight: FontWeight.w500,fontSize: 18),),
                 ),
                       
                ),     
                           ),
             ),
          
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}