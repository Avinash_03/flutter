import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:plants/detialScreen.dart';

class Home extends StatefulWidget{
  const Home({super.key});

  @override
  State createState()=>  HomeState();
}

class HomeState extends State{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: 
      SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SvgPicture.asset('assets/images/Group 5482.svg')
              ],
            ),
            const SizedBox(height: 15,),
            Row(
              children: [
                const SizedBox(width: 20,),
                Text("Find your\nfavorite plants",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 24,color: Color.fromRGBO(0, 0, 0, 1))),),
                const Spacer(),
                Container(
                  height: 40,
                  width: 40,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: const[BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.15),
                      blurRadius: 16.1,
                      offset: Offset(0, 4)
                    )]
                  ),
                  child: Center(
                    child: SvgPicture.asset("assets/images/shopping-bag.svg"),
                  ),
                ),
                const SizedBox(width: 20,)
              ],
            ),
            const SizedBox(height: 30,),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  const SizedBox(width: 20,),
                  Container(
                    padding:const EdgeInsets.all(20),
                    height: 108,
                    width: 310,
                    decoration:  BoxDecoration(
                      color: const Color.fromRGBO(204, 231, 185, 1),
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Row(
                      children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("30% OFF",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w600,fontSize: 24,color: Color.fromRGBO(0, 0, 0, 1))),),
                          Text("02-23 April",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w400,fontSize: 14,color: Color.fromRGBO(0, 0, 0, 0.6))),),
                  
                        ],
                      ),
                      const Spacer(),
                      Image.asset("assets/images/Group 5318.png")
                    ],),
                  ),
                  const SizedBox(width: 20,),
                  Container(
                    padding:const EdgeInsets.all(20),
                    height: 108,
                    width: 310,
                    decoration:  BoxDecoration(
                      color: const Color.fromRGBO(204, 231, 185, 1),
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("30% OFF",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w600,fontSize: 24,color: Color.fromRGBO(0, 0, 0, 1))),),
                          Text("02-23 April",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w400,fontSize: 14,color: Color.fromRGBO(0, 0, 0, 0.6))),),
                  
                        ],
                      ),
                      const Spacer(),
                      Image.asset("assets/images/Group 5318.png")
                    ],),
                  ),
                  const SizedBox(width: 20,),
                  Container(
                    padding:const EdgeInsets.all(20),
                    height: 108,
                    width: 310,
                    decoration:  BoxDecoration(
                      color: const Color.fromRGBO(204, 231, 185, 1),
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("30% OFF",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w600,fontSize: 24,color: Color.fromRGBO(0, 0, 0, 1))),),
                          Text("02-23 April",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w400,fontSize: 14,color: Color.fromRGBO(0, 0, 0, 0.6))),),
                  
                        ],
                      ),
                      const Spacer(),
                      Image.asset("assets/images/Group 5318.png")
                    ],),
                  ),
                  
                ],
              ),
            ),
            const SizedBox(height: 30,),
            Row(children: [
              const SizedBox(width: 40,),
              Text("Indoor",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 20,color: Color.fromRGBO(0, 0, 0, 1))))
            ],),
             SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [

                  const SizedBox(width: 20,),
                   GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>const Detailscreen()));
                    },
                     child: Container(
                      padding:const EdgeInsets.all(20),
                     // height: 188,
                     // width: 141,
                      decoration:  BoxDecoration(
                        color: const Color.fromRGBO(255, 255, 255, 1),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: const [BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.06),
                          offset: Offset(0, 7.52),
                          blurRadius: 18.8
                        )]
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset('assets/images/136722116_a0b8a27e-7bb5-4535-b3fe-d1dcdb5caf6d 1.png'),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text("Snake Plants    ",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w400,fontSize: 13.16,color: Color.fromRGBO(0, 0, 0, 1))),),
                            
                              ],
                            ),
                            Row(
                              children: [
                                Text("₹350",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w600,fontSize: 16.92,color: Color.fromRGBO(62, 102, 24, 1))),),
                                const SizedBox(width: 40,),
                                Container(
                                  padding: const EdgeInsets.all(5),
                                  height: 26,
                                  width: 26,
                                  decoration: const BoxDecoration(
                                    color:  Color.fromRGBO(237, 238, 235, 1),
                                    shape: BoxShape.circle,
                                  ),
                                  child: Center(
                                    child: SvgPicture.asset("assets/images/shopping-bag.svg"),
                                  ),
                                )
                              ],
                            ),
                          
                                      
                        ],
                      ),
                                       ),
                   ),

                   const SizedBox(width: 20,),
                   Container(
                    padding:const EdgeInsets.all(20),
                   // height: 188,
                   // width: 141,
                    decoration:  BoxDecoration(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.06),
                        offset: Offset(0, 7.52),
                        blurRadius: 18.8
                      )]
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset('assets/images/136722116_a0b8a27e-7bb5-4535-b3fe-d1dcdb5caf6d 1.png'),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Snake Plants    ",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w400,fontSize: 13.16,color: Color.fromRGBO(0, 0, 0, 1))),),
                          
                            ],
                          ),
                          Row(
                            children: [
                              Text("₹350",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w600,fontSize: 16.92,color: Color.fromRGBO(62, 102, 24, 1))),),
                              const SizedBox(width: 40,),
                              Container(
                                padding: const EdgeInsets.all(5),
                                height: 26,
                                width: 26,
                                decoration: const BoxDecoration(
                                  color:  Color.fromRGBO(237, 238, 235, 1),
                                  shape: BoxShape.circle,
                                ),
                                child: Center(
                                  child: SvgPicture.asset("assets/images/shopping-bag.svg"),
                                ),
                              )
                            ],
                          ),
                        
                                    
                      ],
                    ),
                  ),

                   const SizedBox(width: 20,),
                   Container(
                    padding:const EdgeInsets.all(20),
                   // height: 188,
                   // width: 141,
                    decoration:  BoxDecoration(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.06),
                        offset: Offset(0, 7.52),
                        blurRadius: 18.8
                      )]
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset('assets/images/136722116_a0b8a27e-7bb5-4535-b3fe-d1dcdb5caf6d 1.png'),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Snake Plants    ",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w400,fontSize: 13.16,color: Color.fromRGBO(0, 0, 0, 1))),),
                          
                            ],
                          ),
                          Row(
                            children: [
                              Text("₹350",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w600,fontSize: 16.92,color: Color.fromRGBO(62, 102, 24, 1))),),
                              const SizedBox(width: 40,),
                              Container(
                                padding: const EdgeInsets.all(5),
                                height: 26,
                                width: 26,
                                decoration: const BoxDecoration(
                                  color:  Color.fromRGBO(237, 238, 235, 1),
                                  shape: BoxShape.circle,
                                ),
                                child: Center(
                                  child: SvgPicture.asset("assets/images/shopping-bag.svg"),
                                ),
                              )
                            ],
                          ),
                        
                                    
                      ],
                    ),
                  ),
                  const SizedBox(width: 20,),
                  
                  
                ],
              ),
            ),
            const SizedBox(height: 30,),
             Row(children: [
              const SizedBox(width: 40,),
              Text("Outdoor",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 20,color: Color.fromRGBO(0, 0, 0, 1))))
            ],),
             SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [

                  const SizedBox(width: 20,),
                   Container(
                    padding:const EdgeInsets.all(20),
                   // height: 188,
                   // width: 141,
                    decoration:  BoxDecoration(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.06),
                        offset: Offset(0, 7.52),
                        blurRadius: 18.8
                      )]
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset('assets/images/136722116_a0b8a27e-7bb5-4535-b3fe-d1dcdb5caf6d 1.png'),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Snake Plants    ",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w400,fontSize: 13.16,color: Color.fromRGBO(0, 0, 0, 1))),),
                          
                            ],
                          ),
                          Row(
                            children: [
                              Text("₹350",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w600,fontSize: 16.92,color: Color.fromRGBO(62, 102, 24, 1))),),
                              const SizedBox(width: 40,),
                              Container(
                                padding: const EdgeInsets.all(5),
                                height: 26,
                                width: 26,
                                decoration: const BoxDecoration(
                                  color:  Color.fromRGBO(237, 238, 235, 1),
                                  shape: BoxShape.circle,
                                ),
                                child: Center(
                                  child: SvgPicture.asset("assets/images/shopping-bag.svg"),
                                ),
                              )
                            ],
                          ),
                        
                                    
                      ],
                    ),
                  ),

                   const SizedBox(width: 20,),
                   Container(
                    padding:const EdgeInsets.all(20),
                   // height: 188,
                   // width: 141,
                    decoration:  BoxDecoration(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.06),
                        offset: Offset(0, 7.52),
                        blurRadius: 18.8
                      )]
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset('assets/images/136722116_a0b8a27e-7bb5-4535-b3fe-d1dcdb5caf6d 1.png'),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Snake Plants    ",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w400,fontSize: 13.16,color: Color.fromRGBO(0, 0, 0, 1))),),
                          
                            ],
                          ),
                          Row(
                            children: [
                              Text("₹350",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w600,fontSize: 16.92,color: Color.fromRGBO(62, 102, 24, 1))),),
                              const SizedBox(width: 40,),
                              Container(
                                padding: const EdgeInsets.all(5),
                                height: 26,
                                width: 26,
                                decoration: const BoxDecoration(
                                  color:  Color.fromRGBO(237, 238, 235, 1),
                                  shape: BoxShape.circle,
                                ),
                                child: Center(
                                  child: SvgPicture.asset("assets/images/shopping-bag.svg"),
                                ),
                              )
                            ],
                          ),
                        
                                    
                      ],
                    ),
                  ),

                   const SizedBox(width: 20,),
                   Container(
                    padding:const EdgeInsets.all(20),
                   // height: 188,
                   // width: 141,
                    decoration:  BoxDecoration(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.06),
                        offset: Offset(0, 7.52),
                        blurRadius: 18.8
                      )]
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset('assets/images/136722116_a0b8a27e-7bb5-4535-b3fe-d1dcdb5caf6d 1.png'),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Snake Plants    ",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w400,fontSize: 13.16,color: Color.fromRGBO(0, 0, 0, 1))),),
                          
                            ],
                          ),
                          Row(
                            children: [
                              Text("₹350",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w600,fontSize: 16.92,color: Color.fromRGBO(62, 102, 24, 1))),),
                              const SizedBox(width: 40,),
                              Container(
                                padding: const EdgeInsets.all(5),
                                height: 26,
                                width: 26,
                                decoration: const BoxDecoration(
                                  color:  Color.fromRGBO(237, 238, 235, 1),
                                  shape: BoxShape.circle,
                                ),
                                child: Center(
                                  child: SvgPicture.asset("assets/images/shopping-bag.svg"),
                                ),
                              )
                            ],
                          ),
                        
                                    
                      ],
                    ),
                  ),
                  const SizedBox(width: 20,),
                  
                  
                ],
              ),
            ),
            const SizedBox(height: 30,)
    
          ],
        ),
      ),
    );
  }
}