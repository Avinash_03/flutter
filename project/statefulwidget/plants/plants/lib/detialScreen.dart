import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class Detailscreen extends StatefulWidget {
  const Detailscreen({super.key});

  @override
  State<Detailscreen> createState() => _DetailscreenState();
}

class _DetailscreenState extends State<Detailscreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(height: 50,),
          Row(
            children: [
              const SizedBox(width: 20,),
              GestureDetector(
                onTap: (){
                  Navigator.pop(context);
                },
                child: const Icon(Icons.arrow_back),
              )
            ],
          ),
          const SizedBox(height: 10,),
          Image.asset('assets/images/136722116_a0b8a27e-7bb5-4535-b3fe-d1dcdb5caf6d 1.png',height: 200,width: 250,),
          const SizedBox(height: 70,),
          Container(
            margin: const EdgeInsets.all(30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
              Text("Snake Plants",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w600,fontSize: 22,color: Color.fromRGBO(0, 0, 0, 1))),),
              Text("Plansts make your life with minimal and happy love the plants more and enjoy life.",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w400,fontSize: 13,color: Color.fromRGBO(0, 0, 0, 1))),),

                const SizedBox(height: 40,),
              ],
            ),
          ),
              Container(
                  width: 340,
                  padding: const EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: const Color.fromRGBO(118, 152, 75, 1)
                  ),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(
                            children: [
                              SvgPicture.asset("assets/images/Group 5470.svg"),
                              const SizedBox(height: 15,),
                              Text("Height",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 12,color: Color.fromRGBO(255, 255, 255, 1))),),
                              Text("30cm-40cm",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 10,color: Color.fromRGBO(255, 255, 255, 0.6))),)

                            ],
                          ),
                          Column(
                            children: [
                              SvgPicture.asset("assets/images/thermometer.svg"),
                              const SizedBox(height: 8,),
                              Text("Temperature",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 12,color: Color.fromRGBO(255, 255, 255, 1))),),
                              Text("20'C-25'C",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 10,color: Color.fromRGBO(255, 255, 255, 0.6))),)

                            ],
                          ),
                          Column(
                            children: [
                              SvgPicture.asset("assets/images/Vector (4).svg"),
                              const SizedBox(height: 20,),
                              Text("Pot",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 12,color: Color.fromRGBO(255, 255, 255, 1))),),
                              Text("Ciramic Pot",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 10,color: Color.fromRGBO(255, 255, 255, 0.6))),)

                            ],
                          ),
                          
                        ],
                      ),
                      const SizedBox(height: 30,),
                      Row(
                        children: [
                          Column(
                            children: [
                              Text("Total Price",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 14,color: Color.fromRGBO(255, 255, 255, 0.8))),),
                              Text("₹ 350",style: GoogleFonts.poppins(textStyle:const TextStyle(fontWeight: FontWeight.w600,fontSize: 18,color: Color.fromRGBO(255, 255, 255, 1))),),
                              
                            ],
                          ),
                          const Spacer(),
                          Container(
                            height: 50,
                            width: 150,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.6),
                              color: const Color.fromRGBO(103, 133, 74, 1),

                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                              SvgPicture.asset("assets/images/shopping-bag.svg",color: Colors.white,),
                              const SizedBox(width: 10,),
                              Text("Add to cart",style: GoogleFonts.rubik(textStyle:const TextStyle(fontWeight: FontWeight.w500,fontSize: 14,color: Color.fromRGBO(255, 255, 255, 1))),)
                            ],),
                          )
                        ],
                      )
                    ],
                  ),
                )

            
           ],
      ),
    );
  }
}