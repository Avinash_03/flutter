import "package:flutter/cupertino.dart";
import "package:flutter/material.dart";
import "package:flutter/widgets.dart";
import "package:flutter_svg/flutter_svg.dart";
import "package:google_fonts/google_fonts.dart";

import "verification.dart";
class Login extends StatefulWidget{
  const Login({super.key});
  @override
  State createState()=>LoginState();
}

class LoginState extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(      
      backgroundColor: const Color.fromRGBO(251, 247, 248, 1),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
          SvgPicture.asset("assets/images/Group 5314.svg"),
          const SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text("Login",style:GoogleFonts.poppins(textStyle:const TextStyle(color: Color.fromRGBO(0, 0, 0, 1),fontWeight: FontWeight.w600,fontSize: 30),))]),
            const SizedBox(height: 30,),
            Container(
              margin:const EdgeInsets.all(20),
              height: 50,
              width: double.infinity,   
              decoration: BoxDecoration(
                color: const Color.fromRGBO(255, 255, 255, 1),
                boxShadow: const [BoxShadow(
                  blurRadius: 20,
                  color: Color.fromRGBO(0, 0, 0, 0.06),
                  offset: Offset(0, 8)
                )],
                border: Border.all(color: const Color.fromRGBO(204, 211, 196, 1)),
                borderRadius: const BorderRadius.all(Radius.circular(10),),
              ),   
              child: Row(
                children: [
                  const SizedBox(width: 30,),
                  SvgPicture.asset('assets/images/Vector (3).svg'),
                  const SizedBox(width: 30,),
                  SizedBox(
                    width: 200,
                    child: TextFormField(
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: "Mobile Number ",
                        hintStyle: TextStyle(color: Color.fromRGBO(164, 164, 164, 1))
                      ),
                    ),
                  )
                ],
              ),
            ),
             GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>const Verification()));
              },
               child: Container(
                margin:const EdgeInsets.all(20),
                height: 50,
                width: double.infinity,   
                decoration:const BoxDecoration(
                  boxShadow: [BoxShadow(
                    blurRadius: 40,
                    color: Color.fromRGBO(0, 0, 0, 0.15),
                    offset: Offset(0, 20)
                  )],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  gradient: LinearGradient(colors: [Color.fromRGBO(62, 102, 24, 1),Color.fromRGBO(124, 180, 70, 1)],begin: Alignment.bottomCenter,end: Alignment.topCenter,stops: [0.2,1])
                ),
                child: Center(
                  child: Text("Log in",style:GoogleFonts.rubik(textStyle:const TextStyle(color: Color.fromRGBO(255, 255, 255, 1),fontWeight: FontWeight.w500,fontSize: 18),),
                 ),
                       
                ),     
                           ),
             ),
            //const SizedBox(height:  60,),
            Image.asset("assets/images/Group 5315.png"),
            
        ],),
      ),
    );
  }
}