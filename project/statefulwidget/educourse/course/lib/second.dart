import 'package:flutter/material.dart';
import "package:flutter/widgets.dart";
import "package:flutter_svg/flutter_svg.dart";
import "package:flutter_svg/svg.dart";
import "package:google_fonts/google_fonts.dart";

class Detail extends StatefulWidget {
  const Detail({super.key});

  @override
  State<Detail> createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(colors: [
          Color.fromRGBO(197, 4, 98, 1),
          Color.fromRGBO(80, 3, 112, 1)
        ], begin: Alignment.topCenter, end: Alignment.center)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: const Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(
                      height: 18,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 300,
                              child: Text(
                                "UX Designer from Scratch.",
                                style: GoogleFonts.jost(
                                  textStyle: const TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 32,
                                      color: Color.fromRGBO(255, 255, 255, 1)),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 300,
                              child: Text(
                                "Basic guideline & tips & tricks for how to become a UX designer easily.",
                                style: GoogleFonts.jost(
                                    textStyle: const TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16,
                                        color:
                                            Color.fromRGBO(255, 255, 255, 1))),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Row(
                      children: [
                        SvgPicture.asset(
                          "assets/Group 4912.svg",
                          height: 32,
                          width: 32,
                        ),
                        Text(
                          " Author.",
                          style: GoogleFonts.jost(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Color.fromRGBO(
                                  190,
                                  154,
                                  197,
                                  1,
                                )),
                          ),
                        ),
                        Text(
                          "Jenny",
                          style: GoogleFonts.jost(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Color.fromRGBO(
                                  255,
                                  255,
                                  255,
                                  1,
                                )),
                          ),
                        ),
                        const Spacer(),
                        Text(
                          "4.8",
                          style: GoogleFonts.jost(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Color.fromRGBO(
                                  255,
                                  255,
                                  255,
                                  1,
                                )),
                          ),
                        ),
                        const Icon(
                          Icons.star,
                          color: Color.fromRGBO(255, 146, 0, 1),
                        ),
                        Text(
                          "(20 review)",
                          style: GoogleFonts.jost(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Color.fromRGBO(
                                  190,
                                  154,
                                  197,
                                  1,
                                )),
                          ),
                        ),
                      ],
                    )
                  ],
                )),
            const SizedBox(
              height: 20,
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                    color: Color.fromRGBO(230, 239, 239, 1),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(38),
                        topRight: Radius.circular(38))),
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 40,
                  ),
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: 5,
                      itemBuilder: (context, index) {
                        return Container(
                          margin: const EdgeInsets.only(
                              bottom: 20, right: 30, left: 30),
                          decoration: BoxDecoration(
                              boxShadow: const [
                                BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.15),
                                    blurRadius: 40,
                                    offset: Offset(0, 8))
                              ],
                              color: const Color.fromRGBO(255, 255, 255, 1),
                              borderRadius: BorderRadius.circular(10)),
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Row(
                              children: [
                                Container(
                                  height: 60,
                                  width: 46,
                                  decoration: BoxDecoration(
                                      color: const Color.fromRGBO(
                                          230, 239, 239, 1),
                                      borderRadius: BorderRadius.circular(12)),
                                  child: Center(
                                    child:
                                        SvgPicture.asset("assets/youtube.svg"),
                                  ),
                                ),
                                const SizedBox(width: 10,),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                Text("Introduction",
                          style: GoogleFonts.jost(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 17,
                                color: Color.fromRGBO(
                                  0,
                                  0,
                                  0,
                                  1,
                                )),
                          ),),
                          const SizedBox(height: 10,),
                          SizedBox(
                            width: 200,
                            child: Text("Lorem Ipsum is simply dummy text ... ",
                            style: GoogleFonts.jost(
                              textStyle: const TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 12,
                                  color: Color.fromRGBO(
                                    0,
                                    0,
                                    0,
                                    1,
                                  )),
                            ),),
                          )

                                ],)
                              ],
                            ),
                          ),
                        );
                      }),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
