import 'package:course/second.dart';
import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(205, 218, 218, 1),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 20,),
                  Row(
                    children: [
                      SvgPicture.asset(
                        "assets/menu.svg",
                        height: 26,
                        width: 26,
                      ),
                      const Spacer(),
                      SvgPicture.asset(
                        "assets/bell.svg",
                        height: 26,
                        width: 26,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Welcome to New",
                    style: GoogleFonts.jost(
                      textStyle: const TextStyle(
                          fontWeight: FontWeight.w300,
                          fontSize: 27,
                          color: Color.fromRGBO(0, 0, 0, 1)),
                    ),
                  ),
                  Text(
                    "Educourse",
                    style: GoogleFonts.jost(
                        textStyle: const TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 37,
                            color: Color.fromRGBO(0, 0, 0, 1))),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Container(
                    height: 60,
                    decoration: const BoxDecoration(
                        color: Color.fromRGBO(255, 255, 255, 1),
                        borderRadius: BorderRadius.all(Radius.circular(28.5))),
                    child: TextFormField(
                      decoration: InputDecoration(
                          suffixIcon: Container(
                              padding: const EdgeInsets.all(20),
                              child: const Icon(
                                Icons.search,
                                size: 27,
                              )),
                          contentPadding:
                              const EdgeInsets.only(top: 18, left: 20),
                          border: InputBorder.none,
                          hintText: "Enter your Keyword"),
                    ),
                  )
                ],
              )),
          const SizedBox(
            height: 10,
          ),
          Expanded(
            child: Container(
              width: double.infinity,
              decoration: const BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(38),
                      topRight: Radius.circular(38))),
              child: Padding(
                padding: const EdgeInsets.only(left: 20,top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 13,
                    ),
                    Row(
                      children: [
                        Text(
                          "Course For You",
                          style: GoogleFonts.jost(
                              textStyle: const TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18,
                                  color: Color.fromRGBO(0, 0, 0, 1))),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>const Detail()));
                            },
                            child: Container(
                              height: 242,
                              width: 190,
                              child: SvgPicture.asset("assets/Course Card 1.svg"),
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          GestureDetector(
                            onTap:() {
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>const Detail()));
                            },
                            child: Container(
                              height: 242,
                              width: 190,
                              decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(14)),
                                gradient: LinearGradient(
                                    colors: [
                                      Color.fromRGBO(0, 77, 228, 1),
                                      Color.fromRGBO(1, 47, 135, 1)
                                    ],
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      const SizedBox(
                                        width: 20,
                                      ),
                                      Expanded(
                                        child: Text(
                                          "Design ThinkingThe Beginner",
                                          style: GoogleFonts.jost(
                                              textStyle: const TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 15,
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 1))),
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 15,
                                  ),
                                  SvgPicture.asset("assets/Objects.svg")
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Text(
                      "Course By Category",
                      style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                              color: Color.fromRGBO(0, 0, 0, 1))),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                         Column(
                          children: [
                            Container(
                              height: 36,
                              width: 36,
                              decoration: BoxDecoration(
                                  color: const Color.fromRGBO(25, 0, 56, 1),
                                  borderRadius: BorderRadius.circular(8)),
                                  child: Center(child: SvgPicture.asset("assets/Traced Image.svg"),),
                            ),
                            const SizedBox(height: 10,),
                           Text("UI/UX",style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Color.fromRGBO(0, 0, 0, 1))),),
                            
                          ],
                        ),
                        const SizedBox(
                          width: 30,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 36,
                              width: 36,
                              decoration: BoxDecoration(
                                  color: const Color.fromRGBO(25, 0, 56, 1),
                                  borderRadius: BorderRadius.circular(8)),
                                  child: Center(child: SvgPicture.asset("assets/Vector (2).svg"),),
                            ),
                            const SizedBox(height: 10,),
                           Text("VISUAL",style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Color.fromRGBO(0, 0, 0, 1))),),
                            
                          ],
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 36,
                              width: 36,
                              decoration: BoxDecoration(
                                  color: const Color.fromRGBO(25, 0, 56, 1),
                                  borderRadius: BorderRadius.circular(8)),
                                  child: Center(child: SvgPicture.asset("assets/Traced Image (1).svg"),),
                            ),
                            const SizedBox(height: 10,),
                           Text("ILLUSTRATON",style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Color.fromRGBO(0, 0, 0, 1))),),
                            
                          ],
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Column(
                          children: [
                            Container(
                              height: 36,
                              width: 36,
                              decoration: BoxDecoration(
                                  color: const Color.fromRGBO(25, 0, 56, 1),
                                  borderRadius: BorderRadius.circular(8)),
                                  child: Center(child: SvgPicture.asset("assets/Traced Image (2).svg"),),
                            ),
                            const SizedBox(height: 10,),
                           Text("PHOTO",style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Color.fromRGBO(0, 0, 0, 1))),),
                            
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
