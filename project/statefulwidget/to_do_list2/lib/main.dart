
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import "package:path/path.dart" as p;
import "home.dart";


dynamic database;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  database = await openDatabase(p.join(await getDatabasesPath(), "todoDB.db"),
      version: 1, onCreate: (db, version) {
    return db.execute('''CREATE TABLE Data1(
        cardno INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT,
        description TEXT,
        date TEXT,
        done INT
      )''');
  });
  await setdata(database);
  runApp(const MainApp());
 
}
class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Homepage(),
    );
  }
}
