
import "package:flutter/material.dart";
import "model.dart";
import "package:sqflite/sqflite.dart";
import "package:google_fonts/google_fonts.dart";
import "package:intl/intl.dart";
import "package:flutter_slidable/flutter_slidable.dart";

dynamic database;

Future<void> setdata(data)async{
  database=await data;
}

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {

 List<Model> data1=[];
 bool done=false;

// SlidableController sc=SlidableController(vsync);

Future<void> insertCardData(Model card) async {
  final localDB = await database;
  await localDB.insert(
    'Data1',
    card.cardMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}
Future<void> deleteCard(int? cardno)async{
  final localDB=await database;
  await localDB.delete(
    "Data1",
    where: "cardno = ?",
    whereArgs:[cardno],
  );
  await getCardData();
  setState(() {  
  });
}
Future<void> updatecard(Model card)async{
  final localDB=await database;
  await localDB.update(
    "Data1",
    card.cardMap(),
    where: 'cardno = ?',
    whereArgs:[card.cardno]
  );
  setState(() {
  });
}

Future<List<Model>> getCardData() async {
  final localDB = await database;
 List<Map<String, dynamic>> mapEntry = await localDB.query('Data1');
  return data1= List.generate(mapEntry.length, (i) {
    setState(() {
    
    });
    return Model(
      done: mapEntry[i]['done'],
      cardno: mapEntry[i]['cardno'],
      title: mapEntry[i]['title'], 
      description: mapEntry[i]['description'], 
      date: mapEntry[i]['date']);
});
}

@override
void initState(){
  super.initState();
  getCardData();
}


  // dynamic database;
  
  int tap = -1;
  TextEditingController titleController = TextEditingController();
  TextEditingController descController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  

  void clearController() {
    setState(() {
      titleController.clear();
      descController.clear();
      dateController.clear();
    });
  }

  Icon geticon(Model card) {
    if (card.done==1) {
      return const Icon(Icons.check_circle_sharp,color: Colors.green,);
    } else {
      return const Icon(Icons.circle_outlined);
    }
  }

  void addcard(bool add, [Model? obj]) async {
    if (titleController.text.trim().isNotEmpty &&
        descController.text.trim().isNotEmpty &&
        dateController.text.trim().isNotEmpty) {
      if (add) {
        // data.add(Model(
        //     title: titleController.text.trim(),
        //     description: descController.text.trim(),
        //     date: dateController.text.trim()));

        await insertCardData(Model(
           // cardno:  data1.length+1,
            title: titleController.text.trim(),
            description: descController.text.trim(),
            date: dateController.text.trim()));
            setState(() {
              getCardData();
            });
      } else {
        setState(() {
          obj!.title = titleController.text.trim();
          obj.description = descController.text.trim();
          obj.date = dateController.text.trim();
          updatecard(obj);
        });
      }
    }
  }

  showbottomsheet(bool add, [Model? obj]) async{
    if (!add) {
      titleController.text = obj!.title;
      descController.text = obj.description;
      dateController.text = obj.date;
    }
    showModalBottomSheet(
        isDismissible: true,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: 10,
                    right: 10,
                    left: 10,
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: SizedBox(
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Create Task",
                            style: GoogleFonts.quicksand(
                                textStyle: const TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 22,
                            )),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Title",
                        style: GoogleFonts.quicksand(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 11,
                                color: Color.fromRGBO(89, 57, 241, 1))),
                      ),
                      Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: const Color.fromRGBO(89, 57, 241, 1)),
                              borderRadius: BorderRadius.circular(5)),
                          child: TextFormField(
                            maxLines: null,
                            controller: titleController,
                            decoration: const InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(left: 10),
                                hintText: "Enter Title"),
                          )),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Description",
                        style: GoogleFonts.quicksand(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 11,
                                color: Color.fromRGBO(89, 57, 241, 1))),
                      ),
                      Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: const Color.fromRGBO(89, 57, 241, 1)),
                              borderRadius: BorderRadius.circular(5)),
                          child: TextFormField(
                            //keyboardType: TextInputType.name,
                            maxLines: null,
                            controller: descController,
                            decoration: const InputDecoration(
                                border: InputBorder.none,
                                // isDense: true,
                                contentPadding: EdgeInsets.only(left: 10),
                                hintText: "Enter Description"),
                          )),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Date",
                        style: GoogleFonts.quicksand(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 11,
                                color: Color.fromRGBO(89, 57, 241, 1))),
                      ),
                      Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: const Color.fromRGBO(89, 57, 241, 1)),
                              borderRadius: BorderRadius.circular(5)),
                          child: TextFormField(
                            onTap: () async {
                              DateTime? piker = await showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(1996),
                                  lastDate: DateTime(20230));

                              String date =
                                  DateFormat.yMMMEd().format(piker as DateTime);

                              dateController.text = date;
                            },
                            controller: dateController,
                            decoration: const InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(left: 10),
                                hintText: "Enter Date"),
                          )),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                              style: const ButtonStyle(
                                shape: MaterialStatePropertyAll(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)))),
                                alignment: Alignment.center,
                                fixedSize:
                                    MaterialStatePropertyAll(Size(300, 50)),
                                backgroundColor: MaterialStatePropertyAll(
                                    Color.fromRGBO(89, 57, 241, 1)),
                              ),
                              onPressed: () {
                                add ? addcard(add) : addcard(add, obj);
                                clearController();
                                Navigator.pop(context);
                              },
                              child: Text(
                                "Submit",
                                style: GoogleFonts.quicksand(
                                    textStyle: const TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 20,
                                        color:
                                            Color.fromRGBO(255, 255, 255, 1))),
                              )),
                        ],
                      ),
                      const SizedBox(
                        height: 30,
                      )
                    ],
                  ),
                ),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(111, 81, 255, 1),
      body: Column(
        children: [
          const SizedBox(
            height: 45,
          ),
          Row(
            children: [
              const SizedBox(
                width: 29,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Good morning",
                    style: GoogleFonts.quicksand(
                        textStyle: const TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 28,
                            fontWeight: FontWeight.w400)),
                  ),
                  Text(
                    "Avinash",
                    style: GoogleFonts.quicksand(
                        textStyle: const TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 30,
                            fontWeight: FontWeight.w600)),
                  ),
                  const SizedBox(
                    height: 41,
                  ),
                ],
              )
            ],
          ),
          Expanded(
            child: Container(
              height: double.infinity,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40)),
                color: Color.fromRGBO(217, 217, 217, 1),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 19,
                  ),
                  Text("CREATE TO DO LIST",
                      style: GoogleFonts.quicksand(
                          textStyle: const TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1),
                              fontSize: 12,
                              fontWeight: FontWeight.w500))),
                  const SizedBox(
                    height: 17,
                  ),
                  Expanded(
                    child: Container(
                      decoration: const BoxDecoration(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(40),
                              topRight: Radius.circular(40))),
                      child: Padding(
                        padding: const EdgeInsets.only(
                          top: 30,
                        ),
                        child:  
                        ListView.builder(
                            // physics: const ScrollPhysics(),
                            //shrinkWrap: true,
                            itemCount: data1.length,
                            itemBuilder: (BuildContext context, int index) {
                              
                              return Slidable(
                                
                                // dragStartBehavior: DragStartBehavior.start,
                                endActionPane: ActionPane(
                                  motion: const DrawerMotion(),
                                  children: [
                                    Expanded(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              showbottomsheet(
                                                  false, data1[index]);
                                            },
                                            child: Container(
                                              padding: const EdgeInsets.all(5),
                                              height: 32,
                                              width: 32,
                                              decoration: BoxDecoration(
                                                color: const Color.fromRGBO(
                                                    89, 57, 241, 1),
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: const Icon(
                                                Icons.edit_outlined,
                                                color: Colors.white,
                                                size: 20,
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                deleteCard(data1[index].cardno);
                                              });
                                            },
                                            child: Container(
                                              padding: const EdgeInsets.all(5),
                                              height: 32,
                                              width: 32,
                                              decoration: BoxDecoration(
                                                color: const Color.fromRGBO(
                                                    89, 57, 241, 1),
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: const Icon(
                                                Icons.delete_outline,
                                                color: Colors.white,
                                                size: 20,
                                              ),
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 5,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                child: Container(
                                    width: double.infinity,
                                    decoration: const BoxDecoration(
                                        color: Color.fromRGBO(255, 255, 255, 1),
                                        boxShadow: [
                                          BoxShadow(
                                              color:
                                                  Color.fromRGBO(0, 0, 0, 0.08),
                                              blurRadius: 20,
                                              offset: Offset(0, 4))
                                        ]),
                                    margin: const EdgeInsets.only(bottom: 20),
                                    child: Row(
                                      children: [
                                        const SizedBox(
                                          width: 16,
                                        ),
                                        Container(
                                          height: 52,
                                          width: 52,
                                          decoration: const BoxDecoration(
                                              shape: BoxShape.circle),
                                          child:
                                              Image.asset("assets/profile.png"),
                                        ),
                                        const SizedBox(
                                          width: 20,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const SizedBox(
                                              height: 20,
                                            ),
                                            SizedBox(
                                                width: 200,
                                                child: Text(
                                                  data1[index].title,
                                                  style: GoogleFonts.inter(
                                                      textStyle:
                                                          const TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontSize: 11,
                                                              color: Color
                                                                  .fromRGBO(
                                                                      0,
                                                                      0,
                                                                      0,
                                                                      1))),
                                                )),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            SizedBox(
                                                width: 200,
                                                child: Text(
                                                  data1[index].description,
                                                  style: GoogleFonts.inter(
                                                      textStyle:
                                                          const TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontSize: 9,
                                                              color: Color
                                                                  .fromRGBO(
                                                                      0,
                                                                      0,
                                                                      0,
                                                                      1))),
                                                )),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              data1[index].date,
                                              style: GoogleFonts.inter(
                                                  textStyle: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: 8,
                                                      color: Color.fromRGBO(
                                                          0, 0, 0, 1))),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                          ],
                                        ),
                                        const Spacer(),
                                        GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                data1[index].done=data1[index].done==1?0:1;
                                                updatecard(data1[index]);
                                                getCardData();
                                              });
                                            },
                                            child: geticon(data1[index])),
                                        const SizedBox(
                                          width: 10,
                                        )
                                      ],
                                    )),
                              );
                            }),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
        shape: const CircleBorder(),
        onPressed: () {
          clearController();
          setState(() {
            showbottomsheet(true);

          });
        },
        child: const Icon(
          Icons.add,
          size: 40,
          color: Colors.white,
          weight: 50,
        ),
      ),
    );
  }
}
