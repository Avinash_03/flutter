
class Model {
  int? cardno ;
  String title;
  String description;
  String date;
  int done;
  Model({this.cardno,required this.title, required this.description, required this.date,this.done= 0});
  
  Map<String, dynamic> cardMap() {
    return {
      'title': title,
      'description': description,
      'date': date,
      'done':done
    };
  }
  @override
  String toString(){
    return '{cardno:$cardno,title: $title,description: $description,date: $date,done :$done}';
  }
}
