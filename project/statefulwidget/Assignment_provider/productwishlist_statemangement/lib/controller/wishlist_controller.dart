import 'package:flutter/material.dart';
import 'package:product_statemanagment/model/product_model.dart';

class WishlistController extends ChangeNotifier{

  List<ProductModel> listwish=[];
  
  void addwishlist(ProductModel obj){
    listwish.add(obj);
  }
  void addFavorait(int index){
    listwish[index].fav=!listwish[index].fav;
    notifyListeners();
  }
  
}