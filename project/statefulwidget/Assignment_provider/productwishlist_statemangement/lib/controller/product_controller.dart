import 'package:flutter/material.dart';
import 'package:product_statemanagment/controller/wishlist_controller.dart';
import 'package:product_statemanagment/model/product_model.dart';
import 'package:provider/provider.dart';

class ProductController with ChangeNotifier {

  String url;
  String name;
  double price;
  bool fav=false;
  int cnt=0;

  List<ProductModel> productlist=[];

  ProductController({required this.url,required this.name,required this.price});

  void changeDetails(ProductModel obj){
   productlist.add(obj);
    notifyListeners();
  }

  void addFavorait(int index){
    productlist[index].fav = !productlist[index].fav;
    notifyListeners();
  }
  void increamentproductcnt(int index){
    productlist[index].cnt= cnt++;
    notifyListeners();
  }

  void decreasproductcnt(int index){
    productlist[index].cnt=cnt>0? cnt--:cnt;
    notifyListeners();
  }

}