import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:product_statemanagment/controller/wishlist_controller.dart';
import 'package:provider/provider.dart';

class WishlistScreen extends StatefulWidget{
  const WishlistScreen({super.key});

  @override
  State createState()=> _WishlistScreenState();
}

class _WishlistScreenState extends State{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("WishList Screen"),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: Provider.of<WishlistController>(context).listwish.length,
        itemBuilder:(context,index){
          return     Container(
            margin: const EdgeInsets.all(5),
            padding:const EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: const [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.6),
                  offset: Offset(0, 3),
                  blurRadius: 6
                )
              ]
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                 SizedBox(
                  height: 200,
                  width: 200,
                  child: Image.network(Provider.of<WishlistController>(context).listwish[index].url,height: 200,width: 200,),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                     Text(Provider.of<WishlistController>(context).listwish[index].pname),
                const SizedBox(height: 20,),
                Text("${Provider.of<WishlistController>(context).listwish[index].price}"),
                    GestureDetector(
                      onTap: () {
                        Provider.of<WishlistController>(context,listen: false).addFavorait(index);
                      },
                      child: Icon(Provider.of<WishlistController>(context).listwish[index].fav==false?Icons.favorite_outline:Icons.favorite,color: Colors.red,),
                    ),
              
                    const SizedBox(width: 50,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                       
                    Text('${Provider.of<WishlistController>(context).listwish[index].cnt}'),
                    
                      ],
                    ),
                    
                  ],
                ),
                
              ],
            ),
          );
       
        } ),
    );
  }
}