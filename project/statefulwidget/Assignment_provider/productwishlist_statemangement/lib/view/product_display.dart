import 'package:flutter/material.dart';
import 'package:product_statemanagment/controller/product_controller.dart';
import 'package:product_statemanagment/controller/wishlist_controller.dart';
import 'package:product_statemanagment/model/product_model.dart';
import 'package:product_statemanagment/view/wishlist_screen.dart';
import 'package:provider/provider.dart';

class ProductDisplay extends StatefulWidget {
  const ProductDisplay({super.key});

  @override
  State<ProductDisplay> createState() => _ProductDisplayState();
}

class _ProductDisplayState extends State<ProductDisplay> {

  bool fav=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Product List"),
        actions: [GestureDetector(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>const WishlistScreen()));
          },
          child: const Icon(Icons.favorite,color: Colors.red,size: 25,),
          
        ),
        const SizedBox(width: 10,)
        ],
      ),
      body: ListView.builder(
        itemCount:Provider.of<ProductController>(context).productlist.length,
        itemBuilder: (context,indx) {
          return Container(
            margin: const EdgeInsets.all(5),
            padding:const EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: const [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.6),
                  offset: Offset(0, 3),
                  blurRadius: 6
                )
              ]
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                 SizedBox(
                  height: 200,
                  width: 200,
                  child: Image.network(Provider.of<ProductController>(context).productlist[indx].url,height: 200,width: 200,),
                ),
                Text(Provider.of<ProductController>(context).productlist[indx].pname),
                const SizedBox(height: 20,),
                Text("${Provider.of<ProductController>(context).productlist[indx].price}"),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        fav=!fav;
                        Provider.of<ProductController>(context,listen: false).addFavorait(indx);
                        Provider.of<WishlistController>(context,listen: false).addwishlist(Provider.of<ProductController>(context,listen: false).productlist[indx]);
                      },
                      child: Icon(Provider.of<ProductController>(context).productlist[indx].fav==false?Icons.favorite_outline:Icons.favorite,color: Colors.red,),
                    ),
              
                    const SizedBox(width: 50,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GestureDetector(
                      onTap: () {
                        Provider.of<ProductController>(context,listen: false).decreasproductcnt(indx);
                      },
                      child:const Icon(Icons.remove),
                    ),
                    Text('${Provider.of<ProductController>(context).productlist[indx].cnt}'),
                    GestureDetector(
                      onTap: () {
                        Provider.of<ProductController>(context,listen: false).increamentproductcnt(indx);
                      },
                      child:const Icon(Icons.add),
                    ),
                      ],
                    ),
                    
                  ],
                ),
                
              ],
            ),
          );
        }
      ),
    );
  }
}