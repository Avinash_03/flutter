import 'package:flutter/material.dart';
import 'package:product_statemanagment/controller/product_controller.dart';
import 'package:product_statemanagment/controller/wishlist_controller.dart';
import 'package:product_statemanagment/view/home_screen.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider(
      create: (context) {
        return ProductController(url: "", name: "", price: 0);
      },
    ),
    ChangeNotifierProvider(
      create: (context) {
        return WishlistController();
      },
    )
    
    ],
    child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home:HomeScreen(),
      ),
    );
    
  }
}
