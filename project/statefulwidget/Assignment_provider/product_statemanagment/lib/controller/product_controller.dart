import 'package:flutter/material.dart';
import 'package:product_statemanagment/model/product_model.dart';

class ProductController with ChangeNotifier {

  String url;
  String name;
  double price;
  bool fav=false;
  int cnt=0;

  ProductController({required this.url,required this.name,required this.price});

  void changeDetails(ProductModel obj){
    url=obj.url;
    name=obj.pname;
    price=obj.price;
    notifyListeners();
  }

  void addFavorait(bool fav){
    this.fav=fav;
    notifyListeners();
  }
  void increamentproductcnt(){
    cnt++;
    notifyListeners();
  }

  void decreasproductcnt(){
    cnt>0? cnt--:cnt;
    notifyListeners();
  }

}