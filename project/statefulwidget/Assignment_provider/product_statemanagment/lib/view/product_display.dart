import 'package:flutter/material.dart';
import 'package:product_statemanagment/controller/product_controller.dart';
import 'package:provider/provider.dart';

class ProductDisplay extends StatefulWidget {
  const ProductDisplay({super.key});

  @override
  State<ProductDisplay> createState() => _ProductDisplayState();
}

class _ProductDisplayState extends State<ProductDisplay> {

  bool fav=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Product List"),
      ),
      body: ListView.builder(
        itemCount: 1,
        itemBuilder: (context,indx) {
          return Container(
            margin: const EdgeInsets.all(5),
            padding:const EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: const [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.6),
                  offset: Offset(0, 3),
                  blurRadius: 6
                )
              ]
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                 SizedBox(
                  height: 200,
                  width: 200,
                  child: Image.network(Provider.of<ProductController>(context).url,height: 200,width: 200,),
                ),
                Text(Provider.of<ProductController>(context).name),
                const SizedBox(height: 20,),
                Text("${Provider.of<ProductController>(context).price}"),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        fav=!fav;
                        Provider.of<ProductController>(context,listen: false).addFavorait(fav);
                      },
                      child: Icon(Provider.of<ProductController>(context).fav==false?Icons.favorite_outline:Icons.favorite,color: Colors.red,),
                    ),
              
                    const SizedBox(width: 50,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GestureDetector(
                      onTap: () {
                        Provider.of<ProductController>(context,listen: false).decreasproductcnt();
                      },
                      child:const Icon(Icons.remove),
                    ),
                    Text('${Provider.of<ProductController>(context).cnt}'),
                    GestureDetector(
                      onTap: () {
                        Provider.of<ProductController>(context,listen: false).increamentproductcnt();
                      },
                      child:const Icon(Icons.add),
                    ),
                      ],
                    ),
                    
                  ],
                ),
                
              ],
            ),
          );
        }
      ),
    );
  }
}