import 'package:flutter/material.dart';
import 'package:product_statemanagment/controller/product_controller.dart';
import 'package:product_statemanagment/model/product_model.dart';
import 'package:product_statemanagment/view/product_display.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  TextEditingController urlcontroller= TextEditingController();
  TextEditingController namecontroller= TextEditingController();
  TextEditingController pricecontroller= TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add Product"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
        TextFormField(
          controller: urlcontroller,
          decoration: InputDecoration(
            hintText: "Enter Url",
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))
          ),
        ),
        const SizedBox(height: 30,),
        TextFormField(
          controller: namecontroller,
          decoration: InputDecoration(
            hintText: "Enter Name",
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))
          ),
        ),
        const SizedBox(height: 30,),
        TextFormField(
          controller: pricecontroller,
          decoration: InputDecoration(
            hintText: "Enter Price",
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))
          ),
        ),
        const SizedBox(height: 30,),
        ElevatedButton(
          onPressed: (){
              Provider.of<ProductController>(context,listen: false).changeDetails(ProductModel(cnt: 0,fav: false,url:urlcontroller.text, pname:namecontroller.text,price:double.parse(pricecontroller.text)));
              Navigator.push(context, MaterialPageRoute(builder: (context)=>const ProductDisplay()));
          }, 
          child: const Text("Submit")),
      ],),
    );
  }
}