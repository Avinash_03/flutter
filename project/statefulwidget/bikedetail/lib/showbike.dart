import 'package:bikedetail/bikedetail.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import "model.dart";
import "package:flutter_slidable/flutter_slidable.dart";

dynamic database;
Future<void> setdatabase(dynamic data) async {
  database = data;
}

List data = [];

class Showbike extends StatefulWidget {
  const Showbike({super.key});

  @override
  State<Showbike> createState() => _ShowbikeState();
}

class _ShowbikeState extends State<Showbike> {

@override
void initState(){
  super.initState();
  getData();
}

  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController engineController = TextEditingController();
  TextEditingController weigthController = TextEditingController();
  TextEditingController speedController = TextEditingController();
  TextEditingController image1Controller = TextEditingController();
  TextEditingController image2Controller = TextEditingController();
  TextEditingController image3Controller = TextEditingController();

  void cleaController(){
    nameController.clear();
  priceController.clear();
  engineController.clear();
  weigthController.clear();
  speedController.clear();
  image1Controller.clear();
  image2Controller.clear();
  image3Controller.clear(); 
  }

  Future<void> updateData(Model obj) async {
    final localDB = await database;
    await localDB.update('Bikedetail',
    obj.getmap(),
     where:'id = ?',
    whereArgs:[obj.id]
    );
    await getData();
  }
  Future<void> deleteData(int id) async {
    final localDB = await database;
    await localDB.delete('Bikedetail',
     where:'id = ?',
    whereArgs:[id]
    );
    await getData();
    setState(() {
      
    });
  }

  Future<List> getData() async {
    final localDB = await database;
    List alldata = await localDB.query('Bikedetail');
    return data = List.generate(alldata.length, (i) {
      setState(() {});
      return Model(
          name: alldata[i]['name'],
          engine: alldata[i]['engine'],
          price: alldata[i]['price'],
          weight: alldata[i]['weight'],
          speed: alldata[i]['speed'],
          image1: alldata[i]['image1'],
          image2: alldata[i]['image2'],
          image3: alldata[i]['image3']);
    });
  }

  Future<void> insertData(Model obj) async {
    final localDB = await database;
    await localDB.insert('Bikedetail', obj.getmap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    await getData();
  }

  void myshowbottomsheet() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: 10,
                    left: 10,
                    right: 10,
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: SingleChildScrollView(
                  physics: const ScrollPhysics(),
                  child: Column(
                    children: [
                      const Text("Add Bike Detail"),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text("Bike"),
                      TextField(
                        controller: nameController,
                        decoration:
                            const InputDecoration(hintText: "Enter Bike Name"),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text("Price"),
                      TextField(
                        controller: priceController,
                        decoration:
                            const InputDecoration(hintText: "Enter Bike Price"),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text("Engine"),
                      TextField(
                        controller: engineController,
                        decoration: const InputDecoration(
                            hintText: "Enter Bike Engine Details"),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text("Weigth"),
                      TextField(
                        controller: weigthController,
                        decoration: const InputDecoration(
                            hintText: "Enter Bike Weigth Details"),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text("Speed"),
                      TextField(
                        controller: speedController,
                        decoration:
                            const InputDecoration(hintText: "Enter Bike Speed"),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text("Image 1"),
                      TextField(
                        controller: image1Controller,
                        decoration: const InputDecoration(
                            hintText: "Enter Bike Image URL"),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text("Image 2"),
                      TextField(
                        controller: image2Controller,
                        decoration: const InputDecoration(
                            hintText: "Enter Bike Image URL"),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text("Image 3"),
                      TextField(
                        controller: image3Controller,
                        decoration: const InputDecoration(
                            hintText: "Enter Bike Image URL"),
                      ),
                      ElevatedButton(
                          onPressed: () {
                            insertData(Model(
                                name: nameController.text,
                                engine: double.parse(engineController.text),
                                price: double.parse(priceController.text),
                                weight: double.parse(weigthController.text),
                                speed: double.parse(speedController.text),
                                image1: image1Controller.text,
                                image2: image2Controller.text,
                                image3: image3Controller.text));
                            cleaController();
                            Navigator.pop(context);
                          },
                          child: const Text("Submit"))
                    ],
                  ),
                ),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        leading: const Icon(Icons.arrow_back),
        title: const Text("Bike"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(4),
        child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, index) {
              return Slidable(
                endActionPane: ActionPane(motion:const ScrollMotion(), children: [
                  Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          
                        },
                        child:const Icon(Icons.edit),
                      ),
                      GestureDetector(
                        onTap: () {
                          deleteData(data[index].id);
                        },
                        child:const Icon(Icons.delete),
                      )
                    ],
                  )
                ]),
                child: Container(
                  margin: const EdgeInsets.all(5),
                  padding: const EdgeInsets.all(0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.black,
                    gradient: const LinearGradient(
                        colors: [
                          Colors.black,
                          Colors.white,
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [0.4, 0.8]),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.all(10),
                        height: 200,
                        decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(20)),
                            image: DecorationImage(
                                image: NetworkImage("${data[index].image1}"),
                                fit: BoxFit.fill)),
                        child: const Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Spacer(),
                              // Icon(Icons.favorite_border_outlined,)
                            ],
                          ),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "  ${data[index].name}",
                            style: const TextStyle(
                                fontWeight: FontWeight.w500, color: Colors.black),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            "  ₹ ${data[index].price} Lakh *",
                            style: const TextStyle(
                                fontWeight: FontWeight.w900, color: Colors.black),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const Detail()));
                        },
                        child: Container(
                          height: 50,
                          padding: const EdgeInsets.all(8),
                          margin: const EdgeInsets.only(left: 10, right: 10),
                          decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.white)),
                          child: const Center(
                            child: Text(
                              "View More Detail",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
              );
            }),
      ),
      floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Colors.blueGrey,
          onPressed: () {
            myshowbottomsheet();
          },
          label: const Text(
            "Add New Bike Details",
            style: TextStyle(color: Colors.white),
          )),
    );
  }
}
