import 'package:flutter/material.dart';
import "package:sqflite/sqflite.dart";
import "package:path/path.dart";
import 'showbike.dart';

dynamic database;
void main() async{
  WidgetsFlutterBinding.ensureInitialized();

  database=await openDatabase(
    join('getDatabasesPath()',"BikeDB1.db"),
    version: 1,
    onCreate: (db,version){
      return db.execute('CREATE TABLE Bikedetail (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, image1 TEXT,image2 TEXT,image3 TEXT,price REAL,engine REAL,weight REAL,speed REAL)');
    }
  );
  await setdatabase(database);

  runApp(const MyApp());
}


class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Showbike(),
    );
  }
}
