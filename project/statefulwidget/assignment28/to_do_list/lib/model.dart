

class Model {
  final String title;
  final String description;
  final String date;
  const Model(
    {required this.title,required this.description,required this.date
    }
  );
}