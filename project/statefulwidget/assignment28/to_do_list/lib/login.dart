import "package:flutter/material.dart";
import "package:flutter_svg/flutter_svg.dart";

class Login extends StatefulWidget{
  const Login({super.key});

  @override
  State createState()=> _LoginState();
}

class _LoginState extends State{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(33),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 30,),
              SvgPicture.asset("assets/images/Group 1.svg"),
              const SizedBox(height: 30,),
              const Text("Log in",textAlign: TextAlign.left,),
              const SizedBox(height: 30,),
              TextFormField(),
              const SizedBox(height: 30,),
              TextFormField(),
              SizedBox(height: 10,),
              Container(    
                height: 54,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Color(0XFF0063E6)
                ),
                child: Center(
                  child: Text("Login",style: TextStyle(color: Colors.white),),
                ),
              ),
              Row(
                children: [
                  const SizedBox(width: 40,),
                  const Text("Don't have account?"),
                  const SizedBox(width: 5,),
                  const Text("Sign Up",style: TextStyle(color: Color(0XFF0063E6)),) 
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}