
import "dart:async";

import "package:flutter/material.dart";
import "package:flutter_svg/flutter_svg.dart";
import "package:to_do_list1/login.dart";

class SplashScreen extends StatefulWidget{

  const SplashScreen({super.key});
  @override
  State createState(){ return _SplashScreenState();}

}


class _SplashScreenState extends State{

  @override
  void initState(){
    super.initState();
    Timer(const Duration(seconds: 2), (){
      Navigator.pushReplacement(context, MaterialPageRoute(builder:(context)=>const Login()));
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
        SvgPicture.asset("assets/images/todologo.svg",height: 120,),
        const Text("TO Do App",style: TextStyle(color: Color.fromARGB(255, 60, 126, 212),fontSize: 30),)
      ],)),
    );
  }
}