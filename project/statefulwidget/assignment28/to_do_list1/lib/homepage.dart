import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:google_fonts/google_fonts.dart';
import 'model.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  TextEditingController titlecontroller = TextEditingController();
  TextEditingController descontroller = TextEditingController();
  TextEditingController datecontroller = TextEditingController();

  GlobalKey<FormState> addkey = GlobalKey();

  bool isvalid() {
    titlecontroller.text = titlecontroller.text.trim();
    descontroller.text = descontroller.text.trim();
    datecontroller.text = datecontroller.text.trim();

    if (titlecontroller.text.isEmpty ||
        descontroller.text.isEmpty ||
        datecontroller.text.isEmpty) {
      return false;
    } else {
      return true;
    }
  }

  bool showbottomsheet(int x) {
    if (x != -1) {
      titlecontroller.text = data[x].title;
      descontroller.text = data[x].description;
      datecontroller.text = data[x].date;
    }

    showModalBottomSheet(
        isDismissible: true,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: EdgeInsets.only(
              top: 15,
              right: 15,
              left: 15,
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // const SizedBox(
                  //   height: 15,
                  // ),
                  Text("Create Task",
                      style: GoogleFonts.quicksand(
                          textStyle: const TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1),
                              fontSize: 22,
                              fontWeight: FontWeight.w600))),
                  Row(
                    children: [
                      // const SizedBox(
                      //   width: 15,
                      // ),
                      Text(
                        "Title",
                        textAlign: TextAlign.start,
                        style: GoogleFonts.quicksand(
                            textStyle: const TextStyle(
                                color: Color.fromRGBO(0, 139, 148, 1),
                                fontSize: 11,
                                fontWeight: FontWeight.w400)),
                      ),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: const Color.fromRGBO(0, 139, 148, 1),
                            width: 0.5),
                        borderRadius: BorderRadius.circular(5)),
                    child: TextField(
                      maxLines: null,
                      autofocus: true,
                      controller: titlecontroller,
                      decoration: const InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          border: InputBorder.none,
                          hintText: "Enter Title"),
                    ),
                  ),
                  Row(
                    children: [
                      // const SizedBox(
                      //   width: 15,
                      // ),
                      Text(
                        // const SizedBox(
                        //   width: 15,
                        // ),
                        "Description",
                        textAlign: TextAlign.start,
                        style: GoogleFonts.quicksand(
                            textStyle: const TextStyle(
                                color: Color.fromRGBO(0, 139, 148, 1),
                                fontSize: 11,
                                fontWeight: FontWeight.w400)),
                      ),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: const Color.fromRGBO(0, 139, 148, 1),
                            width: 0.5),
                        borderRadius: BorderRadius.circular(5)),
                    child: TextField(
                      maxLines: null,
                      controller: descontroller,
                      decoration: const InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          border: InputBorder.none,
                          hintText: "Enter Description"),
                    ),
                  ),
                  Row(
                    children: [
                      // const SizedBox(
                      //   width: 15,
                      // ),
                      Text(
                        "Date",
                        textAlign: TextAlign.start,
                        style: GoogleFonts.quicksand(
                          textStyle: const TextStyle(
                              color: Color.fromRGBO(0, 139, 148, 1),
                              fontSize: 11,
                              fontWeight: FontWeight.w400),
                        ),
                      )
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: const Color.fromRGBO(0, 139, 148, 1),
                            width: 0.5),
                        borderRadius: BorderRadius.circular(5)),
                    child: TextFormField(
                      textAlignVertical: TextAlignVertical.top,
                      controller: datecontroller,
                      decoration: const InputDecoration(
                          suffixIcon: Icon(Icons.date_range_sharp),
                          contentPadding:
                              EdgeInsets.only(top: 15, left: 10, bottom: 15),
                          border: InputBorder.none,
                          hintText: "Enter Date"),
                      onTap: () async {
                        DateTime? picker = await showDatePicker(
                            keyboardType: TextInputType.datetime,
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1977),
                            lastDate: DateTime(2025));

                        String formatedate = DateFormat.yMMMd().format(picker!);

                        setState(() {
                          datecontroller.text = formatedate;
                        });
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Enter Date";
                        }
                        return null;
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  ElevatedButton(
                      style: ButtonStyle(
                          shape: MaterialStatePropertyAll(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                          fixedSize:
                              const MaterialStatePropertyAll(Size(300, 50)),
                          backgroundColor: const MaterialStatePropertyAll(
                              Color.fromRGBO(0, 139, 148, 1))),
                      onPressed: () {
                        setState(() {
                          addkey.currentState!.validate();
                        });
                        isvalid()
                            ? setState(() {
                                x == -1
                                    ? data.add(
                                        Model(
                                            title: titlecontroller.text,
                                            description: descontroller.text,
                                            date: datecontroller.text),
                                      )
                                    : data[x] = Model(
                                        title: titlecontroller.text,
                                        description: descontroller.text,
                                        date: datecontroller.text);
                                titlecontroller.clear();
                                descontroller.clear();
                                datecontroller.clear();
                                Navigator.pop(context);
                              })
                            : setState(() {
                              Navigator.pop(context);
                            });
                      },
                      child: Text(
                        "Submit",
                        style: GoogleFonts.inter(
                            textStyle: const TextStyle(
                                color: Color.fromRGBO(
                                  255,
                                  255,
                                  255,
                                  1,
                                ),
                                fontSize: 20,
                                fontWeight: FontWeight.w700)),
                      )),
                  const SizedBox(
                    height: 30,
                  )
                ],
              ),
            ),
          );
        });
    return true;
  }

  List clr = const [
    Color.fromRGBO(250, 232, 232, 1),
    Color.fromRGBO(232, 237, 250, 1),
    Color.fromRGBO(250, 249, 232, 1),
    Color.fromRGBO(250, 232, 250, 1)
  ];
  Color getcolor(int x) {
    // int sel = 0;

    return clr[x % 4];
    //Way One
    // if (x >= clr.length - 1) {
    //   sel = x;
    //   while (sel > clr.length - 1) {
    //     sel = sel - clr.length;
    //   }
    //   return clr[sel];
    // } else {
    //   return clr[x - sel];
    // }
    //Way Two
    // if (x % 2 == 0) {
    //   if (x % 4 != 0) {
    //     return const Color.fromRGBO(250, 249, 232, 1);
    //   } else {
    //     return const Color.fromRGBO(250, 232, 232, 1);
    //   }
    // } else {
    //   if ((x - 1) % 4 != 0) {
    //     return const Color.fromRGBO(250, 232, 250, 1);
    //   } else {
    //     return const Color.fromRGBO(232, 237, 250, 1);
    //   }
    // }
  }

  List data = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
        title: Text("To-do list",
            style: GoogleFonts.quicksand(
              fontWeight: FontWeight.w700,
              fontSize: 26.1,
              color: const Color.fromRGBO(255, 255, 255, 1),
            )),
      ),
      body: Form(
        key: addkey,
        child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                width: double.infinity,
                margin:  (index ==0||index==data.length-1)?(index ==0)?const EdgeInsets.only(top: 30,bottom: 20, right: 15, left: 15):const EdgeInsets.only( bottom: 30, right: 15, left: 15):const EdgeInsets.only(bottom: 20, right: 15, left: 15),
                decoration: BoxDecoration(
                    color: getcolor(index),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: const [
                      BoxShadow(
                          offset: Offset(0, 10),
                          blurRadius: 20,
                          spreadRadius: 1,
                          color: Color.fromRGBO(0, 0, 0, 0.1)),
                    ]),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        const SizedBox(
                          width: 70,
                        ),
                        SizedBox(
                          width: 260,
                          child: Text(data[index].title,
                              style: GoogleFonts.quicksand(
                                // height: 15,
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                                color: const Color.fromRGBO(0, 0, 0, 1),
                              )),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        const SizedBox(
                          width: 10,
                        ),
                        Container(
                          height: 52,
                          width: 52,
                          decoration: BoxDecoration(
                              color: const Color.fromRGBO(255, 255, 255, 1),
                              borderRadius: BorderRadius.circular(50),
                              boxShadow: const [
                                BoxShadow(
                                    offset: Offset(0, 0),
                                    blurRadius: 10,
                                    color: Color.fromRGBO(0, 0, 0, 0.07))
                              ]),
                          child: Center(
                            child: Image.asset(
                              "assets/images/todo.jpg",
                              // height: 19.07,
                              width: 23.79,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Column(
                          children: [
                            const SizedBox(
                              height: 10,
                            ),
                            SizedBox(
                              width: 250,
                              child: Text(
                                data[index].description,
                                style: GoogleFonts.quicksand(
                                    color: const Color.fromRGBO(84, 84, 84, 1),
                                    fontSize: 10,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        const SizedBox(
                          width: 10,
                        ),
                        Text(
                          data[index].date,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.quicksand(
                              color: const Color.fromRGBO(132, 132, 132, 1),
                              fontSize: 10,
                              fontWeight: FontWeight.w500),
                        ),
                        const Spacer(),
                        Center(
                          child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  showbottomsheet(index);
                                });
                              },
                              child: const Icon(Icons.edit_outlined,
                                  color: Color.fromRGBO(0, 139, 148, 1))),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Center(
                          child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  data.removeAt(index);
                                });
                              },
                              child: const Icon(Icons.delete_outline,
                                  color: Color.fromRGBO(0, 139, 148, 1))),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    )
                  ],
                ),
              );
            }),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
        onPressed: (() {
          showbottomsheet(
            -1,
          );
        }),
        child: const Icon(
          Icons.add,
          size: 40,
          color: Colors.white,
        ),
      ),
    );
  }
}
