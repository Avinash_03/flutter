import "package:flutter/material.dart";
import "package:flutter_svg/flutter_svg.dart";
import "package:to_do_list1/homepage.dart";
import "signup.dart";

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State createState() => _LoginState();
}

class _LoginState extends State {
  final TextEditingController _idcontroller = TextEditingController();
  final TextEditingController _passwordcontroller = TextEditingController();

  GlobalKey<FormState> validkey = GlobalKey();
  List logindata = SignUpState.data;

  bool unshowpass = true;

  Icon toggleicon() {
   
    return  Icon(unshowpass? Icons.remove_red_eye_outlined:Icons.remove_red_eye);
  }

  bool checkuser() {
    logindata = SignUpState.data;

    for (int i = 0; i < logindata.length; i++) {
      if (logindata[i]["id"] == _idcontroller.text &&
          logindata[i]["password"] == _passwordcontroller.text) {
        return true;
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(33),
          child: Form(
            key: validkey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 30,
                ),
                Center(child: SvgPicture.asset("assets/images/Group 1.svg")),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  "Log In",
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 25),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  controller: _idcontroller,
                  decoration: InputDecoration(
                      labelText: "User Name",
                      hintText: "Enter UserName",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Enter UserName";
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                TextFormField(
                  obscureText: unshowpass,
                  obscuringCharacter: "*",
                  controller: _passwordcontroller,
                  decoration: InputDecoration(
                    contentPadding:
                        const EdgeInsets.only(left: 10, bottom: 14, top: 10,right: 10),
                    suffix: GestureDetector(onTap: (){
                      setState(() {
                        unshowpass = !unshowpass;
                        toggleicon();
                      });                    
                      },child: toggleicon()),

                    labelText: "Password",
                    hintText: "Enter Password",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10)),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Enter Password";
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 50,
                ),
                GestureDetector(
                  onTap: () {
                    validkey.currentState!.validate();
                    if (checkuser()) {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => const Homepage()));
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text("Enter Valid Data")));
                    }
                  },
                  child: Container(
                    height: 54,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: const Color(0XFF0063E6)),
                    child: const Center(
                      child: Text(
                        "Login",
                        style: TextStyle(color: Colors.white, fontSize: 19),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    const SizedBox(
                      width: 40,
                    ),
                    const Text("Don't have account?",
                        style: TextStyle(fontSize: 15)),
                    const SizedBox(
                      width: 5,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => const SignUp()));
                        _idcontroller.clear();
                        _passwordcontroller.clear();
                      },
                      child: const Text(
                        "Sign Up",
                        style:
                            TextStyle(color: Color(0XFF0063E6), fontSize: 17),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
