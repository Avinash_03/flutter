import "package:flutter/material.dart";
import 'package:flutter_svg/flutter_svg.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});
  @override
  State createState() => SignUpState();
}

class SignUpState extends State {
  final TextEditingController _useridController = TextEditingController();
  final TextEditingController _userpassController = TextEditingController();

  GlobalKey<FormState> checkkey = GlobalKey();

bool unshowpass=true;
  Widget toggleicon() {
    return IconButton(
        onPressed: () {
          setState(() {
            unshowpass = !unshowpass;
          });
        },
        icon: unshowpass
            ? const Icon(Icons.remove_red_eye_outlined)
            : const Icon(Icons.remove_red_eye));
  }

  static List data = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(33),
          child: Form(
            key: checkkey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 30,
                ),
                Center(child: SvgPicture.asset("assets/images/Group 2.svg")),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  "Sign Up",
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 25),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: "First Name",
                      hintText: "Enter Name",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Enter FistName";
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: "Last Name",
                      hintText: "Enter Name",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Enter LastName";
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller: _useridController,
                  decoration: InputDecoration(
                      label: const Text("User Name"),
                      hintText: "Enter UserName",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Enter UserName";
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                TextFormField(
                  textAlignVertical: TextAlignVertical.top,
                  obscureText: unshowpass,
                  controller: _userpassController,
                  decoration: InputDecoration(
                    contentPadding:
                        const EdgeInsets.only(left: 10, bottom: 12, top: 2),
                    suffix: toggleicon(),
                      labelText: "Password",
                      hintText: "Enter Password",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Enter Password";
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 50,
                ),
                GestureDetector(
                  onTap: () {
                    if (checkkey.currentState!.validate()) {
                      data.add({
                        "id": _useridController.text,
                        "password": _userpassController.text
                      });
                      ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text("Account Created")));
                      Navigator.of(context).pop(context);
                    }
                  },
                  child: Container(
                    height: 54,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: const Color(0XFF0063E6)),
                    child: const Center(
                      child: Text(
                        "Create Account",
                        style: TextStyle(color: Colors.white, fontSize: 19),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    const SizedBox(
                      width: 30,
                    ),
                    const Text(
                      "Already have an account?",
                      style: TextStyle(fontSize: 15),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop(context);
                      },
                      child: const Text(
                        "Sign In",
                        style:
                            TextStyle(color: Color(0XFF0063E6), fontSize: 18),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
