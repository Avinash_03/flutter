import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Assignment1(),
    );
  }
}

class Assignment1 extends StatefulWidget {
  const Assignment1({super.key});

  @override
  State<Assignment1> createState() => _Assignment1State();
}

class _Assignment1State extends State<Assignment1> {
  final TextEditingController _namecontroller = TextEditingController();
  List textlist = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "TextField",
          style: TextStyle(
              color: Colors.white, fontSize: 25, fontWeight: FontWeight.w800),
        ),
        centerTitle: true,
        backgroundColor: Colors.black,
      ),

      body:
     
         Column(
         
          children: [
            SizedBox(
                width: double.infinity,
                child: TextField(
                  controller: _namecontroller,
                  decoration: const InputDecoration(hintText: "Enter Name"),
                  onSubmitted: (value) {
                    setState(() {
                       textlist.add(value);
                    });
                    
                  },
                )),
            ListView.builder(
               shrinkWrap: true,
                padding: EdgeInsets.all(5),
                itemCount: textlist.length,
                itemBuilder: ((context, index) {
                  return Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(10),
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 4,
                              offset: Offset(10, 10))
                        ]),
                    child: Text("${textlist[index]}"),
                  );
                }))
          ],
        ),
      
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            textlist.add(_namecontroller.text);
          });
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
