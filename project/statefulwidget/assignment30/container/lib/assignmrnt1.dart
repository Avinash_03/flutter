import 'package:flutter/material.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Container"),
      ),
      body: Center(
        child: Container(
          height: 120,
          width: 120,
          decoration: BoxDecoration(
            color: const Color.fromRGBO(2,147,172,1),
            shape: BoxShape.circle,
            border: Border.all(width: 2,color: Colors.black),
            boxShadow: const[
              BoxShadow(
                blurRadius: 4,
                spreadRadius: 5,
                color: Colors.grey,
                offset: Offset(5,5)
              )
            ]
          ),
        ),
      ),
    );
  }
}