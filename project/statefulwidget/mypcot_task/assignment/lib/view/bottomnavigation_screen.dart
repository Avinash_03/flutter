import 'package:assignment/view/colors.dart';
import 'package:assignment/view/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:svg_flutter/svg_flutter.dart';

class Mybottomnv extends StatefulWidget {
  const Mybottomnv({super.key});

@override
  State createState() => _MybottomnvState();
}

class _MybottomnvState extends State {
  static const List widgateoption = [
    Homepage(),
  ];

  int _selectedindex = 0;

  void selecteditem(int index) {
    setState(() {
      _selectedindex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widgateoption[_selectedindex],
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: Mycolors.primaryFontColor,
        shape: const CircleBorder(),
        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        height: 60,
        padding: const EdgeInsets.symmetric(horizontal: 10),
        color: Colors.white,
        shape: const CircularNotchedRectangle(),
        notchMargin: 10,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            GestureDetector(
              onTap: () {},
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    "assets/icons/bottomnv1.svg",
                  ),
                  Text("Home",
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: Mycolors.primaryFontColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 12)))
                ],
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    "assets/icons/bottomnv2.svg",
                  ),
                  Text("Customers",
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: Mycolors.primaryFontColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 12)))
                ],
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    "assets/icons/bottomnv3.svg",
                  ),
                  Text("Khata",
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: Mycolors.primaryFontColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 12)))
                ],
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    "assets/icons/bottomnv4.svg",
                  ),
                  Text("Orders",
                      style: GoogleFonts.roboto(
                          textStyle: TextStyle(
                              color: Mycolors.primaryFontColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 12)))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
