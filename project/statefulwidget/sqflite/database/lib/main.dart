import "package:flutter/material.dart";
import "package:sqflite/sqflite.dart";
import "package:path/path.dart";
import "package:sqflite_common_ffi/sqflite_ffi.dart";
import "model.dart";

dynamic database;

void main()async{
  WidgetsFlutterBinding.ensureInitialized();
  database=await openDatabase(
    join(await getDatabasesPath(),"DatabaseDB.db"),
    version: 1,
    onCreate: (db, version) {
      return db.execute('''  CREATE TABLE Card1(
        crdno INT PRIMARY KEY,title TEXT,description TEXT,date TEXT
      )
''');
    },
  );
  await insertCard(const Model(crdno: 1, title: "asasssa", description: "asasasaa", date: "1212"));

  print(await getcardData());

}
Future<void> insertCard(Model card)async{
  final localDB = await database;
  await localDB.insert(
    "Card1",
    card.dataMap(),
    conflictAlgorithm:ConflictAlgorithm.replace,
  );
}

Future<List<Map<String,dynamic>>> getcardData()async{
  final localDB=await database;
  List<Map<String,dynamic>> mapEntry=await localDB.query("Card1");
  return mapEntry;
}