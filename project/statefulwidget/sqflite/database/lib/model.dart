

class Model{
  final String title;
  final String description;
  final String date;
  final int crdno;

  const Model({required this.crdno,required this.title,required this.description,required this.date});

  Map<String,dynamic> dataMap(){
    return {"crdno":crdno,"title":title,"description":description,"date":date,};
  }
}