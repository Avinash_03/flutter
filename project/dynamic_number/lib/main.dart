import 'package:flutter/material.dart';

void main()=> runApp(const MyApp());

class MyApp extends StatefulWidget{
  const MyApp({super.key});

  @override
  State createState()=> _MyAppState();
}

class _MyAppState extends State{
  List numberList=[];

  int count=0;
  void addcard(){
    setState(() {
      numberList.add(count+1);
      count++;
    });
  }

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      home: Scaffold(
        appBar: 
        AppBar(title: const Text("My Dynamic List"),),
        body: ListView.builder(
          itemCount: numberList.length,
          itemBuilder: (context,index){
          return Container(
            height: 100,
            margin: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              color: Colors.cyan,
              boxShadow: [BoxShadow(
                blurRadius: 3,
                color: Colors.grey,
                offset: Offset(0, 2)
              )]
            ),
            child: Text("${numberList[index]}",textAlign: TextAlign.center,style:const TextStyle(fontSize: 25),),
          );
        }),
        floatingActionButton: FloatingActionButton(onPressed: addcard,child:const  Icon(Icons.add),),
      ),
    );
  }
}
