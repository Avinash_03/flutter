import 'package:flutter/material.dart';

void main() {
  runApp(const  MyHomePage());
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
 List imageList =[
  "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg",
  "https://www.shutterstock.com/image-vector/ram-mandir-ayodhya-shri-janmbhoomi-600nw-2408858803.jpg",
"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTH9Aby2hOQCTAF8KT0CqcOie0jxSWfYdQo1qC4OkXN-9Pb7U6RLKFVlhxOq7jw_NXH0Es&usqp=CAU"  ];

  @override
  Widget build(BuildContext context) {
   
    return MaterialApp(
    home:Scaffold(
      appBar: AppBar(
        
        title: const Text("Image List"),
      ),
      body: ListView.builder(
        itemCount: imageList.length,
        itemBuilder: (context, Index){
          return Container(
            
            margin: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              color: Colors.cyan,
              boxShadow: [BoxShadow(
                blurRadius: 3,
                color: Colors.grey,
                offset: Offset(0, 2)
              )]
            ),
            child: Image.network(imageList[Index],height: 400,width: 200,),
          );
        })
      // This trailing comma makes auto-formatting nicer for build methods.
    )
    );
  }
}
