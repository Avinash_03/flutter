import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State createState() => _MyAppState();
}

class _MyAppState extends State {

  List musiclist=[];
int cnt=0;
  void changeSong(){
    setState(() {
      musiclist.add(cnt);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            title: const Text(
              "My Music",
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.black,
            actions: const [
              Icon(
                Icons.search_outlined,
                color: Colors.white,
              ),
              SizedBox(
                width: 20,
              ),
              Icon(
                Icons.message_outlined,
                color: Colors.white,
              ),
              SizedBox(
                width: 20,
              ),
              Icon(
                Icons.person_rounded,
                color: Colors.white,
              ),
              SizedBox(
                width: 20,
              ),
            ],
          ),
          body: ListView(children: [
            Column(
              children: [
                const SizedBox(
                  height: 40,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      height: 70,
                      width: 100,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.red[400]),
                      child: Column(children: [
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Center(
                              child: Icon(
                                Icons.favorite,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "Favorites",
                          style: TextStyle(color: Colors.white),
                        ),
                        const SizedBox(
                          height: 5,
                        )
                      ]),
                    ),
                    Container(
                      height: 70,
                      width: 100,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color.fromARGB(255, 240, 131, 7)),
                      child: Column(children: [
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Icon(
                              Icons.playlist_add_circle,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "Playlist",
                          style: TextStyle(color: Colors.white),
                        ),
                        const SizedBox(
                          height: 5,
                        )
                      ]),
                    ),
                    Container(
                      height: 70,
                      width: 100,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color.fromARGB(255, 67, 124, 185)),
                      child: Column(children: [
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Icon(
                              Icons.lock_clock_rounded,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "Recent",
                          style: TextStyle(color: Colors.white),
                        ),
                        const SizedBox(
                          height: 5,
                        )
                      ]),
                    )
                  ],
                ),
                const SizedBox(
                  height: 50,
                ),
                
                Container(
                  height: 50,
                  width: double.infinity,
                  margin: EdgeInsets.all(20),

                  decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Row(
                    children:  [
                      const Icon(
                        Icons.library_music,
                        size: 50,
                      ),
                      const Spacer(),
                      ElevatedButton(
                        
                        onPressed: (){},
                       child: const Text("Next"),
                       style:  ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.black),
                          fixedSize: MaterialStateProperty.all(Size(80, 40)),
                          shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius:BorderRadius.circular(10),
                            
                            )
                            )
                       ),
                       ),
                       const SizedBox(width: 5,)
                    ],
                  ),
                ),
                Container(child:
                Image.network("https://media.istockphoto.com/id/1175435360/vector/music-note-icon-vector-illustration.jpg?s=612x612&w=0&k=20&c=R7s6RR849L57bv_c7jMIFRW4H87-FjLB8sqZ08mN0OU=",height: 200,)
                )
          ]),
          ]),
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.grey,
            onPressed: (){

          },
          child: Icon(Icons.navigate_next_outlined,color: Colors.black,),
          ),
          ),
    );
  }
}
