import "package:flutter/material.dart";

void main()=>runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: QuizApp(),
    );
  }
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State<QuizApp> createState() => _QuizAppState();
}

class _QuizAppState extends State<QuizApp> {

  bool questionScreen=true;
  int questionIndex=0;
  int selectIndex=-1;
  int score=0;
  MaterialStateProperty<Color?> cheack(int buttonIndex){
    if(selectIndex!=-1){
      if(buttonIndex==allquestion[questionIndex]['answerIndex']){
        return const MaterialStatePropertyAll(Colors.green);
      }else if(buttonIndex==selectIndex){
        return const MaterialStatePropertyAll(Colors.red);
      }else{
        return const MaterialStatePropertyAll(null);
      }

    }else{
      return const MaterialStatePropertyAll(null);
    }
  }

  // bool b1=false;
  // bool b2=false;
  // bool b3=false;
  // bool b4=false;

  // bool c1=false;
  // bool c2=false;
  // bool c3=false;
  // bool c4=false;

  // Color isColor1(){
  //   if(c1==true){
  //     return Colors.red;
  //   }else if(b1==true){
  //     return Colors.green;
  //   }else{
  //     return Colors.blue;
  //   }
  // }
  //  Color isColor2(){
  //   if(c2==true){
  //     return Colors.red;
  //   }else if(b2==true){
  //     return Colors.green;
  //   }else{
  //     return Colors.blue;
  //   }
  // }
  //  Color isColor3(){
  //   if(c3==true){
  //     return Colors.red;
  //   }else if(b3==true){
  //     return Colors.green;
  //   }else{
  //     return Colors.blue;
  //   }
  // }
  //  Color isColor4(){
  //   if(c4==true){
  //     return Colors.red;
  //   }else if(b4==true){
  //     return Colors.green;
  //   }else{
  //     return Colors.blue;
  //   }
  // }



  List allquestion=[
    {
      "question":"Who is the fonder of Microsoft?",
      "optios":["Steve Jobs","Jeff Bezos","Bill Gates","Elon Musk"],
      "answerIndex":2,
    },
    {
      "question":"Who is the fonder of Apple",
      "optios":["Steve Jobs","Jeff Bezos","Bill Gates","Elon Musk"],
      "answerIndex":0,
    },
    {
      "question":"Who is the fonder of Amazon?",
      "optios":["Steve Jobs","Jeff Bezos","Bill Gates","Elon Musk"],
      "answerIndex":1,
    },
    {
      "question":"Who is the fonder of Tesla?",
      "optios":["Steve Jobs","Jeff Bezos","Bill Gates","Elon Musk"],
      "answerIndex":3,
    },
    {
      "question":"Who is the fonder of Google?",
      "optios":["Steve Jobs","Lary Page","Bill Gates","Elon Musk"],
      "answerIndex":1,
    }
  ];

  Scaffold isquestion(){
    if(questionScreen==true){
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: 
            const Text(
              "Quiz App",
              style: 
              TextStyle(fontSize: 30,
                        fontWeight: FontWeight.w800,
                        color: Colors.orange
              ),
            ),
          backgroundColor: Colors.blue,
        ),
        body: Column(
          children: [
            const SizedBox(height: 30,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
              const Text("Question : ",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600),),
              Text("${questionIndex+1}/${allquestion.length}",style:const  TextStyle(fontSize: 20,fontWeight: FontWeight.w600),)
            ],),
            const SizedBox(height: 20,),
            Text("${allquestion[questionIndex]['question']}",style:const  TextStyle(fontSize: 20,fontWeight: FontWeight.w600),),
            const SizedBox(height: 30,),
            ElevatedButton(
              style: ButtonStyle(
               backgroundColor:cheack(0),
                fixedSize:const MaterialStatePropertyAll(Size(350, 50))
              ),
              onPressed: (){
                setState(() {
                  if(selectIndex==-1){
                  selectIndex=0;
                  }
                  // if(0==allquestion[questionIndex]['answerIndex']){
                  //   b1=true;
                  // }else{
                  //   c1=true;
                  //   switch(allquestion[questionIndex]['answerIndex']){
                  //     case 0:
                  //       b1=true;
                  //       break;
                  //     case 1:
                  //       b2=true;
                  //       break;
                  //     case 2:
                  //       b3=true;
                  //       break;
                  //     case 3:
                  //       b4=true;
                  //       break;

                  //   }
                  // }
                });

              }, 
              child: Text("${allquestion[questionIndex]["optios"][0]}",style:const  TextStyle(fontSize: 20,fontWeight: FontWeight.w400))
            ),
            const SizedBox(height: 20,),
            ElevatedButton(
              style:  ButtonStyle(
                backgroundColor:cheack(1),
                fixedSize:const MaterialStatePropertyAll(Size(350, 50))
              ),
              onPressed: (){
                setState(() {
                  if(selectIndex==-1){
                     selectIndex=1;
                  }
                
                  // if(1==allquestion[questionIndex]['answerIndex']){
                  //   b2=true;
                  // }else{
                  //    c2=true;
                  //    switch(allquestion[questionIndex]['answerIndex']){
                  //     case 0:
                  //       b1=true;
                  //       break;
                  //     case 1:
                  //       b2=true;
                  //       break;
                  //     case 2:
                  //       b3=true;
                  //       break;
                  //     case 3:
                  //       b4=true;
                  //       break;

                  //   }
                  // }
                });
                 
              }, 
              child: Text("${allquestion[questionIndex]["optios"][1]}",style:const  TextStyle(fontSize: 20,fontWeight: FontWeight.w400))
            ),
            const SizedBox(height: 20,),
            ElevatedButton(
              style:  ButtonStyle(
                backgroundColor:cheack(2),
                fixedSize:const MaterialStatePropertyAll(Size(350, 50))
              ),
              onPressed: (){
                setState(() {
                  if(selectIndex==-1){
                  selectIndex=2;
                  }
                  //  if(2==allquestion[questionIndex]['answerIndex']){
                  //   b3=true;
                  // }else{
                  //    c3=true;
                  //    switch(allquestion[questionIndex]['answerIndex']){
                  //     case 0:
                  //       b1=true;
                  //       break;
                  //     case 1:
                  //       b2=true;
                  //       break;
                  //     case 2:
                  //       b3=true;
                  //       break;
                  //     case 3:
                  //       b4=true;
                  //       break;

                  //   }
                  // }
                });
                 
              }, 
              child: Text("${allquestion[questionIndex]["optios"][2]}",style:const  TextStyle(fontSize: 20,fontWeight: FontWeight.w400))
            ),
            const SizedBox(height: 20,),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor:cheack(3),
                fixedSize:const MaterialStatePropertyAll(Size(350, 50))
              ),
              onPressed: (){
                setState(() {
                  if(selectIndex==-1){
                  selectIndex=3;
                  }
                  //  if(3==allquestion[questionIndex]['answerIndex']){
                  //   b4=true;
                  // }else{
                  //    c4=true;
                  //    switch(allquestion[questionIndex]['answerIndex']){
                  //     case 0:
                  //       b1=true;
                  //       break;
                  //     case 1:
                  //       b2=true;
                  //       break;
                  //     case 2:
                  //       b3=true;
                  //       break;
                  //     case 3:
                  //       b4=true;
                  //       break;

                  //   }
                  // }
                });
                
              }, 
              child: Text("${allquestion[questionIndex]["optios"][3]}",style:const  TextStyle(fontSize: 20,fontWeight: FontWeight.w400))
            ),
           
          ],
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.blue,
          onPressed: (){
            
            setState(() {
             
              if(selectIndex==allquestion[questionIndex]['answerIndex']){
                score+=1;
                
              }
              if(selectIndex!=-1){
                 questionIndex++;
                 selectIndex=-1;
              }
              
              if(allquestion.length==questionIndex){
                questionScreen=false;
              }
              
              // allquestion.length!=questionIndex+1? questionIndex++:questionScreen=false;
              // b1=false;
              // b2=false;
              // b3=false;
              // b4=false;
              // c1=false;
              // c2=false;
              // c3=false;
              // c4=false;
            });
            
          },
          child:const  Icon(
            Icons.forward,
            color: Colors.orange,

            ),
        ),
      );
    }else{
      return Scaffold(
               appBar: AppBar(
          centerTitle: true,
          title: 
            const Text(
              "Quiz App",
              style: 
              TextStyle(fontSize: 30,
                        fontWeight: FontWeight.w800,
                        color: Colors.orange
              ),
            ),
          backgroundColor: Colors.blue,
        ),
        body: Column(
          
          children: [
            const SizedBox(height: 30,width: double.infinity,),
            const Text("Completed....",style: TextStyle(fontWeight: FontWeight.w700,fontSize: 30),),
            const SizedBox(height: 20,),
            Image.network("https://img.freepik.com/premium-vector/winner-trophy-cup-with-ribbon-confetti_51486-122.jpg",height: 300,),
            const SizedBox(height: 30,),
            Text("Score - $score/${allquestion.length}",style:const TextStyle(fontWeight: FontWeight.w700,fontSize: 30),),

            const SizedBox(height: 20,),
            ElevatedButton(
              style: ButtonStyle(
                shadowColor:const MaterialStatePropertyAll(Colors.grey),
                shape: MaterialStatePropertyAll(RoundedRectangleBorder(borderRadius: BorderRadius.circular(30))),
                fixedSize:const MaterialStatePropertyAll(Size(150, 50))
              ),
            onPressed: (){
              setState(() {
                selectIndex=-1;
                questionScreen=true;
                questionIndex=0;
                score=0;
              });
            }, 
            child: const Text("Reset",style: TextStyle(color: Colors.white,fontSize: 25),))
          ],
        ),
        backgroundColor: Colors.white,
      );
    }
  }
  @override
  Widget build(BuildContext context) {
    return isquestion();
  }
}