import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class SinglequestionModel {
  final String? question;
  final List? options;
  final int? answerIndex;

  const SinglequestionModel({this.question, this.options, this.answerIndex});
}

class _MyAppState extends State<MyApp> {
  Color icontstateset() {
    setState(() {});
    return Colors.green;
  }

  List allquestion = [
    const SinglequestionModel(
      question: "What is Flutter?",
      options: ["backend development framework","Flutter is an open-source UI toolkit" ,"Flutter is a cross-platform applications","Flutters is a DBMS toolkit"],
      answerIndex: 1,
    ),
    const SinglequestionModel(
      question: "Who developed the Flutter Framework and continues to maintain it today?",
      options: ["Facebook","Microsoft","Google","Oracle"],
      answerIndex: 2,
    ),
    const SinglequestionModel(
      question: "Which programming language is used to build Flutter applications?",
      options: [" Kotlin","Dart","Java","Go"],
      answerIndex: 1,
    ),
    const SinglequestionModel(
      question: "How many types of widgets are there in Flutter?",
      options: ["2", "4", "6", "8"],
      answerIndex: 0,
    ),
    const SinglequestionModel(
      question: "What is the key configuration file used when building a Flutter project?",
      options: ["pubspec.yaml", "pubspec.xml", "config.html", "root.xml"],
      answerIndex: 0,
    ),
  ];
  int questionIndex = 0;
  int selectedButtonIndex = -1;
  bool questionScreen = true;
  int score = 0;

  Color? checkAnswer(int buttonindex) {
    if (selectedButtonIndex != -1) {
      if (buttonindex == allquestion[questionIndex].answerIndex) {
        return Colors.green[100];
      } else if (selectedButtonIndex == buttonindex) {
        return Colors.red[100];
      } else {
        return Colors.white;
      }
    } else {
      return Colors.white;
    }
  }

  Scaffold checkScreen() {
    if (questionScreen == true) {
      return Scaffold(
        backgroundColor: Colors.cyan[300],
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            'Quiz App',
            style: TextStyle(
                fontSize: 25, color: Colors.black, fontWeight: FontWeight.w900),
          ),
          backgroundColor: Colors.cyan[200],
        ),
        body: Column(
          children: [
            Container(
              margin: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.grey,
                        blurRadius: 4,
                        spreadRadius: 0.1,
                        offset: Offset(2, 1))
                  ]),
              child: Column(
                children: [
                  const SizedBox(
                    height: 30,
                    width: double.infinity,
                  ),
                  Text(
                    "Question : ${questionIndex + 1}/${allquestion.length}",
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  Container(
                   
                    margin: const EdgeInsets.only(left: 20,right: 20),
                    child: Text(
                      "${questionIndex+1}) ${allquestion[questionIndex].question}",
                      style: const TextStyle(
                          fontSize: 20, fontWeight: FontWeight.w700,),
                    ),
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    height: 50,
                    width: 400,
                    decoration: BoxDecoration(
                        border: Border.all(width: 2,
                          color: checkAnswer(0) == Colors.white
                              ? Colors.grey
                              : checkAnswer(0) == Colors.red[100]
                                  ? Colors.red
                                  : Colors.green,
                        ),
                        borderRadius: BorderRadius.circular(10)),
                    child: ElevatedButton(
                        style: ButtonStyle(
                          alignment: Alignment.centerLeft,
                          shape: MaterialStatePropertyAll(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                          backgroundColor:
                              MaterialStatePropertyAll(checkAnswer(0)),
                        ),
                        onPressed: () {
                          if (selectedButtonIndex == -1) {
                            setState(() {
                              selectedButtonIndex = 0;
                            });
                          }
                        },
                        child: Row(
                          children: [
                            Text(
                              "A. ${allquestion[questionIndex].options[0]}",
                              style: TextStyle(
                                  color: checkAnswer(0) == Colors.white
                                      ? Colors.grey
                                      : checkAnswer(0) == Colors.red[100]
                                          ? Colors.red
                                          : Colors.green,
                                  fontWeight: FontWeight.w700),
                            ),
                            const Spacer(),
                            Icon(
                              checkAnswer(0) == Colors.red[100]
                                  ? Icons.cancel_outlined
                                  : checkAnswer(0) != Colors.green[100]
                                      ? Icons.radio_button_unchecked_outlined
                                      : Icons.check_circle_outline_outlined,
                              color: checkAnswer(0) == Colors.red[100]
                                  ? Colors.red
                                  : checkAnswer(0) != Colors.green[100]
                                      ? Colors.grey
                                      : Colors.green,
                            )
                          ],
                        )),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    height: 50,
                    width: 400,
                    decoration: BoxDecoration(
                        border: Border.all(width: 2,
                          color: checkAnswer(1) == Colors.white
                              ? Colors.grey
                              : checkAnswer(1) == Colors.red[100]
                                  ? Colors.red
                                  : Colors.green,
                        ),
                        borderRadius: BorderRadius.circular(10)),
                    child: ElevatedButton(
                        style: ButtonStyle(
                          alignment: Alignment.centerLeft,
                          shape: MaterialStatePropertyAll(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                          backgroundColor:
                              MaterialStatePropertyAll(checkAnswer(1)),
                          fixedSize:
                              const MaterialStatePropertyAll(Size(400, 50)),
                        ),
                        onPressed: () {
                          if (selectedButtonIndex == -1) {
                            setState(() {
                              selectedButtonIndex = 1;
                            });
                          }
                        },
                        child: Row(
                          children: [
                            Text("B. ${allquestion[questionIndex].options[1]}",
                                style: TextStyle(
                                    color: checkAnswer(1) == Colors.white
                                        ? Colors.grey
                                        : checkAnswer(1) == Colors.red[100]
                                            ? Colors.red
                                            : Colors.green,
                                    fontWeight: FontWeight.w700)),
                            const Spacer(),
                            Icon(
                              checkAnswer(1) == Colors.green[100]
                                  ? Icons.check_circle_outline_outlined
                                  : checkAnswer(1) == Colors.white
                                      ? Icons.radio_button_unchecked_outlined
                                      : Icons.cancel_outlined,
                              color: checkAnswer(1) == Colors.red[100]
                                  ? Colors.red
                                  : checkAnswer(1) != Colors.green[100]
                                      ? Colors.grey
                                      : Colors.green,
                            )
                          ],
                        )),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    height: 50,
                    width: 400,
                    decoration: BoxDecoration(
                        border: Border.all(width: 2,
                          color: checkAnswer(2) == Colors.white
                              ? Colors.grey
                              : checkAnswer(2) == Colors.red[100]
                                  ? Colors.red
                                  : Colors.green,
                        ),
                        borderRadius: BorderRadius.circular(10)),
                    child: ElevatedButton(
                        style: ButtonStyle(
                          alignment: Alignment.centerLeft,
                          shape: MaterialStatePropertyAll(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                          backgroundColor:
                              MaterialStatePropertyAll(checkAnswer(2)),
                          fixedSize:
                              const MaterialStatePropertyAll(Size(400, 50)),
                        ),
                        onPressed: () {
                          if (selectedButtonIndex == -1) {
                            setState(() {
                              selectedButtonIndex = 2;
                            });
                          }
                        },
                        child: Row(
                          children: [
                            Text("C. ${allquestion[questionIndex].options[2]}",
                                style: TextStyle(
                                    color: checkAnswer(2) == Colors.white
                                        ? Colors.grey
                                        : checkAnswer(2) == Colors.red[100]
                                            ? Colors.red
                                            : Colors.green,
                                    fontWeight: FontWeight.w700)),
                            const Spacer(),
                            Icon(
                              checkAnswer(2) == Colors.green[100]
                                  ? Icons.check_circle_outline_outlined
                                  : checkAnswer(2) == Colors.white
                                      ? Icons.radio_button_unchecked_outlined
                                      : Icons.cancel_outlined,
                              color: checkAnswer(2) == Colors.red[100]
                                  ? Colors.red
                                  : checkAnswer(2) != Colors.green[100]
                                      ? Colors.grey
                                      : Colors.green,
                            )
                          ],
                        )),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    height: 50,
                    width: 400,
                    decoration: BoxDecoration(
                        border: Border.all(width: 2,
                          color: checkAnswer(3) == Colors.white
                              ? Colors.grey
                              : checkAnswer(3) == Colors.red[100]
                                  ? Colors.red
                                  : Colors.green,
                        ),
                        borderRadius: BorderRadius.circular(10)),
                    child: ElevatedButton(
                        style: ButtonStyle(
                          alignment: Alignment.centerLeft,
                          shape: MaterialStatePropertyAll(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                          backgroundColor:
                              MaterialStatePropertyAll(checkAnswer(3)),
                        ),
                        onPressed: () {
                          if (selectedButtonIndex == -1) {
                            setState(() {
                              selectedButtonIndex = 3;
                            });
                          }
                        },
                        child: Row(
                          children: [
                            Text("D. ${allquestion[questionIndex].options[3]}",
                                style: TextStyle(
                                    color: checkAnswer(3) == Colors.white
                                        ? Colors.grey
                                        : checkAnswer(3) == Colors.red[100]
                                            ? Colors.red
                                            : Colors.green,
                                    fontWeight: FontWeight.w700)),
                            const Spacer(),
                            Icon(
                              checkAnswer(3) == Colors.green[100]
                                  ? Icons.check_circle_outline_outlined
                                  : checkAnswer(3) == Colors.white
                                      ? Icons.radio_button_unchecked_outlined
                                      : Icons.cancel_outlined,
                              color: checkAnswer(3) == Colors.red[100]
                                  ? Colors.red
                                  : checkAnswer(3) != Colors.green[100]
                                      ? Colors.grey
                                      : Colors.green,
                            )
                          ],
                        )),
                  ),
                  const SizedBox(
                    height: 80,
                  ),
                ],
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton.extended(
          label: const Text(
            "  Next ",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w900),
          ),
          onPressed: (() {
            if (questionIndex == allquestion.length - 1) {
              questionScreen = false;
            }
            if (selectedButtonIndex == allquestion[questionIndex].answerIndex) {
              score++;
            }
            if (selectedButtonIndex != -1) {
              questionIndex++;
              setState(() {
                selectedButtonIndex = -1;
              });
            }
          }),
        ),
      );
    } else {
      return Scaffold(
        backgroundColor: Colors.cyan,
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            'Quiz App',
            style: TextStyle(
                fontSize: 25, color: Colors.black, fontWeight: FontWeight.w900),
          ),
          backgroundColor: Colors.cyan[200],
        ),
        body: Column(
          children: [
            Container(
              margin: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.grey,
                        blurRadius: 4,
                        spreadRadius: 0.1,
                        
                        offset: Offset(1, 2))
                  ]),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 30,
                    width: double.infinity,
                  ),
                  Image.asset('assets/trophy.jpg'),
                 
                  const Text(
                    "Congrats !!!",
                    style: TextStyle(fontSize: 35, fontWeight: FontWeight.w600),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                    "Quiz completed successfully.",
                    style: TextStyle(fontSize: 23, fontWeight: FontWeight.w900),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        "You attempt",
                        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      Text(
                        " ${allquestion.length} questions",
                        style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w600,color: Colors.blue),
                      ),
                      const Text(
                        " and",
                        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        " from that",
                        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      Text(
                        " $score answer",
                        style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w600,color: Colors.green),
                      ),
                      const Text(
                        " is correct.",
                        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStatePropertyAll(Colors.cyan[100]),
                          fixedSize:const MaterialStatePropertyAll(Size(150, 40))),
                      onPressed: () {
                        if(score!=allquestion.length){
                        selectedButtonIndex - 1;
                        score = 0;
                        questionIndex = 0;
                        questionScreen = true;
                        }
                        setState(() {});
                      },
                      child: (score!=allquestion.length)? const Text(
                        "Retry",
                        style: TextStyle(color: Colors.black),
                      ):const Text(
                        "Close",
                        style: TextStyle(color: Colors.black),
                      )),
                      const SizedBox(height: 20,)
                ],
              ),
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: checkScreen(),
    );
  }
}
