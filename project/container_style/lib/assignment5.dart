import 'package:flutter/material.dart';

class container5 extends StatefulWidget{
  const container5({super.key});

  @override
  State createState()=> _MyAppState();

}
class _MyAppState extends State {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("Container_Style",style: TextStyle(fontSize: 25,fontWeight: FontWeight.w800,color: Colors.white),),
          backgroundColor: Colors.black,
        ),
        body:Container(
          height: 300,
          width: 300,
          decoration:BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(
              color: Colors.blue,
              width: 5,
            )
          ) ,
          ),
        )
      );
  }
}
