import 'package:flutter/material.dart';

class container7 extends StatefulWidget{
  const container7({super.key});

  @override
  State createState()=> _MyAppState();

}
class _MyAppState extends State {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("Container_Style",style: TextStyle(fontSize: 25,fontWeight: FontWeight.w800,color: Colors.white),),
          backgroundColor: Colors.black,
        ),
        body:Container(
          height: 300,
          width: 300,
          decoration:BoxDecoration(
            color: Colors.yellow,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(
              color: Colors.blue,
              width: 5,
            ),
            boxShadow:const [
              BoxShadow(
                color: Colors.purple,offset: Offset(30, 30),blurRadius: 8
              ),
              BoxShadow(
                color: Colors.red,offset  :Offset(20, 20),blurRadius: 8
              ),
              BoxShadow(
                color: Colors.green,offset: Offset(10, 10),blurRadius: 8
              ),
            ]
          ) ,
          ),
        )
      );
  }
}
