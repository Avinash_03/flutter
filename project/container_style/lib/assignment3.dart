import 'package:flutter/material.dart';

class container3 extends StatefulWidget{
  const container3({super.key});

  @override
  State createState()=> _MyAppState();

}
class _MyAppState extends State {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("Container_Style",style: TextStyle(fontSize: 25,fontWeight: FontWeight.w800,color: Colors.white),),
          backgroundColor: Colors.black,
        ),
        body:Container(
          height: 200,
          width: 200,
          color: Colors.blue,
          margin:const EdgeInsets.only(top: 10,right: 10,left: 10,bottom: 10
          ),
        )
      ),
    );
  }
}
