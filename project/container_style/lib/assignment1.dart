import 'package:flutter/material.dart';

class container1 extends StatefulWidget{
  const container1({super.key});

  @override
  State createState()=> _MyAppState();

}
class _MyAppState extends State {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("Container_Style",style: TextStyle(fontSize: 25,fontWeight: FontWeight.w800,color: Colors.white),),
          backgroundColor: Colors.black,
        ),
        body:Center(
          child: Container(
            height: 200,
            width: 200,
            color: Colors.red,
          ),
        )
      ),
    );
  }
}
