import 'package:flutter/material.dart';

class QuizPage extends StatefulWidget {
  const QuizPage({super.key});
  @override
  State createState() => _QuizPageState();
}

class _QuizPageState extends State {
  List question = ["What is flutter", "What is java","What is your fav. Lang","What is Dart","What is Async","What is awiat","What is container","what is SizedBox","What is column","What is Row"];
  int _counter = 1;

  void _incrementCounter() {
    setState(() {
      (_counter < question.length) ? _counter++ : Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        child: const Icon(Icons.skip_next_outlined),
      ),
      appBar: AppBar(
        title: const Text("Quiz_App"),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 10,
            width: double.infinity,
          ),
          Text(
            "Question :$_counter /10",
            textAlign: TextAlign.center,
          ),
          Text(
            "Question $_counter : ${question[_counter-1]}",
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 10,),
          TextButton(
            onPressed: () {},
            style: TextButton.styleFrom(
                backgroundColor: Colors.blue,
                minimumSize: const Size(double.infinity, 10)),
            child: const Text(
              "Option 1",
              style: TextStyle(color: Colors.white),
            ),
          ),
          const SizedBox(height: 10,),
          TextButton(
            onPressed: () {},
            style: TextButton.styleFrom(
                backgroundColor: Colors.blue,
                minimumSize: const Size(double.infinity, 10)),
            child: const Text(
              "Option 2",
              style: TextStyle(color: Colors.white),
            ),
          ),
          const SizedBox(height: 10,),
          TextButton(
            onPressed: () {},
            style: TextButton.styleFrom(
                backgroundColor: Colors.blue,
                minimumSize: const Size(double.infinity, 10)),
            child: const Text(
              "Option 3",
              style: TextStyle(color: Colors.white),
            ),
          ),
          const SizedBox(height: 10,),

          TextButton(
            onPressed: () {},
            style: TextButton.styleFrom(
                backgroundColor: Colors.blue,
                minimumSize: const Size(double.infinity, 10)),
            child: const Text(
              "Option 4",
              style: TextStyle(color: Colors.white),
            ),
          ),
          // ElevatedButton(onPressed: _incrementCounter, child:const  Text("Next"))
        ],
      ),
    ); // This trailing comma makes auto-formatting nicer for build methods.
  }
}
