/*
5. Create a Screen, in the center of the Screen display a Container with
rounded corners, give a specific color to the Container, the container
must have a shadow of color red.
*/
import 'package:flutter/material.dart';

class Mypage5 extends StatefulWidget{
  const Mypage5({super.key});
  @override
  State createState()=>_Mypage5State();
}

class _Mypage5State extends State{

@override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Container"),
      ),
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: BoxDecoration(
            color: Colors.blueAccent,
            borderRadius: BorderRadius.circular(30),
            boxShadow:const [
              BoxShadow(
                color: Colors.red,
                blurRadius: 15
              )
            ]
          ),
        ),
      ),
    );
  }  
}