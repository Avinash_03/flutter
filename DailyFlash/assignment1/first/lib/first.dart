/*
Create an AppBar, give an Icon at the start of the appbar, give a title
in the middle, and at the end add an Icon.
*/

import 'package:flutter/material.dart';

class Mypage extends StatefulWidget{

    const Mypage({super.key});

    @override
    State createState()=> _MypageState();
}

class _MypageState extends State{

  @override 
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        leading:const Icon(Icons.article),
        centerTitle:true,
        title: const Text("AppBar"),
        actions:const [
          Icon(Icons.mail),
          SizedBox(width: 10,), 
        ],
      ),
    );
  }
}

