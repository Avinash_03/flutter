/*
Create a Screen that will display the Container in the Center of the
Screen,
with size(width: 300, height: 300). The container must have a blue
color and it must have a border which must be of color red.

*/

import 'package:flutter/material.dart';

class Mypage4 extends StatefulWidget{

  const Mypage4({super.key});

  @override
  State createState()=> _Mypage4State();
}

class _Mypage4State extends State{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Container"),
      ),
      body: Center(
        child:Container(
          height: 300,
          width: 300,
          decoration: BoxDecoration(
            color:Colors.blue,
            border: Border.all(color: Colors.red)
          ) ),
    ));
  }
}