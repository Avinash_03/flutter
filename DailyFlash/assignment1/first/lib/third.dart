/*
Create a Screen that will display an AppBar. Add a title in the AppBar
the app bar must have a round rectangular border at the bottom.
*/

import 'package:flutter/material.dart';

class Mypage3 extends StatefulWidget{

  const Mypage3({super.key});

  @override
  State createState()=> _Mypage3State();

}
class _Mypage3State extends State{

  @override 
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title:const Text("AppBar"),
        shape:const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30))
        ),
      ),
    );
  }
}