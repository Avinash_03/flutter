/*
Create an AppBar give a color of your choice to the AppBar and then
add an icon at the start of the AppBar and 3 icons at the end of the
AppBar.
*/

import 'package:flutter/material.dart';

class Mypage2 extends StatefulWidget{

    const Mypage2({super.key});

    @override
    State createState()=> _Mypage2State();
}

class _Mypage2State extends State{

  @override 
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor:Colors.amber,
        leading:const Icon(Icons.article),
        centerTitle:true,
        title: const Text("AppBar"),
        actions:const [
          Icon(Icons.mail),
          SizedBox(width: 10,),
          Icon(Icons.favorite),
          SizedBox(width: 10,),
          Icon(Icons.person),
          SizedBox(width: 10,), 
        ],
      ),
    );
  }
}

