/*
Create a Screen and try to replicate the provided diagram. Customize the UI to
include containers with different colors, providing each container with
appropriate width and height dimensions as shown. Ensure proper margins as
depicted in the provided diagram, using colors and dimensions of your choice.
*/
import "package:flutter/material.dart";

class Assign1 extends StatefulWidget {
  const Assign1({super.key});

  @override
  State<Assign1> createState() => _Assign1State();
}

class _Assign1State extends State<Assign1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(actions: [
        Container(
          margin: EdgeInsets.all(8),
          height: 62,
          width: 62,
          decoration: BoxDecoration(
            color: Colors.blue[100],
            shape: BoxShape.circle
          ),
        ),
      ],),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                height: 200,
                width: 150,
                color: Colors.amber[200],
              ),
               Container(
                height: 200,
                width: 150,
                color: Colors.red[200],
              ),
            
            ],
          ),
          Container(
            height: 100,
            width: 400,
            color: Colors.green[200],
          ),
           Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                height: 200,
                width: 150,
                color: Colors.purple[200],
              ),
               Container(
                height: 200,
                width: 150,
                color: Colors.blue[200],
              ),
            
            ],
          ),
        ],
      ),
    );
  }
}