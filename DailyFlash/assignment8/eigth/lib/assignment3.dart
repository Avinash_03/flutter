import "package:flutter/material.dart";

class Assign3 extends StatefulWidget {
  const Assign3({super.key});

  @override
  State<Assign3> createState() => _Assign3State();
}

class _Assign3State extends State<Assign3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("DailyFlash"),backgroundColor: Colors.blue,),
      body: Center(
        child: Container(
          height: 100,
          width: 400,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.black),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 150,
                decoration:const BoxDecoration(
                  border: Border(right: BorderSide(color: Colors.black
                  ),left:BorderSide(color: Colors.black
                  ) )
                ),
              )
            ],
          ),
        ),
      ),
    
    );
  }
}