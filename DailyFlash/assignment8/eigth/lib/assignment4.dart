/*
Display a ListView in the ListView display 10 Children such that each
child must be a Container having a circular image in the beginning and
next to the image display the Text. Give a border to the container.
Refer to the below image for creating the child:
*/
import 'package:flutter/material.dart';

class Assign4 extends StatefulWidget {
  const Assign4({super.key});

  @override
  State<Assign4> createState() => _Assign4State();
}

class _Assign4State extends State<Assign4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar:AppBar(title: const Text("DailyFlash"),),
    body:
       Padding(
         padding: const EdgeInsets.all(8.0),
         child: ListView(
          children: [
            Container(
              height: 100,
            
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration:const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.network("https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=600"),
                ),
                const Spacer(),
                const Text("Image 1"),
                const SizedBox(width: 20,)
              ],),
            ),
            const SizedBox(height: 20,),
            Container(
              height: 100,
              width: 300,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration:const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.network("https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=600"),
                ),
                const Spacer(),
                const Text("Image 1"),
                const SizedBox(width: 20,)
              ],),
            ),
            const SizedBox(height: 20,),
            Container(
              height: 100,
              width: 300,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration:const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.network("https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=600"),
                ),
                const Spacer(),
                const Text("Image 1"),
                const SizedBox(width: 20,)
              ],),
            ),
            const SizedBox(height: 20,),
            Container(
              height: 100,
              width: 300,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration:const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.network("https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=600"),
                ),
                const Spacer(),
                const Text("Image 1"),
                const SizedBox(width: 20,)
              ],),
            ),
            const SizedBox(height: 20,),
            Container(
              height: 100,
              width: 300,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration:const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.network("https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=600"),
                ),
                const Spacer(),
                const Text("Image 1"),
                const SizedBox(width: 20,)
              ],),
            ),
            const SizedBox(height: 20,),
            Container(
              height: 100,
              width: 300,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration:const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.network("https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=600"),
                ),
                const Spacer(),
                const Text("Image 1"),
                const SizedBox(width: 20,)
              ],),
            ),
            const SizedBox(height: 20,),
            Container(
              height: 100,
              width: 300,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration:const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.network("https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=600"),
                ),
                const Spacer(),
                const Text("Image 1"),
                const SizedBox(width: 20,)
              ],),
            ),
            const SizedBox(height: 20,),
            Container(
              height: 100,
              width: 300,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration:const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.network("https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=600"),
                ),
                const Spacer(),
                const Text("Image 1"),
                const SizedBox(width: 20,)
              ],),
            ),
            const SizedBox(height: 20,),
            Container(
              height: 100,
              width: 300,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration:const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.network("https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=600"),
                ),
                const Spacer(),
                const Text("Image 1"),
                const SizedBox(width: 20,)
              ],),
            ),
            const SizedBox(height: 20,),
            Container(
              height: 100,
              width: 300,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration:const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.network("https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=600"),
                ),
                const Spacer(),
                const Text("Image 9"),
                const SizedBox(width: 20,)
              ],),
            ),
            const SizedBox(height: 20,),
            Container(
              height: 100,
              width: 300,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Row(children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration:const BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.network("https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&w=600"),
                ),
                const Spacer(),
                const Text("Image 10"),
                const SizedBox(width: 20,)
              ],),
            ),
            
          ],
               ),
       ),
  
    );
  }
}