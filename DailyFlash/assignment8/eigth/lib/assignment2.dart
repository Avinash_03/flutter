/*
Create a Screen in which you will have to display the image of food items
and below the image display the name of that food item.
Display 10 such images.The scrolling direction must be horizontal
(refer to the below image )
*/

import "package:flutter/material.dart";

class Assign2 extends StatefulWidget {
  const Assign2({super.key});

  @override
  State<Assign2> createState() => _Assign2State();
}

class _Assign2State extends State<Assign2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash"),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            const SizedBox(width: 10,),
            Column(
              children: [
                Container(
                  height: 200,
                  width: 200,
                  child: Image.network(
                      "https://www.foodiesfeed.com/wp-content/uploads/2023/06/burger-with-melted-cheese.jpg"),
                ),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 50,
                  width: 200,
                  child: ElevatedButton(
                      onPressed: () {}, child: const Text("Food1")),
                )
              ],
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              children: [
                Container(
                  height: 200,
                  width: 200,
                  child: Image.network(
                      "https://www.foodiesfeed.com/wp-content/uploads/2023/06/ice-cream-cone-splash.jpg"),
                ),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 50,
                  width: 200,
                  child: ElevatedButton(
                      onPressed: () {}, child: const Text("Food2")),
                )
              ],
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              children: [
                Container(
                  height: 200,
                  width: 200,
                  child: Image.network(
                      "https://www.foodiesfeed.com/wp-content/uploads/2023/08/fruit-tartelette-macro.jpg"),
                ),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 50,
                  width: 200,
                  child: ElevatedButton(
                      onPressed: () {}, child: const Text("Food3")),
                )
              ],
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              children: [
                Container(
                  height: 200,
                  width: 200,
                  child: Image.network(
                      "https://www.foodiesfeed.com/wp-content/uploads/2023/08/puffed-pastry-with-tomatoes.jpg"),
                ),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 50,
                  width: 200,
                  child: ElevatedButton(
                      onPressed: () {}, child: const Text("Food4")),
                )
              ],
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              children: [
                Container(
                  height: 200,
                  width: 200,
                  child: Image.network(
                      "https://www.foodiesfeed.com/wp-content/uploads/2023/08/tartelettes-top-view.jpg"),
                ),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 50,
                  width: 200,
                  child: ElevatedButton(
                      onPressed: () {}, child: const Text("Food5")),
                )
              ],
            ),
            const SizedBox(
              width: 10,
            ),
          ],
        ),
      ),
    );
  }
}
