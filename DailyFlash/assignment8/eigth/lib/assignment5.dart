/*
Create a screen and display a Listview having a count of 10.
The child of the Listview must be a container which must display a title and a
description on top of each other and next to them display an Icon in a circular
container.
*/
import 'package:flutter/material.dart';

class Assign5 extends StatefulWidget {
  const Assign5({super.key});

  @override
  State<Assign5> createState() => _Assign5State();
}

class _Assign5State extends State<Assign5> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 237, 231, 231),
      appBar:AppBar(title: const Text("DailyFlash"),),
    body: Padding(padding: const EdgeInsets.all(10),
    child: ListView(
      children: [
        
        Container(
              height: 100,
              margin: EdgeInsets.all(10),

              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black),
              ),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(width: 40,),
                 const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Title"),
                    SizedBox(height: 20,),
                    Text("Discription")
                  ],
                ),
                const Spacer(),
                Container(
                  height: 40,
                  width: 40,
                  decoration:const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.purple,),
                  child:const  Icon(Icons.add),
                ),
                const SizedBox(width: 40,)
              ],),
            ),
           Container(
              height: 100,
            
                            margin: EdgeInsets.all(10),

              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black),
              ),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(width: 40,),
                 const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Title"),
                    SizedBox(height: 20,),
                    Text("Discription")
                  ],
                ),
                const Spacer(),
                Container(
                  height: 40,
                  width: 40,
                  decoration:const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.purple,),
                  child:const  Icon(Icons.add),
                ),
                const SizedBox(width: 40,)
              ],),
            ),
             Container(
              height: 100,
            
                            margin: EdgeInsets.all(10),

              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black),
              ),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(width: 40,),
                 const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Title"),
                    SizedBox(height: 20,),
                    Text("Discription")
                  ],
                ),
                const Spacer(),
                Container(
                  height: 40,
                  width: 40,
                  decoration:const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.purple,),
                  child:const  Icon(Icons.add),
                ),
                const SizedBox(width: 40,)
              ],),
            ),
        Container(
              height: 100,
            
                            margin: EdgeInsets.all(10),

              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black),
              ),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(width: 40,),
                 const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Title"),
                    SizedBox(height: 20,),
                    Text("Discription")
                  ],
                ),
                const Spacer(),
                Container(
                  height: 40,
                  width: 40,
                  decoration:const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.purple,),
                  child:const  Icon(Icons.add),
                ),
                const SizedBox(width: 40,)
              ],),
            ),
        Container(
              height: 100,
            
                            margin: EdgeInsets.all(10),

              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black),
              ),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(width: 40,),
                 const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Title"),
                    SizedBox(height: 20,),
                    Text("Discription")
                  ],
                ),
                const Spacer(),
                Container(
                  height: 40,
                  width: 40,
                  decoration:const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.purple,),
                  child:const  Icon(Icons.add),
                ),
                const SizedBox(width: 40,)
              ],),
            ),
        Container(
              height: 100,
            
                            margin: EdgeInsets.all(10),

              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black),
              ),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(width: 40,),
                 const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Title"),
                    SizedBox(height: 20,),
                    Text("Discription")
                  ],
                ),
                const Spacer(),
                Container(
                  height: 40,
                  width: 40,
                  decoration:const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.purple,),
                  child:const  Icon(Icons.add),
                ),
                const SizedBox(width: 40,)
              ],),
            ),
        Container(
              height: 100,
            
                            margin: EdgeInsets.all(10),

              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black),
              ),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(width: 40,),
                 const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Title"),
                    SizedBox(height: 20,),
                    Text("Discription")
                  ],
                ),
                const Spacer(),
                Container(
                  height: 40,
                  width: 40,
                  decoration:const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.purple,),
                  child:const  Icon(Icons.add),
                ),
                const SizedBox(width: 40,)
              ],),
            ),
        Container(
              height: 100,
            
                            margin: EdgeInsets.all(10),

              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black),
              ),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(width: 40,),
                 const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Title"),
                    SizedBox(height: 20,),
                    Text("Discription")
                  ],
                ),
                const Spacer(),
                Container(
                  height: 40,
                  width: 40,
                  decoration:const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.purple,),
                  child:const  Icon(Icons.add),
                ),
                const SizedBox(width: 40,)
              ],),
            ),
        Container(
              height: 100,
            
                            margin: EdgeInsets.all(10),

              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black),
              ),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(width: 40,),
                 const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Title"),
                    SizedBox(height: 20,),
                    Text("Discription")
                  ],
                ),
                const Spacer(),
                Container(
                  height: 40,
                  width: 40,
                  decoration:const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.purple,),
                  child:const  Icon(Icons.add),
                ),
                const SizedBox(width: 40,)
              ],),
            ),
        Container(
              height: 100,
            
                            margin: EdgeInsets.all(10),

              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black),
              ),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(width: 40,),
                 const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Title"),
                    SizedBox(height: 20,),
                    Text("Discription")
                  ],
                ),
                const Spacer(),
                Container(
                  height: 40,
                  width: 40,
                  decoration:const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.purple,),
                  child:const  Icon(Icons.add),
                ),
                const SizedBox(width: 40,)
              ],),
            ),
          
      ],
    ),),
    );
  }
}