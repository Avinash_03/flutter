/*
Create a Screen and then add a floating action button. In this button, you
will have to display your name and an Icon which must be placed in a row.
*/
import "package:flutter/material.dart";

class Assign3 extends StatefulWidget {
  const Assign3({super.key});

  @override
  State<Assign3> createState() => _Assign3State();
}

class _Assign3State extends State<Assign3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        onPressed: (){},
        label:const  Row(children: [
          Text("Avinash"),
          SizedBox(width: 10,),
          Icon(Icons.favorite)
        ],),),
    );
  }
}