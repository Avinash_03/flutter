import "package:flutter/material.dart";

class Assign1 extends StatefulWidget {
  const Assign1({super.key});

  @override
  State<Assign1> createState() => _Assign1State();
}

class _Assign1State extends State<Assign1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:const MaterialStatePropertyAll(Colors.blue),
            shadowColor:const MaterialStatePropertyAll(Colors.red),
            shape:MaterialStatePropertyAll(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10)
            )),
          ),
          onPressed: (){}, 
          child: const Text("Botton")),
      ),
    );
  }
}