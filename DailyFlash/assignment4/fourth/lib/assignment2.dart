/*
Create an Elevated button in the Center of the Screen. Decorate the button as
follows.
a. The button must be of Circular Shape.
b. The Size of the button must be (width:200, height: 200).
c. The button must have a border of color red.
*/

import "package:flutter/material.dart";

class Assign2 extends StatefulWidget {
  const Assign2({super.key});

  @override
  State<Assign2> createState() => _Assign2State();
}

class _Assign2State extends State<Assign2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          style: ButtonStyle(
            side:const MaterialStatePropertyAll(BorderSide(color: Colors.red)),
            fixedSize:const MaterialStatePropertyAll(Size(200,200)),
            backgroundColor:const MaterialStatePropertyAll(Colors.blue),
            shadowColor:const MaterialStatePropertyAll(Colors.red),
            shape:MaterialStatePropertyAll(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            )),
          ),
          onPressed: (){}, 
          child: const Text("Botton")),
      ),
    );
  }
}