/*
Create a Screen that will display 3 containers in a Row, the first container
must be of height 100 and width 100, the 2nd container must be of height
80 and width 80, and 3rd Container must be of height 70 and width 80.
Give color to the containers as per your choice.
*/
import "package:flutter/material.dart";

class Assign1 extends StatefulWidget{
  const Assign1({super.key});

 @override
 State createState()=>_Assign1State();
}
class _Assign1State extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title:const Text("Daily Flash"),),
       body:Center(
        child: Row(
         mainAxisAlignment: MainAxisAlignment.spaceAround,
         children: [
          Container(
            height: 100,
            width: 100,
            color:  Colors.red,
          ),
          Container(
            height: 80,
            width: 80,
            color:  Colors.green,
          ),
          Container(
            height: 70,
            width: 80,
            color:  Colors.blue,
          ),
         ],
        ),
      )
    );
  }
}