/*
Create a Screen with two horizontally aligned containers at the center of the
screen. Apply a shadow to each container set individual colors and give a border
to the Containers only the bottom edges of the container must be rounded.
*/

import "package:flutter/material.dart";

class Assign3 extends StatefulWidget{
  const Assign3({super.key});

 @override
 State createState()=>_Assign3State();
}
class _Assign3State extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body:Center(
        child: Row(
         mainAxisAlignment: MainAxisAlignment.spaceAround,
         children: [
           Container(
            height: 100,width: 100,
            decoration:const BoxDecoration(
              color: Colors.red,
              border: Border(bottom: BorderSide(color: Colors.black)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 4
                )
              ]
            ),
            ),
Container(
            height: 100,width: 100,
            decoration:const BoxDecoration(
              color: Colors.purple,
              border: Border(bottom: BorderSide(color: Colors.black)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 4
                )
              ]
            ),
            ),         ],
        ),
      )
    );
  }
}