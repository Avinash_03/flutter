/*
Create a screen that displays a container. The container must display an image.
Give a circular border only at the bottom of the container. Below the container
display the button with size:(width:250, height:70). The button must display
“Add to cart”. The color of the button must be purple. Both the container and
button must be in the center of the screen.
*/

import "package:flutter/material.dart";

class Assign2 extends StatefulWidget{
  const Assign2({super.key});

 @override
 State createState()=>_Assign2State();
}
class _Assign2State extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: Container(
          width: 200,
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black),
            borderRadius:const BorderRadius.all(Radius.circular(10)),
          ),
          
          child:const Row(
            children: [

              Icon(Icons.star,color: Colors.orange,size: 40,),
              SizedBox(width: 10,),
              Text("Rating 4.5")
            ],
          ),
        ),
      ),
    );
  }
}