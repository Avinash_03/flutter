/*
Create a container that will have a border. The top right and bottom left corners
of the border must be rounded. Now display the Text in the Container and give
appropriate padding to the container.
Refer to the below image :
*/

import "package:flutter/material.dart";

class Assign4 extends StatefulWidget {
  const Assign4({super.key});

  @override
  State<Assign4> createState() => _Assign1State();
}

class _Assign1State extends State<Assign4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: 100,
        width: 400,
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.cyan[100],
          borderRadius:const BorderRadius.only(topLeft: Radius.circular(25),bottomRight: Radius.circular(25)),
          border: Border.all(width: 5,color: Colors.cyan)),
          
          child:const Text("Avinash"),

        ),
        
      );
  }
}