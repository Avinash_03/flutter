/*
In the screen add a container of size( width 100, height: 100) that must only
have a left border of width 5 and color as per your choice. Give padding to the
container and display a text in the Container.
*/

import "package:flutter/material.dart";

class Assign2 extends StatefulWidget {
  const Assign2({super.key});

  @override
  State<Assign2> createState() => _Assign2State();
}

class _Assign2State extends State<Assign2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: 100,
        width: 100,
        padding: const EdgeInsets.all(10),
        decoration:const BoxDecoration(
          color: Colors.black12,
          border: Border(left: BorderSide(width: 5,color: Colors.cyan))
        ),
        child:const Center(
          child: Text("Container"),
        ),
      ),
    );
  }
}