
/*
Add a container with the color red and display the text "Click me!" in the center
of the container. On tapping the container, the text must change to “Container
Tapped” and the color of the container must change to blue.
*/
import "package:flutter/material.dart";

class Assign5 extends StatefulWidget {
  const Assign5({super.key});

  @override
  State<Assign5> createState() => _Assign1State();
}

class _Assign1State extends State<Assign5> {
 
bool change=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          setState(() {
            change=!change;
          });
        },
        child: Container(
          height: 300,
          width: 300,
          padding: const EdgeInsets.all(10),
          decoration:BoxDecoration(
            color: !change?Colors.red:Colors.blue,
          ),
          child: Center(
            child:  change?const Text("ContainerTapped"):const Text("Click Me!"),
          ),
        ),
      ),
    );
  }
}