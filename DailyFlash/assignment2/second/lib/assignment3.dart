/*
In the screen add a container of size( width 100, height: 100) . The container
must have a border as displayed in the below image. Give color to the container
and border as per your choice.
*/

import "package:flutter/material.dart";

class Assign3 extends StatefulWidget {
  const Assign3({super.key});

  @override
  State<Assign3> createState() => _Assign3State();
}

class _Assign3State extends State<Assign3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: 100,
        width: 100,
        decoration: BoxDecoration(
          color: Colors.cyan[100],
            borderRadius:const BorderRadius.only(topRight:Radius.circular(25) ),
          border: Border.all(width: 5,color: Colors.cyan)
        ),
        child:const Center(
          child: Text("Container"),
        ),
      ),
    );
  }
}
