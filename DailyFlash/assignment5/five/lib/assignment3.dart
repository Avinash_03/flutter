/*
Create a Screen and add your image in the center of the screen below your
image display your name in a container, give a shadow to the Container
and give a border to the container the top left and top right corners must
be circular, with a radius of 20. Add appropriate padding to the container.
*/

import "package:flutter/material.dart";

class Assign3 extends StatefulWidget{
  const Assign3({super.key});

 @override
 State createState()=>_Assign3State();
}
class _Assign3State extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body:Center(
        child: Column(
          children: [
            Image.asset("assets/container.jpg"),
            const SizedBox(height: 10,),
            Container(
              padding:const EdgeInsets.all(30),
              decoration:const BoxDecoration(
                color: Colors.black12,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(20),topRight: Radius.circular(20))
              ),
              child: const Text("Avinash"),
            )
          ],
        ),
      )
    );
  }
}