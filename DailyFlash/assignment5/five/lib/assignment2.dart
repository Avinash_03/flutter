/*
Create a Screen in which we have 3 Containers in a Column each container
must be of height 100 and width 100. Each container must have an image
as a child.
*/

import "package:flutter/material.dart";

class Assign2 extends StatefulWidget{
  const Assign2({super.key});

 @override
 State createState()=>_Assign2State();
}
class _Assign2State extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 100,
            width: 100,
            child: Image.asset("assets/container.jpg"),
          ),
           Container(
            height: 100,
            width: 100,
            child: Image.asset("assets/container.jpg"),
          ),
           Container(
            height: 100,
            width: 100,
            child: Image.asset("assets/container.jpg"),
          )
        ],
      ),
    );
  }
}