/*
Create a Screen in which we will display 3 Containers of Size 100,100 in a
Row. Give color to the containers. The containers must divide the free
space in the main axis evenly among each other.
*/
import "package:flutter/material.dart";

class Assign4 extends StatefulWidget{
  const Assign4({super.key});

 @override
 State createState()=>_Assign4State();
}
class _Assign4State extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body:Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
          height: 100,
          width: 100,
          color: Colors.red,
        ),
         Container(
          height: 100,
          width: 100,
          color: Colors.green,
        ),
         Container(
          height: 100,
          width: 100,
          color: Colors.blue,
        )
        ],
      )
    );
  }
}