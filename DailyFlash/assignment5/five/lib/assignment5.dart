/*
Create a Screen that displays 3 widgets in a Column. The image must be the
first widget, the next widget must be a Container of color red and the 3rd
widget must be a Container of color blue. Place all the 3 widgets in a
Column.
The Image must be placed at the top center and the other 2 widgets must
be placed at the bottom center of the screen.
*/
import "package:flutter/material.dart";

class Assign5 extends StatefulWidget{
  const Assign5({super.key});

 @override
 State createState()=>_Assign4State();
}
class _Assign4State extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body:Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset("assets/container.jpg",height: 200,),
            const Spacer(),
             Container(
              height: 100,
              width: 100,
              color: Colors.red,
            ),
            const SizedBox(height: 10,),
             Container(
              height: 100,
              width: 100,
              color: Colors.blue,
            )
            ],
          ),
        ],
      )
    );
  }
}