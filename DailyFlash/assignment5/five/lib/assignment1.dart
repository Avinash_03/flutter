/*
Create a Screen, in the appBar display "Profile Information". In the body,
display an image of size (height: 250 width:250). Below the image add
appropriate spacing and then display the user Name and Phone Number
vertically. The name and phone number must have a font size of 16 and a font
weight of 500.
*/
import "package:flutter_svg/flutter_svg.dart";
import "package:flutter/material.dart";

class Assign1 extends StatefulWidget{
  const Assign1({super.key});

 @override
 State createState()=>_Assign1State();
}
class _Assign1State extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title:const Text("Profile Information"),),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SvgPicture.asset("assets/profile.svg",height: 250,width: 250,),
          const SizedBox(height: 10,),
          const Row(
            children: [SizedBox(width: 90,),
              Text("Avinash",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500),),
            ],
          ),
          const SizedBox(height: 10,),
           const Row(
            children: [ SizedBox(width: 70,),
              Text("9096477654",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500),),
            ],
          ),
        ],
      ),
    );
  }
}