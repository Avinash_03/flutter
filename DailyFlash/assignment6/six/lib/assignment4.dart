/*

*/
import "package:flutter/material.dart";

class Assign4 extends StatefulWidget{
  const Assign4({super.key});

 @override
 State createState()=>_Assign4State();
}
class _Assign4State extends State{
  @override
   Widget build(BuildContext context){
    return Scaffold(
      body:Center(
        child: Container(
          padding:const EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black)
          ),
          child: Row(
           mainAxisAlignment: MainAxisAlignment.spaceAround,
           children: [
            Container(decoration: BoxDecoration(
            border: Border.all(color: Colors.black)
          ),padding: const EdgeInsets.all(10), child: Container(height: 100,width: 100,color: Colors.red,)),
            Container(decoration: BoxDecoration(
            border: Border.all(color: Colors.black)
          ),padding: const EdgeInsets.all(10), child: Container(height: 100,width: 100,color: Colors.purple,)),
           ],
          ),
        ),
      )
    );
  }
}