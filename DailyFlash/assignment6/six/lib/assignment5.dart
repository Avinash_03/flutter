/*
Create a Screen in which we have 3 Containers with size:
(height:100,width:200) placed vertically. Each container must have a
black border. Initially, the Color of the Containers must be white. The
container that is tapped must change its color to red and other containers
must be white.
*/
import "package:flutter/material.dart";

class Assign5 extends StatefulWidget {
  const Assign5({super.key});

  @override
  State createState() => _Assign4State();
}

class _Assign4State extends State {
  int? tapped;
  

  Color getcolor(int x){
    if(x==tapped){
      return Colors.red;
    }else{
      return Colors.white;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              onTap: () {
                setState(() {
                  tapped=1;
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  color: getcolor(1)
                ),
                height: 100,
                width: 200,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  tapped=2;
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  color: getcolor(2)
                ),
                height: 100,
                width: 200,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  tapped=3 ;
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  color: getcolor(3)
                ),
                height: 100,
                width: 200,
              ),
            ),
          ],
        ),
      ],
    ));
  }
}
