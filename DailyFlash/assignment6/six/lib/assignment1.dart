/*
Create a screen that displays an asset image of the food item at the top of the
Screen, below the image, display the name of the food item and below the name
give the description of the item. Add appropriate padding.
*/
import "package:flutter/material.dart";

class Assign1 extends StatefulWidget{
  const Assign1({super.key});

 @override
 State createState()=>_Assign1State();
}
class _Assign1State extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title:const Text("Daily Flash"),),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset("assets/food.jpg",),
          const SizedBox(height: 10,),
          const Padding(padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
             Text("Pizza",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500),),
          SizedBox(height: 10,),
           SizedBox(width: 400,
             child:Text("A Large circle of flat bread baked with cheese, tomatoes, and vegetables spread on top",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500),),),
          ],),
          )
           
        ],
      ),
    );
  }
}