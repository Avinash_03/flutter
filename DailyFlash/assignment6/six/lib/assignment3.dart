/*

*/

import "package:flutter/material.dart";
import "package:flutter/widgets.dart";

class Assign3 extends StatefulWidget{
  const Assign3({super.key});

 @override
 State createState()=>_Assign3State();
}
class _Assign3State extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body:Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
         Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(height: 100,width: 100,color: Colors.red,),
            Container(height: 100,width: 100,color: Colors.purple,)
          ],
         ),
         Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround
          ,
          children: [
            Container(height: 100,width: 100,color: Colors.orange,),
            Container(height: 100,width: 100,color: Colors.green,)
          ],
         )
        ],
      )
    );
  }
}