/*
Create a screen that displays a container. The container must display an image.
Give a circular border only at the bottom of the container. Below the container
display the button with size:(width:250, height:70). The button must display
“Add to cart”. The color of the button must be purple. Both the container and
button must be in the center of the screen.
*/

import "package:flutter/material.dart";

class Assign2 extends StatefulWidget{
  const Assign2({super.key});

 @override
 State createState()=>_Assign2State();
}
class _Assign2State extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              // height: 100,
              // width: 100,
              child: Image.asset("assets/food.jpg"),
              decoration:const  BoxDecoration(
                              color: Colors.grey,

                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20))
              ),
            ),   
            const SizedBox(height: 10,),
            ElevatedButton(
              style:const ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(Colors.purple),
                fixedSize: MaterialStatePropertyAll(Size(250, 70))
              ),
              onPressed: (){

            }, child: const Text("Add To Cart",style: TextStyle(color: Colors.white),))
          ],
        ),
      ),
    );
  }
}