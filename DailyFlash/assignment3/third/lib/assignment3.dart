/*
Add a container in the center of the screen with a size(width: 200,
height: 200). Give a red border to the container. Now when the user taps
the container change the color of the border to green.
*/
import "package:flutter/material.dart";

class Assign3 extends StatefulWidget {
  const Assign3({super.key});

  @override
  State<Assign3> createState() => _Assign3State();
}

class _Assign3State extends State<Assign3> {
  bool tap = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: GestureDetector(
      onTap: () {
        setState(() {
          tap = !tap;
        });
      },
      child: Container(
        height: 300,
        width: 300,
        decoration: BoxDecoration(
            color: Colors.black12,
            border:
                Border.all(width: 5, color: tap ? Colors.green : Colors.red)),
      ),
    )));
  }
}
