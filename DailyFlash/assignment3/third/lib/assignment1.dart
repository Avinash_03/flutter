/*
Create a Container in the Center of the Screen with size(width: 300,
height: 300) and display an image in the center of the Container. Apply
appropriate padding to the container.
*/
import "package:flutter/material.dart";

class Assign1 extends StatefulWidget {
  const Assign1({super.key});

  @override
  State<Assign1> createState() => _Assign1State();
}

class _Assign1State extends State<Assign1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: 
      Container(
        color: Colors.black12,
        height: 300,
        width: 300,
        padding:const  EdgeInsets.all(10),
        child: Center(child: Image.asset('assets/photo.jpg'),),),),
    );
  }
}