/*
Create a Container with size(height:200, width:300) now give a shadow to
the container but the shadow must only be at the top side of the container.
*/
import "package:flutter/material.dart";

class Assign4 extends StatefulWidget {
  const Assign4({super.key});

  @override
  State<Assign4> createState() => _Assign4State();
}

class _Assign4State extends State<Assign4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 200,
          width: 300,
          decoration: BoxDecoration(
            color: Colors.red[100],
            boxShadow: const[
              BoxShadow(
                blurRadius: 4,
                color: Colors.grey,
                offset: Offset(0, -7)
              )
            ]
          ),
        ),
      ),
    );
  }
}