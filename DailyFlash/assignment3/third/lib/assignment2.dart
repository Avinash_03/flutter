/*
Create a Container in the Center of the screen, now In the background of
the Container display an Image (the image can be an asset or network
image ). Also, display text in the center of the Container.
*/

import "package:flutter/material.dart";

class Assign2 extends StatefulWidget {
  const Assign2({super.key});

  @override
  State<Assign2> createState() => _Assign2State();
}

class _Assign2State extends State<Assign2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: 
      Container(
        decoration:const  BoxDecoration(
          image: DecorationImage(image: AssetImage("assets/photo.jpg"),
          fit: BoxFit.cover
          ),
          
        ),
        height: 300,
        width: 300,
        child:const Center(child: Text("Core2Web",style: TextStyle(color: Colors.white),)),),),
    );
  }
}