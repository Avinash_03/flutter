/*
Create 2 TextFields to take input i.e. Name and Phone Number also give a
submit button below the text fields. When the button is pressed the name
and number entered must be displayed below the Button.
*/

import 'package:flutter/material.dart';

class Assign5 extends StatefulWidget {
  const Assign5({super.key});

  @override
  State<Assign5> createState() => _Assign5State();
}

GlobalKey<FormFieldState> check1 = GlobalKey();
GlobalKey<FormFieldState> check2 = GlobalKey();

class _Assign5State extends State<Assign5> {

  TextEditingController nameController=TextEditingController();
  TextEditingController nuberController=TextEditingController();

  String ? name;
  String? number;
  
  bool t1=false;
  bool t2=false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DailyFlash"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 300,
              child: TextField(
                controller: nameController,
                key: check1,
                
                decoration: const InputDecoration(
                  filled: true,
                  hintText: "Enter Name",
                  border: OutlineInputBorder(),
                ),
                
              ),
            ),
            const SizedBox(
              height: 30,
            ),
             SizedBox(
              width: 300,
              child: TextField(
                controller: nuberController,
                key: check2,
                decoration:const InputDecoration(
                  filled: true,
                  hintText: "Enter Mobile Number",
                  border: OutlineInputBorder(),
                ),
                onSubmitted: (value) {
                  if(value.isEmpty){
                    const Text("Enter");
                  }
                },
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            ElevatedButton(onPressed: () {
              setState(() {

                  t1=check1.currentState!.validate();
                  t2=check2.currentState!.validate();
              });
            }, child: const Text("Submit")),
            (t1&&t2)?Column(
              children: [
                const SizedBox(height: 10,),
                Text("Name: $name"),
                Text("Number: $number")
              ],
            ):const SizedBox()
          ],
        ),
      ),
    );
  }
}
