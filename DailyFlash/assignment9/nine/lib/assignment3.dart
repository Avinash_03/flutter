/*
Create a ListView in which there are 10 children refer to the below image
for the child of ListView.
The given image must be the child of the Listview. In the
*/
import "package:flutter/material.dart";

class Assign3 extends StatefulWidget {
  const Assign3({super.key});

  @override
  State<Assign3> createState() => _Assign2State();
}

class _Assign2State extends State<Assign3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DailyFlash"),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                      margin: const EdgeInsets.all(8),
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                      child: Image.asset(
                        "assets/favicon.png",
                        height: 80,
                        width: 80,
                      )),
                  Column(
                    children: [
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Core2Web"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Biencaps"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Incubators"),
                        ),
                      ),
                    ],
                  ),
                  const Icon(Icons.check_circle_outline,size: 60,)
                ],
              ),
            ),
          ),
            Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                      margin: const EdgeInsets.all(8),
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                      child: Image.asset(
                        "assets/favicon.png",
                        height: 80,
                        width: 80,
                      )),
                  Column(
                    children: [
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Core2Web"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Biencaps"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Incubators"),
                        ),
                      ),
                    ],
                  ),
                  const Icon(Icons.check_circle_outline,size: 60,)
                ],
              ),
            ),
          ),
           Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                      margin: const EdgeInsets.all(8),
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                      child: Image.asset(
                        "assets/favicon.png",
                        height: 80,
                        width: 80,
                      )),
                  Column(
                    children: [
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Core2Web"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Biencaps"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Incubators"),
                        ),
                      ),
                    ],
                  ),
                  const Icon(Icons.check_circle_outline,size: 60,)
                ],
              ),
            ),
          ),
           Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                      margin: const EdgeInsets.all(8),
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                      child: Image.asset(
                        "assets/favicon.png",
                        height: 80,
                        width: 80,
                      )),
                  Column(
                    children: [
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Core2Web"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Biencaps"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Incubators"),
                        ),
                      ),
                    ],
                  ),
                  const Icon(Icons.check_circle_outline,size: 60,)
                ],
              ),
            ),
          ),
           Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                      margin: const EdgeInsets.all(8),
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                      child: Image.asset(
                        "assets/favicon.png",
                        height: 80,
                        width: 80,
                      )),
                  Column(
                    children: [
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Core2Web"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Biencaps"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Incubators"),
                        ),
                      ),
                    ],
                  ),
                  const Icon(Icons.check_circle_outline,size: 60,)
                ],
              ),
            ),
          ),
           Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                      margin: const EdgeInsets.all(8),
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                      child: Image.asset(
                        "assets/favicon.png",
                        height: 80,
                        width: 80,
                      )),
                  Column(
                    children: [
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Core2Web"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Biencaps"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Incubators"),
                        ),
                      ),
                    ],
                  ),
                  const Icon(Icons.check_circle_outline,size: 60,)
                ],
              ),
            ),
          ),
           Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                      margin: const EdgeInsets.all(8),
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                      child: Image.asset(
                        "assets/favicon.png",
                        height: 80,
                        width: 80,
                      )),
                  Column(
                    children: [
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Core2Web"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Biencaps"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Incubators"),
                        ),
                      ),
                    ],
                  ),
                  const Icon(Icons.check_circle_outline,size: 60,)
                ],
              ),
            ),
          ),
           Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                      margin: const EdgeInsets.all(8),
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                      child: Image.asset(
                        "assets/favicon.png",
                        height: 80,
                        width: 80,
                      )),
                  Column(
                    children: [
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Core2Web"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Biencaps"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Incubators"),
                        ),
                      ),
                    ],
                  ),
                  const Icon(Icons.check_circle_outline,size: 60,)
                ],
              ),
            ),
          ),
           Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                      margin: const EdgeInsets.all(8),
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                      child: Image.asset(
                        "assets/favicon.png",
                        height: 80,
                        width: 80,
                      )),
                  Column(
                    children: [
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Core2Web"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Biencaps"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Incubators"),
                        ),
                      ),
                    ],
                  ),
                  const Icon(Icons.check_circle_outline,size: 60,)
                ],
              ),
            ),
          ),
           Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                      margin: const EdgeInsets.all(8),
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                      child: Image.asset(
                        "assets/favicon.png",
                        height: 80,
                        width: 80,
                      )),
                  Column(
                    children: [
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Core2Web"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Biencaps"),
                        ),
                      ),
                      Container(
                        width: 90,
                        height: 50,
                        margin: const EdgeInsets.all(8),
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10)),
                        child: const Center(
                          child: Text("Incubators"),
                        ),
                      ),
                    ],
                  ),
                  const Icon(Icons.check_circle_outline,size: 60,)
                ],
              ),
            ),
          ),
       ],
      ),
    );
  }
}
