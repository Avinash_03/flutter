/*
Create a ListView in which there are 8 children and each child must be a
Column widget. In each Column you must have an Image in the Start (The
image must be of width 80 and height 80) Besides the image there must be
a Container with a text in the Center and a border to the Container.
*/
import "package:flutter/material.dart";

class Assign2 extends StatefulWidget {
  const Assign2({super.key});

  @override
  State<Assign2> createState() => _Assign2State();
}

class _Assign2State extends State<Assign2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold
  (
      appBar:AppBar(
        title: const Text("DailyFlash"),
      ),
      body: ListView(
        children: [
          Padding(padding: const EdgeInsets.all(10),
          child: Container(
            height: 100,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
              Image.asset("assets/favicon.png",height: 80,width: 80,),
              Container(
                margin:const EdgeInsets.all(8),
                padding:const EdgeInsets.all(8),
                 decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: const Center(
              child: Text("Core2Web"),
            ),
              )
            ],),
          ),
          )
           , Padding(padding: const EdgeInsets.all(10),
          child: Container(
            height: 100,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
              Image.asset("assets/favicon.png",height: 80,width: 80,),
              Container(
                margin:const EdgeInsets.all(8),
                padding:const EdgeInsets.all(8),
                 decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: const Center(
              child: Text("Core2Web"),
            ),
              )
            ],),
          ),
          ),
            Padding(padding: const EdgeInsets.all(10),
          child: Container(
            height: 100,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
              Image.asset("assets/favicon.png",height: 80,width: 80,),
              Container(
                margin:const EdgeInsets.all(8),
                padding:const EdgeInsets.all(8),
                 decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: const Center(
              child: Text("Core2Web"),
            ),
              )
            ],),
          ),
          ),
            Padding(padding: const EdgeInsets.all(10),
          child: Container(
            height: 100,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
              Image.asset("assets/favicon.png",height: 80,width: 80,),
              Container(
                margin:const EdgeInsets.all(8),
                padding:const EdgeInsets.all(8),
                 decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: const Center(
              child: Text("Core2Web"),
            ),
              )
            ],),
          ),
          )
        ,  Padding(padding: const EdgeInsets.all(10),
          child: Container(
            height: 100,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
              Image.asset("assets/favicon.png",height: 80,width: 80,),
              Container(
                margin:const EdgeInsets.all(8),
                padding:const EdgeInsets.all(8),
                 decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: const Center(
              child: Text("Core2Web"),
            ),
              )
            ],),
          ),
          )
        ,  Padding(padding: const EdgeInsets.all(10),
          child: Container(
            height: 100,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
              Image.asset("assets/favicon.png",height: 80,width: 80,),
              Container(
                margin:const EdgeInsets.all(8),
                padding:const EdgeInsets.all(8),
                 decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: const Center(
              child: Text("Core2Web"),
            ),
              )
            ],),
          ),
          )
        ,  Padding(padding: const EdgeInsets.all(10),
          child: Container(
            height: 100,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
              Image.asset("assets/favicon.png",height: 80,width: 80,),
              Container(
                margin:const EdgeInsets.all(8),
                padding:const EdgeInsets.all(8),
                 decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: const Center(
              child: Text("Core2Web"),
            ),
              )
            ],),
          ),
          )
        ,  Padding(padding: const EdgeInsets.all(10),
          child: Container(
            height: 100,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
              Image.asset("assets/favicon.png",height: 80,width: 80,),
              Container(
                margin:const EdgeInsets.all(8),
                padding:const EdgeInsets.all(8),
                 decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)
            ),
            child: const Center(
              child: Text("Core2Web"),
            ),
              )
            ],),
          ),
          )
        
        
                
        ],
      ),
    );
  }
}