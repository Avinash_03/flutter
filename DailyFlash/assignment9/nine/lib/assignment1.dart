/*
Create a ListView but the Listview must be scrollable in a horizontal
direction. The Listview must contain 5 children. Each child must be a
Container widget of height 60 and width 60, giving color to the container.
*/
import 'package:flutter/material.dart';

class Assign1 extends StatefulWidget {
  const Assign1({super.key});

  @override
  State<Assign1> createState() => _Assifn1State();
}

class _Assifn1State extends State<Assign1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DailyFlash"),
      ),
      body: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                width: 10,
              ),
              Container(
                height: 60,
                width: 60,
                color: Colors.red,
              ),
              const SizedBox(
                width: 10,
              ),
              Container(
                height:60,
                width: 60,
                color: Colors.green,
              ),
              const SizedBox(
                width: 10,
              ),
              Container(
                height: 60,
                width: 60,
                color: Colors.blue,
              ),
              const SizedBox(
                width: 10,
              ),
              Container(
                height: 60,
                width: 60,
                color: Colors.black,
              ),
              const SizedBox(
                width: 10,
              ),
              Container(
                height: 60,
                width: 60,
                color: Colors.amber,
              ),
              const SizedBox(
                width: 10,
              ),
              Container(
                height: 60,
                width: 60,
                color: Colors.orange,
              ),
              const SizedBox(
                width: 10,
              ),
            ],
          )
        ],
      ),
    );
  }
}
