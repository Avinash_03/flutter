/*
Create a Screen and in the center of the screen place a TextField for user input
and a "Submit" `Button` below the TextField. The TextField must have rounded
borders and hint text. The color of the text field must be purple.
*/
import 'package:flutter/material.dart';

class Assign4 extends StatefulWidget {
  const Assign4({super.key});

  @override
  State<Assign4> createState() => _Assignm4State();
}

class _Assignm4State extends State<Assign4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DailyFlash"),
      ),
      body:  
      Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
             const SizedBox(
              width: 300,
               child: TextField(
                decoration: InputDecoration(
                  filled: true,
                  hintText: "Enter Name",
                  fillColor: Colors.purple,
                  border: OutlineInputBorder(),
                
                ),
                           ),
             ),
            const SizedBox(height: 30,),
            ElevatedButton(
              onPressed: (){
              }, 
              child: const Text("Submit")
              )
          ],
        ),
      ),
    );
  }
}