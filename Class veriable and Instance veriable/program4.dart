//Static Veriable	OR Class Veriable
import 'Dart:io';

class Employee{
	static String? Ename="Avi";	//Class veriable 1
	int? EId=25;			//instance veriable 2
	static double? Sal=6;		//Class veriable 3
	void Employeeinfo(){
		print("Employee Name : $Ename");
		print("Employee Id : $EId");
		print("Employee Salary : $Sal");
	}	
}
void main(){
	Employee obj = new Employee();
	obj.Employeeinfo();
	Employee obj2 = Employee();
	obj2.Employeeinfo();
	
	print("====================");
	obj.Sal=7;				//Error: The setter 'Sal' isn't defined for the class 'Employee'.
	obj.Employeeinfo();
	obj2.Employeeinfo();
}
