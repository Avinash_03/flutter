//Intance Veriable
import 'Dart:io';

class Employee{
	String? Ename="Avi";	//instance veriable 1
	int? EId=25;		//instance veriable 2
	double? Sal=6;		//instance veriable 3
	void Employeeinfo(){
		print("Employee Name : $Ename");
		print("Employee Id : $EId");
		print("Employee Salary : $Sal");
	}	
}
void main(){
	Employee obj = new Employee();
	obj.Employeeinfo();
	Employee obj2 = Employee();
	obj2.Employeeinfo();
}
