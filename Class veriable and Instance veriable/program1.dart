class Compony{
	int ecnt=500;
	String Cname = "Google";
	String Loc = "Pune";
	
	void Compinfo(){
		print("Compony Employee Count : $ecnt");
		print("Compony Name : $Cname");
		print("Compony Location : $Loc");
	}
}

void main(){
	Compony c = new Compony();	//we can creat object in four type
	c.Compinfo();
	
	Compony c2 = Compony();
	c2.Compinfo();
	
	new Compony().Compinfo();
	
	Compony().Compinfo();

	
}
